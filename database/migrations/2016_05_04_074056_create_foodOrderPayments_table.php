<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodOrderPaymentsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('foodOrderPayments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id');
			$table->integer('amount');
			$table->integer('payment_type_id');
			$table->boolean('payment_status');
			$table->string('payment_date');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('foodOrderPayments');
	}

}
