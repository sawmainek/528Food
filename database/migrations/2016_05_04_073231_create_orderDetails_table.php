<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderDetails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id');
			$table->integer('food_id');
			$table->integer('size_id');
			$table->integer('qty');
			$table->integer('price');
			$table->double('sub_total');
			$table->string('note');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderDetails');
	}

}
