<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomPricesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roomPrices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('hotel_id');
			$table->integer('room_type_id');
			$table->integer('price');
			$table->string('views');
			$table->integer('floor_no');
			$table->string('image');
			$table->string('facilities');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roomPrices');
	}

}
