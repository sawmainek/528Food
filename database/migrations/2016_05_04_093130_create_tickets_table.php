<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ticket_type_id');
			$table->integer('operator_id');
			$table->integer('agent_id');
			$table->integer('from_city');
			$table->integer('to_city');
			$table->string('from_time');
			$table->string('to_time');
			$table->integer('from_country');
			$table->integer('to_country');
			$table->integer('price');
			$table->integer('return_price');
			$table->integer('class_type_id');
			$table->integer('currency_unit');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
