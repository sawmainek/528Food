<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourPackagesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tourPackages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('agent_id');
			$table->integer('package_type_id');
			$table->string('duration');
			$table->string('start_end_point');
			$table->integer('price');
			$table->string('brief_itinerary');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tourPackages');
	}

}
