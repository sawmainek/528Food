<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('agentUsers/table.agent_id')</th>
			<th>@lang('agentUsers/table.user_id')</th>
    <th width="100px;">@lang('agentUsers/table.action')</th>
    </thead>
    <tbody>
    @foreach($agentUsers as $agentUser)
        <tr>
            <td>{!! $agentUser['agent']['name'] !!}</td>
			<td>{!! $agentUser['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.agentUsers.show', [$agentUser->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.agentUsers.edit', [$agentUser->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.agentUsers.delete', [$agentUser->id]) !!}" onclick="return confirm('@lang('agentUsers/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
