<!-- Agent Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }}">
	    {!! Form::label('agent_id', Lang::get('agentUsers/fields.agent_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('agent_id', $agents, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('agent_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('agent_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('agentUsers/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('agentUsers/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.agentUsers.index') !!}">@lang('agentUsers/fields.cancel')</a>
	    </div>
	</div>
</div>
