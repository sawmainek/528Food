<!-- Agent Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('agent_id', Lang::get('agentUsers/show_fields.agent_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agentUser['agent']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('agentUsers/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agentUser['user']['name'] !!}</p>
        </div>
    </div>

</div>

