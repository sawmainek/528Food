@extends('layouts.admin')

@section('content')
<div class="container">

	<h1>
        @lang('shopUsers/edit.edit_model')
    </h1>

    @include('common.errors')

    <div class="widget-container fluid-height clearfix">
        <div class="heading">
            <i class="fa fa-th-list"></i>@lang('shopUsers/edit.edit_model')
        </div>
        <div class="clearfix">

		    {!! Form::model($shopUser, ['route' => ['administration.shopUsers.update', $shopUser->id], 'enctype'=>'multipart/form-data', 'method' => 'patch', 'class'=>'form-horizontal', 'role'=>'form']) !!}

		        @include('shopUsers.fields')

		    {!! Form::close() !!}
		    
		</div>
	</div>
</div>
@endsection
