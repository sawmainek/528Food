<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('shopUsers/table.user_id')</th>
			<th>@lang('shopUsers/table.shop_id')</th>
    <th width="100px;">@lang('shopUsers/table.action')</th>
    </thead>
    <tbody>
    @foreach($shopUsers as $shopUser)
        <tr>
            <td>{!! $shopUser['user']['name'] !!}</td>
			<td>{!! $shopUser['shop']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.shopUsers.show', [$shopUser->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.shopUsers.edit', [$shopUser->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.shopUsers.delete', [$shopUser->id]) !!}" onclick="return confirm('@lang('shopUsers/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
