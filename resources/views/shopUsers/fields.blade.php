<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('shopUsers/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Shop Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('shop_id') ? ' has-error' : '' }}">
	    {!! Form::label('shop_id', Lang::get('shopUsers/fields.shop_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('shop_id', $shops, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('shop_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('shop_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('shopUsers/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.shopUsers.index') !!}">@lang('shopUsers/fields.cancel')</a>
	    </div>
	</div>
</div>
