<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('shopUsers/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopUser['user']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Shop Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('shop_id', Lang::get('shopUsers/show_fields.shop_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopUser['shop']['name'] !!}</p>
        </div>
    </div>

</div>

