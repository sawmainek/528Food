<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('agents/table.name')</th>
			<th>@lang('agents/table.phone')</th>
			<th>@lang('agents/table.email')</th>
			<th>@lang('agents/table.address')</th>
    <th width="100px;">@lang('agents/table.action')</th>
    </thead>
    <tbody>
    @foreach($agents as $agent)
        <tr>
            <td>{!! $agent->name !!}</td>
			<td>{!! $agent->phone !!}</td>
			<td>{!! $agent->email !!}</td>
			<td>{!! $agent->address !!}</td>
            <td>
                <a href="{!! route('administration.agents.show', [$agent->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.agents.edit', [$agent->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.agents.delete', [$agent->id]) !!}" onclick="return confirm('@lang('agents/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
