<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('agents/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Phone Field -->
<div class="row">
	<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	    {!! Form::label('phone', Lang::get('agents/fields.phone'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('phone', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('phone'))
	            <span class="help-block">
	                <strong>{{ $errors->first('phone') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Email Field -->
<div class="row">
	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    {!! Form::label('email', Lang::get('agents/fields.email'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('email', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('email'))
	            <span class="help-block">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Address Field -->
<div class="row">
	<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
	    {!! Form::label('address', Lang::get('agents/fields.address'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('address', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('address'))
	            <span class="help-block">
	                <strong>{{ $errors->first('address') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('agents/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.agents.index') !!}">@lang('agents/fields.cancel')</a>
	    </div>
	</div>
</div>
