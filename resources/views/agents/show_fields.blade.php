<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('agents/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agent->name !!}</p>
        </div>
    </div>

</div>

<!-- Phone Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('phone', Lang::get('agents/show_fields.phone'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agent->phone !!}</p>
        </div>
    </div>

</div>

<!-- Email Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('email', Lang::get('agents/show_fields.email'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agent->email !!}</p>
        </div>
    </div>

</div>

<!-- Address Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('address', Lang::get('agents/show_fields.address'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $agent->address !!}</p>
        </div>
    </div>

</div>

