<!-- Hotel Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('hotel_id') ? ' has-error' : '' }}">
	    {!! Form::label('hotel_id', Lang::get('roomPrices/fields.hotel_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('hotel_id', $hotels, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('hotel_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('hotel_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Room Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('room_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('room_type_id', Lang::get('roomPrices/fields.room_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('room_type_id', $roomTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('room_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('room_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	    {!! Form::label('price', Lang::get('roomPrices/fields.price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Views Field -->
<div class="row">
	<div class="form-group{{ $errors->has('views') ? ' has-error' : '' }}">
	    {!! Form::label('views', Lang::get('roomPrices/fields.views'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('views', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('views'))
	            <span class="help-block">
	                <strong>{{ $errors->first('views') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Floor No Field -->
<div class="row">
	<div class="form-group{{ $errors->has('floor_no') ? ' has-error' : '' }}">
	    {!! Form::label('floor_no', Lang::get('roomPrices/fields.floor_no'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('floor_no', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('floor_no'))
	            <span class="help-block">
	                <strong>{{ $errors->first('floor_no') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Image Field -->
<div class="row">
	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
	    {!! Form::label('image', Lang::get('roomPrices/fields.image'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($roomPrice))
	                	<img src="{{asset('roomPrices/x400/'.$roomPrice->image)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('image') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('image'))
	            <span class="help-block">
	                <strong>{{ $errors->first('image') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Facilities Field -->
<div class="row">
	<div class="form-group{{ $errors->has('facilities') ? ' has-error' : '' }}">
	    {!! Form::label('facilities', Lang::get('roomPrices/fields.facilities'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('facilities', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('facilities'))
	            <span class="help-block">
	                <strong>{{ $errors->first('facilities') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('roomPrices/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.roomPrices.index') !!}">@lang('roomPrices/fields.cancel')</a>
	    </div>
	</div>
</div>
