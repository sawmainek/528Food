<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('roomPrices/table.hotel_id')</th>
			<th>@lang('roomPrices/table.room_type_id')</th>
			<th>@lang('roomPrices/table.price')</th>
			<th>@lang('roomPrices/table.views')</th>
			<th>@lang('roomPrices/table.floor_no')</th>
			<th>@lang('roomPrices/table.image')</th>
			<th>@lang('roomPrices/table.facilities')</th>
    <th width="100px;">@lang('roomPrices/table.action')</th>
    </thead>
    <tbody>
    @foreach($roomPrices as $roomPrice)
        <tr>
            <td>{!! $roomPrice['hotel']['name'] !!}</td>
			<td>{!! $roomPrice['roomType']['name'] !!}</td>
			<td>{!! $roomPrice->price !!}</td>
			<td>{!! $roomPrice->views !!}</td>
			<td>{!! $roomPrice->floor_no !!}</td>
			<td><img width="150" src="{!! asset('roomPrices/x400/'.$roomPrice->image) !!}"></td>
			<td>{!! $roomPrice->facilities !!}</td>
            <td>
                <a href="{!! route('administration.roomPrices.show', [$roomPrice->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.roomPrices.edit', [$roomPrice->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.roomPrices.delete', [$roomPrice->id]) !!}" onclick="return confirm('@lang('roomPrices/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
