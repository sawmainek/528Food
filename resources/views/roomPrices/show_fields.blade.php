<!-- Hotel Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('hotel_id', Lang::get('roomPrices/show_fields.hotel_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice['hotel']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Room Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('room_type_id', Lang::get('roomPrices/show_fields.room_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice['roomType']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('price', Lang::get('roomPrices/show_fields.price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice->price !!}</p>
        </div>
    </div>

</div>

<!-- Views Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('views', Lang::get('roomPrices/show_fields.views'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice->views !!}</p>
        </div>
    </div>

</div>

<!-- Floor No Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('floor_no', Lang::get('roomPrices/show_fields.floor_no'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice->floor_no !!}</p>
        </div>
    </div>

</div>

<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('roomPrices/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice->image !!}</p>
        </div>
    </div>

</div>

<!-- Facilities Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('facilities', Lang::get('roomPrices/show_fields.facilities'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $roomPrice->facilities !!}</p>
        </div>
    </div>

</div>

