<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('shops/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Address Field -->
<div class="row">
	<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
	    {!! Form::label('address', Lang::get('shops/fields.address'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('address', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('address'))
	            <span class="help-block">
	                <strong>{{ $errors->first('address') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Phone Field -->
<div class="row">
	<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	    {!! Form::label('phone', Lang::get('shops/fields.phone'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('phone', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('phone'))
	            <span class="help-block">
	                <strong>{{ $errors->first('phone') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Image Field -->
<div class="row">
	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
	    {!! Form::label('image', Lang::get('shops/fields.image'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($shop))
	                	<img src="{{asset('shops/x400/'.$shop->image)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('image') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('image'))
	            <span class="help-block">
	                <strong>{{ $errors->first('image') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Shop Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('shop_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('shop_type_id', Lang::get('shops/fields.shop_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('shop_type_id', $shoptypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('shop_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('shop_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Open Hr Field -->
<div class="row">
	<div class="form-group{{ $errors->has('open_hr') ? ' has-error' : '' }}">
	    {!! Form::label('open_hr', Lang::get('shops/fields.open_hr'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('open_hr', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('open_hr'))
	            <span class="help-block">
	                <strong>{{ $errors->first('open_hr') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Lat Field -->
<div class="row">
	<div class="form-group{{ $errors->has('lat') ? ' has-error' : '' }}">
	    {!! Form::label('lat', Lang::get('shops/fields.lat'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('lat', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('lat'))
	            <span class="help-block">
	                <strong>{{ $errors->first('lat') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Lng Field -->
<div class="row">
	<div class="form-group{{ $errors->has('lng') ? ' has-error' : '' }}">
	    {!! Form::label('lng', Lang::get('shops/fields.lng'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('lng', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('lng'))
	            <span class="help-block">
	                <strong>{{ $errors->first('lng') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Total Ratings Field -->
<div class="row">
	<div class="form-group{{ $errors->has('total_ratings') ? ' has-error' : '' }}">
	    {!! Form::label('total_ratings', Lang::get('shops/fields.total_ratings'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('total_ratings', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('total_ratings'))
	            <span class="help-block">
	                <strong>{{ $errors->first('total_ratings') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Total Reviews Field -->
<div class="row">
	<div class="form-group{{ $errors->has('total_reviews') ? ' has-error' : '' }}">
	    {!! Form::label('total_reviews', Lang::get('shops/fields.total_reviews'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('total_reviews', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('total_reviews'))
	            <span class="help-block">
	                <strong>{{ $errors->first('total_reviews') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('shops/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.shops.index') !!}">@lang('shops/fields.cancel')</a>
	    </div>
	</div>
</div>
