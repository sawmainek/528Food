<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('shops/table.name')</th>
			<th>@lang('shops/table.address')</th>
			<th>@lang('shops/table.phone')</th>
			<th>@lang('shops/table.image')</th>
			<th>@lang('shops/table.shop_type_id')</th>
			<th>@lang('shops/table.open_hr')</th>
			<th>@lang('shops/table.lat')</th>
			<th>@lang('shops/table.lng')</th>
			<th>@lang('shops/table.total_ratings')</th>
			<th>@lang('shops/table.total_reviews')</th>
    <th width="100px;">@lang('shops/table.action')</th>
    </thead>
    <tbody>
    @foreach($shops as $shop)
        <tr>
            <td>{!! $shop->name !!}</td>
			<td>{!! $shop->address !!}</td>
			<td>{!! $shop->phone !!}</td>
			<td><img width="150" src="{!! asset('shops/x400/'.$shop->image) !!}"></td>
			<td>{!! $shop['shoptype']['name'] !!}</td>
			<td>{!! $shop->open_hr !!}</td>
			<td>{!! $shop->lat !!}</td>
			<td>{!! $shop->lng !!}</td>
			<td>{!! $shop->total_ratings !!}</td>
			<td>{!! $shop->total_reviews !!}</td>
            <td>
                <a href="{!! route('administration.shops.show', [$shop->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.shops.edit', [$shop->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.shops.delete', [$shop->id]) !!}" onclick="return confirm('@lang('shops/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
