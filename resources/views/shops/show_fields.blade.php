<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('shops/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->name !!}</p>
        </div>
    </div>

</div>

<!-- Address Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('address', Lang::get('shops/show_fields.address'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->address !!}</p>
        </div>
    </div>

</div>

<!-- Phone Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('phone', Lang::get('shops/show_fields.phone'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->phone !!}</p>
        </div>
    </div>

</div>

<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('shops/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->image !!}</p>
        </div>
    </div>

</div>

<!-- Shop Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('shop_type_id', Lang::get('shops/show_fields.shop_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop['shoptype']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Open Hr Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('open_hr', Lang::get('shops/show_fields.open_hr'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->open_hr !!}</p>
        </div>
    </div>

</div>

<!-- Lat Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('lat', Lang::get('shops/show_fields.lat'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->lat !!}</p>
        </div>
    </div>

</div>

<!-- Lng Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('lng', Lang::get('shops/show_fields.lng'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->lng !!}</p>
        </div>
    </div>

</div>

<!-- Total Ratings Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('total_ratings', Lang::get('shops/show_fields.total_ratings'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->total_ratings !!}</p>
        </div>
    </div>

</div>

<!-- Total Reviews Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('total_reviews', Lang::get('shops/show_fields.total_reviews'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shop->total_reviews !!}</p>
        </div>
    </div>

</div>

