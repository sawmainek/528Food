@extends('layouts.admin')

@section('content')
<div class="container">

	<h1>
        @lang('shops/edit.edit_model')
    </h1>

    @include('common.errors')

    <div class="widget-container fluid-height clearfix">
        <div class="heading">
            <i class="fa fa-th-list"></i>@lang('shops/edit.edit_model')
        </div>
        <div class="clearfix">

		    {!! Form::model($shop, ['route' => ['administration.shops.update', $shop->id], 'enctype'=>'multipart/form-data', 'method' => 'patch', 'class'=>'form-horizontal', 'role'=>'form']) !!}

		        @include('shops.fields')

		    {!! Form::close() !!}
		    
		</div>
	</div>
</div>
@endsection
