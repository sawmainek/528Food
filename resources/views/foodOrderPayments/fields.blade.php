<!-- Order Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('order_id') ? ' has-error' : '' }}">
	    {!! Form::label('order_id', Lang::get('foodOrderPayments/fields.order_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('order_id', $foodOrders, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('order_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('order_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Amount Field -->
<div class="row">
	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
	    {!! Form::label('amount', Lang::get('foodOrderPayments/fields.amount'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('amount', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('amount'))
	            <span class="help-block">
	                <strong>{{ $errors->first('amount') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Payment Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('payment_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('payment_type_id', Lang::get('foodOrderPayments/fields.payment_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('payment_type_id', $paymentTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('payment_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('payment_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Payment Status Field -->
<div class="row">
	<div class="form-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
	    {!! Form::label('payment_status', Lang::get('foodOrderPayments/fields.payment_status'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('payment_status', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('payment_status'))
	            <span class="help-block">
	                <strong>{{ $errors->first('payment_status') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Payment Date Field -->
<div class="row">
	<div class="form-group{{ $errors->has('payment_date') ? ' has-error' : '' }}">
	    {!! Form::label('payment_date', Lang::get('foodOrderPayments/fields.payment_date'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('payment_date', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('payment_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('payment_date') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('foodOrderPayments/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.foodOrderPayments.index') !!}">@lang('foodOrderPayments/fields.cancel')</a>
	    </div>
	</div>
</div>
