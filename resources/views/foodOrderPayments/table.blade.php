<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodOrderPayments/table.order_id')</th>
			<th>@lang('foodOrderPayments/table.amount')</th>
			<th>@lang('foodOrderPayments/table.payment_type_id')</th>
			<th>@lang('foodOrderPayments/table.payment_status')</th>
			<th>@lang('foodOrderPayments/table.payment_date')</th>
    <th width="100px;">@lang('foodOrderPayments/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodOrderPayments as $foodOrderPayment)
        <tr>
            <td>{!! $foodOrderPayment['foodOrder']['order_date'] !!}</td>
			<td>{!! $foodOrderPayment->amount !!}</td>
			<td>{!! $foodOrderPayment['paymentType']['name'] !!}</td>
			<td>{!! $foodOrderPayment->payment_status !!}</td>
			<td>{!! $foodOrderPayment->payment_date !!}</td>
            <td>
                <a href="{!! route('administration.foodOrderPayments.show', [$foodOrderPayment->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodOrderPayments.edit', [$foodOrderPayment->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodOrderPayments.delete', [$foodOrderPayment->id]) !!}" onclick="return confirm('@lang('foodOrderPayments/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
