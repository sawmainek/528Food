<!-- Order Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('order_id', Lang::get('foodOrderPayments/show_fields.order_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrderPayment['foodOrder']['order_date'] !!}</p>
        </div>
    </div>

</div>

<!-- Amount Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('amount', Lang::get('foodOrderPayments/show_fields.amount'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrderPayment->amount !!}</p>
        </div>
    </div>

</div>

<!-- Payment Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('payment_type_id', Lang::get('foodOrderPayments/show_fields.payment_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrderPayment['paymentType']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Payment Status Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('payment_status', Lang::get('foodOrderPayments/show_fields.payment_status'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrderPayment->payment_status !!}</p>
        </div>
    </div>

</div>

<!-- Payment Date Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('payment_date', Lang::get('foodOrderPayments/show_fields.payment_date'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrderPayment->payment_date !!}</p>
        </div>
    </div>

</div>

