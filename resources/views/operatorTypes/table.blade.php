<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('operatorTypes/table.name')</th>
    <th width="100px;">@lang('operatorTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($operatorTypes as $operatorType)
        <tr>
            <td>{!! $operatorType->name !!}</td>
            <td>
                <a href="{!! route('administration.operatorTypes.show', [$operatorType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.operatorTypes.edit', [$operatorType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.operatorTypes.delete', [$operatorType->id]) !!}" onclick="return confirm('@lang('operatorTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
