<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('userMessages/table.message')</th>
			<th>@lang('userMessages/table.user_id')</th>
			<th>@lang('userMessages/table.seen')</th>
    <th width="100px;">@lang('userMessages/table.action')</th>
    </thead>
    <tbody>
    @foreach($userMessages as $userMessage)
        <tr>
            <td>{!! $userMessage->message !!}</td>
			<td>{!! $userMessage['user']['name'] !!}</td>
			<td>{!! $userMessage->seen !!}</td>
            <td>
                <a href="{!! route('administration.userMessages.show', [$userMessage->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.userMessages.edit', [$userMessage->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.userMessages.delete', [$userMessage->id]) !!}" onclick="return confirm('@lang('userMessages/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
