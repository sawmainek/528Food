<!-- Message Field -->
<div class="row">
	<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
	    {!! Form::label('message', Lang::get('userMessages/fields.message'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('message', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('message'))
	            <span class="help-block">
	                <strong>{{ $errors->first('message') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('userMessages/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Seen Field -->
<div class="row">
	<div class="form-group{{ $errors->has('seen') ? ' has-error' : '' }}">
	    {!! Form::label('seen', Lang::get('userMessages/fields.seen'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			<div class="radio-inline">
						<label>
									{!! Form::radio('seen', 'Yes', null) !!} <span>Yes</span>
						</label>
			</div>
			<div class="radio-inline">
						<label>
									{!! Form::radio('seen', 'No', null) !!} <span>No</span>
						</label>
			</div>
	        @if ($errors->has('seen'))
	            <span class="help-block">
	                <strong>{{ $errors->first('seen') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('userMessages/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.userMessages.index') !!}">@lang('userMessages/fields.cancel')</a>
	    </div>
	</div>
</div>
