<!-- Message Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('message', Lang::get('userMessages/show_fields.message'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $userMessage->message !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('userMessages/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $userMessage['user']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Seen Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('seen', Lang::get('userMessages/show_fields.seen'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $userMessage->seen !!}</p>
        </div>
    </div>

</div>

