<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('foods/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Category Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
	    {!! Form::label('category_id', Lang::get('foods/fields.category_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('category_id', $foodCategories, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('category_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('category_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Description Field -->
<div class="row">
	<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	    {!! Form::label('description', Lang::get('foods/fields.description'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('description'))
	            <span class="help-block">
	                <strong>{{ $errors->first('description') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Shop Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('shop_id') ? ' has-error' : '' }}">
	    {!! Form::label('shop_id', Lang::get('foods/fields.shop_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('shop_id', $shops, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('shop_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('shop_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Total Ratings Field -->
<div class="row">
	<div class="form-group{{ $errors->has('total_ratings') ? ' has-error' : '' }}">
	    {!! Form::label('total_ratings', Lang::get('foods/fields.total_ratings'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('total_ratings', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('total_ratings'))
	            <span class="help-block">
	                <strong>{{ $errors->first('total_ratings') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Total Reviews Field -->
<div class="row">
	<div class="form-group{{ $errors->has('total_reviews') ? ' has-error' : '' }}">
	    {!! Form::label('total_reviews', Lang::get('foods/fields.total_reviews'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('total_reviews', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('total_reviews'))
	            <span class="help-block">
	                <strong>{{ $errors->first('total_reviews') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('foods/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.foods.index') !!}">@lang('foods/fields.cancel')</a>
	    </div>
	</div>
</div>
