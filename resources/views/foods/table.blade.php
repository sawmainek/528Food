<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foods/table.name')</th>
			<th>@lang('foods/table.category_id')</th>
			<th>@lang('foods/table.description')</th>
			<th>@lang('foods/table.shop_id')</th>
			<th>@lang('foods/table.total_ratings')</th>
			<th>@lang('foods/table.total_reviews')</th>
    <th width="100px;">@lang('foods/table.action')</th>
    </thead>
    <tbody>
    @foreach($foods as $food)
        <tr>
            <td>{!! $food->name !!}</td>
			<td>{!! $food['foodCategory']['name'] !!}</td>
			<td>{!! Illuminate\Support\Str::words($food->description, 16,'....') !!}</td>
			<td>{!! $food['shop']['name'] !!}</td>
			<td>{!! $food->total_ratings !!}</td>
			<td>{!! $food->total_reviews !!}</td>
            <td>
                <a href="{!! route('administration.foods.show', [$food->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foods.edit', [$food->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foods.delete', [$food->id]) !!}" onclick="return confirm('@lang('foods/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
