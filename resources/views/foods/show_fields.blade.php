<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('foods/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food->name !!}</p>
        </div>
    </div>

</div>

<!-- Category Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('category_id', Lang::get('foods/show_fields.category_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food['foodCategory']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Description Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('description', Lang::get('foods/show_fields.description'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food->description !!}</p>
        </div>
    </div>

</div>

<!-- Shop Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('shop_id', Lang::get('foods/show_fields.shop_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food['shop']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Total Ratings Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('total_ratings', Lang::get('foods/show_fields.total_ratings'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food->total_ratings !!}</p>
        </div>
    </div>

</div>

<!-- Total Reviews Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('total_reviews', Lang::get('foods/show_fields.total_reviews'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $food->total_reviews !!}</p>
        </div>
    </div>

</div>

