<!-- Role Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('role_id', Lang::get('userRoles/show_fields.role_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $userRole['role']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('userRoles/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $userRole['user']['name'] !!}</p>
        </div>
    </div>

</div>

