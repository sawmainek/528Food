<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('userRoles/table.role_id')</th>
			<th>@lang('userRoles/table.user_id')</th>
    <th width="100px;">@lang('userRoles/table.action')</th>
    </thead>
    <tbody>
    @foreach($userRoles as $userRole)
        <tr>
            <td>{!! $userRole['role']['name'] !!}</td>
			<td>{!! $userRole['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.userRoles.show', [$userRole->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.userRoles.edit', [$userRole->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.userRoles.delete', [$userRole->id]) !!}" onclick="return confirm('@lang('userRoles/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
