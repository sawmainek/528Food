<!-- Role Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
	    {!! Form::label('role_id', Lang::get('userRoles/fields.role_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('role_id', $roles, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('role_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('role_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('userRoles/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('userRoles/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.userRoles.index') !!}">@lang('userRoles/fields.cancel')</a>
	    </div>
	</div>
</div>
