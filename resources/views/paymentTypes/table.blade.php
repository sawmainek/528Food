<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('paymentTypes/table.name')</th>
    <th width="100px;">@lang('paymentTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($paymentTypes as $paymentType)
        <tr>
            <td>{!! $paymentType->name !!}</td>
            <td>
                <a href="{!! route('administration.paymentTypes.show', [$paymentType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.paymentTypes.edit', [$paymentType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.paymentTypes.delete', [$paymentType->id]) !!}" onclick="return confirm('@lang('paymentTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
