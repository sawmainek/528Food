<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('classsTypes/table.name')</th>
			<th>@lang('classsTypes/table.facilities')</th>
    <th width="100px;">@lang('classsTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($classsTypes as $classsType)
        <tr>
            <td>{!! $classsType->name !!}</td>
			<td>{!! $classsType->facilities !!}</td>
            <td>
                <a href="{!! route('administration.classsTypes.show', [$classsType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.classsTypes.edit', [$classsType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.classsTypes.delete', [$classsType->id]) !!}" onclick="return confirm('@lang('classsTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
