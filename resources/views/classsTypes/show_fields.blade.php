<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('classsTypes/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $classsType->name !!}</p>
        </div>
    </div>

</div>

<!-- Facilities Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('facilities', Lang::get('classsTypes/show_fields.facilities'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $classsType->facilities !!}</p>
        </div>
    </div>

</div>

