<!-- Reviews Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('reviews', Lang::get('hotelReviews/show_fields.reviews'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotelReview->reviews !!}</p>
        </div>
    </div>

</div>

<!-- Ratings Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ratings', Lang::get('hotelReviews/show_fields.ratings'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotelReview->ratings !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('hotelReviews/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotelReview['user']['name'] !!}</p>
        </div>
    </div>

</div>

