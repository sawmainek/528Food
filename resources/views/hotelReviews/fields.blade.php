<!-- Reviews Field -->
<div class="row">
	<div class="form-group{{ $errors->has('reviews') ? ' has-error' : '' }}">
	    {!! Form::label('reviews', Lang::get('hotelReviews/fields.reviews'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('reviews', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('reviews'))
	            <span class="help-block">
	                <strong>{{ $errors->first('reviews') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Ratings Field -->
<div class="row">
	<div class="form-group{{ $errors->has('ratings') ? ' has-error' : '' }}">
	    {!! Form::label('ratings', Lang::get('hotelReviews/fields.ratings'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('ratings', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('ratings'))
	            <span class="help-block">
	                <strong>{{ $errors->first('ratings') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('hotelReviews/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('hotelReviews/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.hotelReviews.index') !!}">@lang('hotelReviews/fields.cancel')</a>
	    </div>
	</div>
</div>
