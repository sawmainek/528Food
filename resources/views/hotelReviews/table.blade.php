<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('hotelReviews/table.reviews')</th>
			<th>@lang('hotelReviews/table.ratings')</th>
			<th>@lang('hotelReviews/table.user_id')</th>
    <th width="100px;">@lang('hotelReviews/table.action')</th>
    </thead>
    <tbody>
    @foreach($hotelReviews as $hotelReview)
        <tr>
            <td>{!! $hotelReview->reviews !!}</td>
			<td>{!! $hotelReview->ratings !!}</td>
			<td>{!! $hotelReview['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.hotelReviews.show', [$hotelReview->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.hotelReviews.edit', [$hotelReview->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.hotelReviews.delete', [$hotelReview->id]) !!}" onclick="return confirm('@lang('hotelReviews/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
