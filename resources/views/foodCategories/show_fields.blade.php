<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('foodCategories/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodCategory->name !!}</p>
        </div>
    </div>

</div>

<!-- Description Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('description', Lang::get('foodCategories/show_fields.description'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodCategory->description !!}</p>
        </div>
    </div>

</div>

<!-- Icon Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('icon', Lang::get('foodCategories/show_fields.icon'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodCategory->icon !!}</p>
        </div>
    </div>

</div>

<!-- Background Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('background', Lang::get('foodCategories/show_fields.background'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodCategory->background !!}</p>
        </div>
    </div>

</div>

<!-- Color Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('color', Lang::get('foodCategories/show_fields.color'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodCategory->color !!}</p>
        </div>
    </div>

</div>

