<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('foodCategories/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Description Field -->
<div class="row">
	<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	    {!! Form::label('description', Lang::get('foodCategories/fields.description'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('description'))
	            <span class="help-block">
	                <strong>{{ $errors->first('description') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Icon Field -->
<div class="row">
	<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
	    {!! Form::label('icon', Lang::get('foodCategories/fields.icon'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($foodCategory))
	                	<img src="{{asset('foodCategories/x400/'.$foodCategory->icon)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('icon') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('icon'))
	            <span class="help-block">
	                <strong>{{ $errors->first('icon') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Background Field -->
<div class="row">
	<div class="form-group{{ $errors->has('background') ? ' has-error' : '' }}">
	    {!! Form::label('background', Lang::get('foodCategories/fields.background'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($foodCategory))
	                	<img src="{{asset('foodCategories/x400/'.$foodCategory->background)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('background') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('background'))
	            <span class="help-block">
	                <strong>{{ $errors->first('background') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Color Field -->
<div class="row">
	<div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
	    {!! Form::label('color', Lang::get('foodCategories/fields.color'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('color', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('color'))
	            <span class="help-block">
	                <strong>{{ $errors->first('color') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('foodCategories/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.foodCategories.index') !!}">@lang('foodCategories/fields.cancel')</a>
	    </div>
	</div>
</div>
