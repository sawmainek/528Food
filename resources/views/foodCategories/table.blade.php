<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodCategories/table.name')</th>
			<th>@lang('foodCategories/table.description')</th>
			<th>@lang('foodCategories/table.icon')</th>
			<th>@lang('foodCategories/table.background')</th>
			<th>@lang('foodCategories/table.color')</th>
    <th width="100px;">@lang('foodCategories/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodCategories as $foodCategory)
        <tr>
            <td>{!! $foodCategory->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($foodCategory->description, 16,'....') !!}</td>
			<td><img width="150" src="{!! asset('foodCategories/x400/'.$foodCategory->icon) !!}"></td>
			<td><img width="150" src="{!! asset('foodCategories/x400/'.$foodCategory->background) !!}"></td>
			<td>{!! $foodCategory->color !!}</td>
            <td>
                <a href="{!! route('administration.foodCategories.show', [$foodCategory->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodCategories.edit', [$foodCategory->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodCategories.delete', [$foodCategory->id]) !!}" onclick="return confirm('@lang('foodCategories/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
