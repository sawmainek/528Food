@extends('layouts.admin')

@section('content')
<div class="container">
    <h1>
        @lang('foodCategories/create.new_model')
    </h1>
    @include('common.errors')
    <div class="widget-container fluid-height clearfix">
        <div class="heading">
            <i class="fa fa-th-list"></i>@lang('foodCategories/create.new_model')
        </div>
        <div class="clearfix">
		    {!! Form::open(['route' => 'administration.foodCategories.store', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal', 'role'=>'form']) !!}

		        @include('foodCategories.fields')

		    {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

