<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('roomTypes/table.name')</th>
			<th>@lang('roomTypes/table.description')</th>
    <th width="100px;">@lang('roomTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($roomTypes as $roomType)
        <tr>
            <td>{!! $roomType->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($roomType->description, 16,'....') !!}</td>
            <td>
                <a href="{!! route('administration.roomTypes.show', [$roomType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.roomTypes.edit', [$roomType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.roomTypes.delete', [$roomType->id]) !!}" onclick="return confirm('@lang('roomTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
