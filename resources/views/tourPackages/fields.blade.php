<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('tourPackages/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Agent Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }}">
	    {!! Form::label('agent_id', Lang::get('tourPackages/fields.agent_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('agent_id', $agents, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('agent_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('agent_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Package Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('package_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('package_type_id', Lang::get('tourPackages/fields.package_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('package_type_id', $tourPackageTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('package_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('package_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Duration Field -->
<div class="row">
	<div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
	    {!! Form::label('duration', Lang::get('tourPackages/fields.duration'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('duration', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('duration'))
	            <span class="help-block">
	                <strong>{{ $errors->first('duration') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Start End Point Field -->
<div class="row">
	<div class="form-group{{ $errors->has('start_end_point') ? ' has-error' : '' }}">
	    {!! Form::label('start_end_point', Lang::get('tourPackages/fields.start_end_point'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('start_end_point', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('start_end_point'))
	            <span class="help-block">
	                <strong>{{ $errors->first('start_end_point') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	    {!! Form::label('price', Lang::get('tourPackages/fields.price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Brief Itinerary Field -->
<div class="row">
	<div class="form-group{{ $errors->has('brief_itinerary') ? ' has-error' : '' }}">
	    {!! Form::label('brief_itinerary', Lang::get('tourPackages/fields.brief_itinerary'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('brief_itinerary', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('brief_itinerary'))
	            <span class="help-block">
	                <strong>{{ $errors->first('brief_itinerary') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('tourPackages/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.tourPackages.index') !!}">@lang('tourPackages/fields.cancel')</a>
	    </div>
	</div>
</div>
