<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('tourPackages/table.name')</th>
			<th>@lang('tourPackages/table.agent_id')</th>
			<th>@lang('tourPackages/table.package_type_id')</th>
			<th>@lang('tourPackages/table.duration')</th>
			<th>@lang('tourPackages/table.start_end_point')</th>
			<th>@lang('tourPackages/table.price')</th>
			<th>@lang('tourPackages/table.brief_itinerary')</th>
    <th width="100px;">@lang('tourPackages/table.action')</th>
    </thead>
    <tbody>
    @foreach($tourPackages as $tourPackage)
        <tr>
            <td>{!! $tourPackage->name !!}</td>
			<td>{!! $tourPackage['agent']['name'] !!}</td>
			<td>{!! $tourPackage['tourPackageType']['name'] !!}</td>
			<td>{!! $tourPackage->duration !!}</td>
			<td>{!! $tourPackage->start_end_point !!}</td>
			<td>{!! $tourPackage->price !!}</td>
			<td>{!! $tourPackage->brief_itinerary !!}</td>
            <td>
                <a href="{!! route('administration.tourPackages.show', [$tourPackage->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.tourPackages.edit', [$tourPackage->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.tourPackages.delete', [$tourPackage->id]) !!}" onclick="return confirm('@lang('tourPackages/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
