<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('tourPackages/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage->name !!}</p>
        </div>
    </div>

</div>

<!-- Agent Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('agent_id', Lang::get('tourPackages/show_fields.agent_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage['agent']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Package Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('package_type_id', Lang::get('tourPackages/show_fields.package_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage['tourPackageType']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Duration Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('duration', Lang::get('tourPackages/show_fields.duration'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage->duration !!}</p>
        </div>
    </div>

</div>

<!-- Start End Point Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('start_end_point', Lang::get('tourPackages/show_fields.start_end_point'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage->start_end_point !!}</p>
        </div>
    </div>

</div>

<!-- Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('price', Lang::get('tourPackages/show_fields.price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage->price !!}</p>
        </div>
    </div>

</div>

<!-- Brief Itinerary Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('brief_itinerary', Lang::get('tourPackages/show_fields.brief_itinerary'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $tourPackage->brief_itinerary !!}</p>
        </div>
    </div>

</div>

