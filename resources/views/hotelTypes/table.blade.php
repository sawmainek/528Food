<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('hotelTypes/table.name')</th>
			<th>@lang('hotelTypes/table.description')</th>
    <th width="100px;">@lang('hotelTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($hotelTypes as $hotelType)
        <tr>
            <td>{!! $hotelType->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($hotelType->description, 16,'....') !!}</td>
            <td>
                <a href="{!! route('administration.hotelTypes.show', [$hotelType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.hotelTypes.edit', [$hotelType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.hotelTypes.delete', [$hotelType->id]) !!}" onclick="return confirm('@lang('hotelTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
