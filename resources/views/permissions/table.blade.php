<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('permissions/table.name')</th>
			<th>@lang('permissions/table.description')</th>
			<th>@lang('permissions/table.role_id')</th>
    <th width="100px;">@lang('permissions/table.action')</th>
    </thead>
    <tbody>
    @foreach($permissions as $permission)
        <tr>
            <td>{!! $permission->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($permission->description, 16,'....') !!}</td>
			<td>{!! $permission['role']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.permissions.show', [$permission->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.permissions.edit', [$permission->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.permissions.delete', [$permission->id]) !!}" onclick="return confirm('@lang('permissions/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
