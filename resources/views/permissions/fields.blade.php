<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('permissions/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Description Field -->
<div class="row">
	<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	    {!! Form::label('description', Lang::get('permissions/fields.description'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('description'))
	            <span class="help-block">
	                <strong>{{ $errors->first('description') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Role Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
	    {!! Form::label('role_id', Lang::get('permissions/fields.role_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('role_id', $roles, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('role_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('role_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('permissions/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.permissions.index') !!}">@lang('permissions/fields.cancel')</a>
	    </div>
	</div>
</div>
