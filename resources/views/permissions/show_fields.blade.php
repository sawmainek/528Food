<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('permissions/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $permission->name !!}</p>
        </div>
    </div>

</div>

<!-- Description Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('description', Lang::get('permissions/show_fields.description'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $permission->description !!}</p>
        </div>
    </div>

</div>

<!-- Role Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('role_id', Lang::get('permissions/show_fields.role_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $permission['role']['name'] !!}</p>
        </div>
    </div>

</div>

