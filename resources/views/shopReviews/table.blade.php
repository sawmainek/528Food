<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('shopReviews/table.reviews')</th>
			<th>@lang('shopReviews/table.ratings')</th>
			<th>@lang('shopReviews/table.user_id')</th>
			<th>@lang('shopReviews/table.shop_id')</th>
    <th width="100px;">@lang('shopReviews/table.action')</th>
    </thead>
    <tbody>
    @foreach($shopReviews as $shopReview)
        <tr>
            <td>{!! $shopReview->reviews !!}</td>
			<td>{!! $shopReview->ratings !!}</td>
			<td>{!! $shopReview['user']['name'] !!}</td>
			<td>{!! $shopReview['shop']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.shopReviews.show', [$shopReview->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.shopReviews.edit', [$shopReview->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.shopReviews.delete', [$shopReview->id]) !!}" onclick="return confirm('@lang('shopReviews/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
