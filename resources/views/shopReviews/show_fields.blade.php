<!-- Reviews Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('reviews', Lang::get('shopReviews/show_fields.reviews'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopReview->reviews !!}</p>
        </div>
    </div>

</div>

<!-- Ratings Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ratings', Lang::get('shopReviews/show_fields.ratings'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopReview->ratings !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('shopReviews/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopReview['user']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Shop Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('shop_id', Lang::get('shopReviews/show_fields.shop_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $shopReview['shop']['name'] !!}</p>
        </div>
    </div>

</div>

