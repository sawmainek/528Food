<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('hotels/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Address Field -->
<div class="row">
	<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
	    {!! Form::label('address', Lang::get('hotels/fields.address'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('address', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('address'))
	            <span class="help-block">
	                <strong>{{ $errors->first('address') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Phone Field -->
<div class="row">
	<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	    {!! Form::label('phone', Lang::get('hotels/fields.phone'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('phone', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('phone'))
	            <span class="help-block">
	                <strong>{{ $errors->first('phone') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Email Field -->
<div class="row">
	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    {!! Form::label('email', Lang::get('hotels/fields.email'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::email('email', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('email'))
	            <span class="help-block">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Agent Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }}">
	    {!! Form::label('agent_id', Lang::get('hotels/fields.agent_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('agent_id', $agents, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('agent_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('agent_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Hotel Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('hotel_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('hotel_type_id', Lang::get('hotels/fields.hotel_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('hotel_type_id', $hotelTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('hotel_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('hotel_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('hotels/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.hotels.index') !!}">@lang('hotels/fields.cancel')</a>
	    </div>
	</div>
</div>
