<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('hotels/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel->name !!}</p>
        </div>
    </div>

</div>

<!-- Address Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('address', Lang::get('hotels/show_fields.address'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel->address !!}</p>
        </div>
    </div>

</div>

<!-- Phone Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('phone', Lang::get('hotels/show_fields.phone'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel->phone !!}</p>
        </div>
    </div>

</div>

<!-- Email Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('email', Lang::get('hotels/show_fields.email'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel->email !!}</p>
        </div>
    </div>

</div>

<!-- Agent Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('agent_id', Lang::get('hotels/show_fields.agent_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel['agent']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Hotel Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('hotel_type_id', Lang::get('hotels/show_fields.hotel_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotel['hotelType']['name'] !!}</p>
        </div>
    </div>

</div>

