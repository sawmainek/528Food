<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('hotels/table.name')</th>
			<th>@lang('hotels/table.address')</th>
			<th>@lang('hotels/table.phone')</th>
			<th>@lang('hotels/table.email')</th>
			<th>@lang('hotels/table.agent_id')</th>
			<th>@lang('hotels/table.hotel_type_id')</th>
    <th width="100px;">@lang('hotels/table.action')</th>
    </thead>
    <tbody>
    @foreach($hotels as $hotel)
        <tr>
            <td>{!! $hotel->name !!}</td>
			<td>{!! $hotel->address !!}</td>
			<td>{!! $hotel->phone !!}</td>
			<td>{!! $hotel->email !!}</td>
			<td>{!! $hotel['agent']['name'] !!}</td>
			<td>{!! $hotel['hotelType']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.hotels.show', [$hotel->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.hotels.edit', [$hotel->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.hotels.delete', [$hotel->id]) !!}" onclick="return confirm('@lang('hotels/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
