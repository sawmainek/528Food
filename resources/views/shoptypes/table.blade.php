<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('shoptypes/table.name')</th>
    <th width="100px;">@lang('shoptypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($shoptypes as $shoptype)
        <tr>
            <td>{!! $shoptype->name !!}</td>
            <td>
                <a href="{!! route('administration.shoptypes.show', [$shoptype->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.shoptypes.edit', [$shoptype->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.shoptypes.delete', [$shoptype->id]) !!}" onclick="return confirm('@lang('shoptypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
