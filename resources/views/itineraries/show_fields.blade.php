<!-- Tour Package Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('tour_package_id', Lang::get('itineraries/show_fields.tour_package_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $itinerary['tourPackage']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Day Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('day', Lang::get('itineraries/show_fields.day'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $itinerary->day !!}</p>
        </div>
    </div>

</div>

<!-- Description Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('description', Lang::get('itineraries/show_fields.description'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $itinerary->description !!}</p>
        </div>
    </div>

</div>

<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('itineraries/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $itinerary->image !!}</p>
        </div>
    </div>

</div>

