<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('itineraries/table.tour_package_id')</th>
			<th>@lang('itineraries/table.day')</th>
			<th>@lang('itineraries/table.description')</th>
			<th>@lang('itineraries/table.image')</th>
    <th width="100px;">@lang('itineraries/table.action')</th>
    </thead>
    <tbody>
    @foreach($itineraries as $itinerary)
        <tr>
            <td>{!! $itinerary['tourPackage']['name'] !!}</td>
			<td>{!! $itinerary->day !!}</td>
			<td>{!! Illuminate\Support\Str::words($itinerary->description, 16,'....') !!}</td>
			<td><img width="150" src="{!! asset('itineraries/x400/'.$itinerary->image) !!}"></td>
            <td>
                <a href="{!! route('administration.itineraries.show', [$itinerary->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.itineraries.edit', [$itinerary->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.itineraries.delete', [$itinerary->id]) !!}" onclick="return confirm('@lang('itineraries/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
