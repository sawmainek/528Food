<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('tourPackageTypes/table.name')</th>
			<th>@lang('tourPackageTypes/table.description')</th>
    <th width="100px;">@lang('tourPackageTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($tourPackageTypes as $tourPackageType)
        <tr>
            <td>{!! $tourPackageType->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($tourPackageType->description, 16,'....') !!}</td>
            <td>
                <a href="{!! route('administration.tourPackageTypes.show', [$tourPackageType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.tourPackageTypes.edit', [$tourPackageType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.tourPackageTypes.delete', [$tourPackageType->id]) !!}" onclick="return confirm('@lang('tourPackageTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
