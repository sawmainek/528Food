<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('orderDetails/table.order_id')</th>
			<th>@lang('orderDetails/table.food_id')</th>
			<th>@lang('orderDetails/table.size_id')</th>
			<th>@lang('orderDetails/table.qty')</th>
			<th>@lang('orderDetails/table.price')</th>
			<th>@lang('orderDetails/table.sub_total')</th>
			<th>@lang('orderDetails/table.note')</th>
    <th width="100px;">@lang('orderDetails/table.action')</th>
    </thead>
    <tbody>
    @foreach($orderDetails as $orderDetail)
        <tr>
            <td>{!! $orderDetail['foodOrder']['order_date'] !!}</td>
			<td>{!! $orderDetail['food']['name'] !!}</td>
			<td>{!! $orderDetail['foodSize']['size'] !!}</td>
			<td>{!! $orderDetail->qty !!}</td>
			<td>{!! $orderDetail->price !!}</td>
			<td>{!! $orderDetail->sub_total !!}</td>
			<td>{!! $orderDetail->note !!}</td>
            <td>
                <a href="{!! route('administration.orderDetails.show', [$orderDetail->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.orderDetails.edit', [$orderDetail->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.orderDetails.delete', [$orderDetail->id]) !!}" onclick="return confirm('@lang('orderDetails/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
