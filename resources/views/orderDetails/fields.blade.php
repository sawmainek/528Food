<!-- Order Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('order_id') ? ' has-error' : '' }}">
	    {!! Form::label('order_id', Lang::get('orderDetails/fields.order_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('order_id', $foodOrders, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('order_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('order_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Food Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('food_id') ? ' has-error' : '' }}">
	    {!! Form::label('food_id', Lang::get('orderDetails/fields.food_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('food_id', $foods, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('food_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('food_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Size Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('size_id') ? ' has-error' : '' }}">
	    {!! Form::label('size_id', Lang::get('orderDetails/fields.size_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('size_id', $foodSizes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('size_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('size_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Qty Field -->
<div class="row">
	<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
	    {!! Form::label('qty', Lang::get('orderDetails/fields.qty'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('qty', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('qty'))
	            <span class="help-block">
	                <strong>{{ $errors->first('qty') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	    {!! Form::label('price', Lang::get('orderDetails/fields.price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Sub Total Field -->
<div class="row">
	<div class="form-group{{ $errors->has('sub_total') ? ' has-error' : '' }}">
	    {!! Form::label('sub_total', Lang::get('orderDetails/fields.sub_total'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('sub_total', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('sub_total'))
	            <span class="help-block">
	                <strong>{{ $errors->first('sub_total') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Note Field -->
<div class="row">
	<div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
	    {!! Form::label('note', Lang::get('orderDetails/fields.note'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('note', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('note'))
	            <span class="help-block">
	                <strong>{{ $errors->first('note') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('orderDetails/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.orderDetails.index') !!}">@lang('orderDetails/fields.cancel')</a>
	    </div>
	</div>
</div>
