<!-- Order Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('order_id', Lang::get('orderDetails/show_fields.order_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail['foodOrder']['order_date'] !!}</p>
        </div>
    </div>

</div>

<!-- Food Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('food_id', Lang::get('orderDetails/show_fields.food_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail['food']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Size Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('size_id', Lang::get('orderDetails/show_fields.size_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail['foodSize']['size'] !!}</p>
        </div>
    </div>

</div>

<!-- Qty Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('qty', Lang::get('orderDetails/show_fields.qty'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail->qty !!}</p>
        </div>
    </div>

</div>

<!-- Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('price', Lang::get('orderDetails/show_fields.price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail->price !!}</p>
        </div>
    </div>

</div>

<!-- Sub Total Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('sub_total', Lang::get('orderDetails/show_fields.sub_total'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail->sub_total !!}</p>
        </div>
    </div>

</div>

<!-- Note Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('note', Lang::get('orderDetails/show_fields.note'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $orderDetail->note !!}</p>
        </div>
    </div>

</div>

