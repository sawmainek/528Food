<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('countries/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $country->name !!}</p>
        </div>
    </div>

</div>

<!-- Name Mm Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name_mm', Lang::get('countries/show_fields.name_mm'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $country->name_mm !!}</p>
        </div>
    </div>

</div>

<!-- Flag Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('flag', Lang::get('countries/show_fields.flag'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $country->flag !!}</p>
        </div>
    </div>

</div>

