<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('countries/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Name Mm Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name_mm') ? ' has-error' : '' }}">
	    {!! Form::label('name_mm', Lang::get('countries/fields.name_mm'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name_mm', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name_mm'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name_mm') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Flag Field -->
<div class="row">
	<div class="form-group{{ $errors->has('flag') ? ' has-error' : '' }}">
	    {!! Form::label('flag', Lang::get('countries/fields.flag'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($country))
	                	<img src="{{asset('countries/x400/'.$country->flag)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('flag') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('flag'))
	            <span class="help-block">
	                <strong>{{ $errors->first('flag') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('countries/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.countries.index') !!}">@lang('countries/fields.cancel')</a>
	    </div>
	</div>
</div>
