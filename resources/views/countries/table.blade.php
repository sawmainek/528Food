<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('countries/table.name')</th>
			<th>@lang('countries/table.name_mm')</th>
			<th>@lang('countries/table.flag')</th>
    <th width="100px;">@lang('countries/table.action')</th>
    </thead>
    <tbody>
    @foreach($countries as $country)
        <tr>
            <td>{!! $country->name !!}</td>
			<td>{!! $country->name_mm !!}</td>
			<td><img width="150" src="{!! asset('countries/x400/'.$country->flag) !!}"></td>
            <td>
                <a href="{!! route('administration.countries.show', [$country->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.countries.edit', [$country->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.countries.delete', [$country->id]) !!}" onclick="return confirm('@lang('countries/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
