<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('ticketOperators/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticketOperator['agent']['id'] !!}</p>
        </div>
    </div>

</div>

<!-- Operator Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('operator_type_id', Lang::get('ticketOperators/show_fields.operator_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticketOperator['operatorType']['name'] !!}</p>
        </div>
    </div>

</div>

