<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('ticketOperators/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('name', $agents, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Operator Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('operator_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('operator_type_id', Lang::get('ticketOperators/fields.operator_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('operator_type_id', $operatorTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('operator_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('operator_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('ticketOperators/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.ticketOperators.index') !!}">@lang('ticketOperators/fields.cancel')</a>
	    </div>
	</div>
</div>
