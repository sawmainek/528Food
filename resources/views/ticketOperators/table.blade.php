<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('ticketOperators/table.name')</th>
			<th>@lang('ticketOperators/table.operator_type_id')</th>
    <th width="100px;">@lang('ticketOperators/table.action')</th>
    </thead>
    <tbody>
    @foreach($ticketOperators as $ticketOperator)
        <tr>
            <td>{!! $ticketOperator['agent']['id'] !!}</td>
			<td>{!! $ticketOperator['operatorType']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.ticketOperators.show', [$ticketOperator->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.ticketOperators.edit', [$ticketOperator->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.ticketOperators.delete', [$ticketOperator->id]) !!}" onclick="return confirm('@lang('ticketOperators/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
