<!-- Reviews Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('reviews', Lang::get('foodReviews/show_fields.reviews'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodReview->reviews !!}</p>
        </div>
    </div>

</div>

<!-- Ratings Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ratings', Lang::get('foodReviews/show_fields.ratings'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodReview->ratings !!}</p>
        </div>
    </div>

</div>

<!-- Food Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('food_id', Lang::get('foodReviews/show_fields.food_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodReview['food']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('foodReviews/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodReview['user']['name'] !!}</p>
        </div>
    </div>

</div>

