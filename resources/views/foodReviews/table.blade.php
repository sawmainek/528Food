<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodReviews/table.reviews')</th>
			<th>@lang('foodReviews/table.ratings')</th>
			<th>@lang('foodReviews/table.food_id')</th>
			<th>@lang('foodReviews/table.user_id')</th>
    <th width="100px;">@lang('foodReviews/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodReviews as $foodReview)
        <tr>
            <td>{!! $foodReview->reviews !!}</td>
			<td>{!! $foodReview->ratings !!}</td>
			<td>{!! $foodReview['food']['name'] !!}</td>
			<td>{!! $foodReview['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.foodReviews.show', [$foodReview->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodReviews.edit', [$foodReview->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodReviews.delete', [$foodReview->id]) !!}" onclick="return confirm('@lang('foodReviews/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
