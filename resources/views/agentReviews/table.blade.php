<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('agentReviews/table.reviews')</th>
			<th>@lang('agentReviews/table.ratings')</th>
			<th>@lang('agentReviews/table.user_id')</th>
    <th width="100px;">@lang('agentReviews/table.action')</th>
    </thead>
    <tbody>
    @foreach($agentReviews as $agentReview)
        <tr>
            <td>{!! $agentReview->reviews !!}</td>
			<td>{!! $agentReview->ratings !!}</td>
			<td>{!! $agentReview['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.agentReviews.show', [$agentReview->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.agentReviews.edit', [$agentReview->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.agentReviews.delete', [$agentReview->id]) !!}" onclick="return confirm('@lang('agentReviews/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
