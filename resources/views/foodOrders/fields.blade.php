<!-- Order Date Field -->
<div class="row">
	<div class="form-group{{ $errors->has('order_date') ? ' has-error' : '' }}">
	    {!! Form::label('order_date', Lang::get('foodOrders/fields.order_date'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('order_date', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('order_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('order_date') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- User Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
	    {!! Form::label('user_id', Lang::get('foodOrders/fields.user_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('user_id', $users, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('user_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('user_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Paid Field -->
<div class="row">
	<div class="form-group{{ $errors->has('paid') ? ' has-error' : '' }}">
	    {!! Form::label('paid', Lang::get('foodOrders/fields.paid'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('paid', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('paid'))
	            <span class="help-block">
	                <strong>{{ $errors->first('paid') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Total Amount Field -->
<div class="row">
	<div class="form-group{{ $errors->has('total_amount') ? ' has-error' : '' }}">
	    {!! Form::label('total_amount', Lang::get('foodOrders/fields.total_amount'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('total_amount', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('total_amount'))
	            <span class="help-block">
	                <strong>{{ $errors->first('total_amount') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('foodOrders/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.foodOrders.index') !!}">@lang('foodOrders/fields.cancel')</a>
	    </div>
	</div>
</div>
