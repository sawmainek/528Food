<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodOrders/table.order_date')</th>
			<th>@lang('foodOrders/table.user_id')</th>
			<th>@lang('foodOrders/table.paid')</th>
			<th>@lang('foodOrders/table.total_amount')</th>
    <th width="100px;">@lang('foodOrders/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodOrders as $foodOrder)
        <tr>
            <td>{!! $foodOrder->order_date !!}</td>
			<td>{!! $foodOrder['user']['name'] !!}</td>
			<td>{!! $foodOrder->paid !!}</td>
			<td>{!! $foodOrder->total_amount !!}</td>
            <td>
                <a href="{!! route('administration.foodOrders.show', [$foodOrder->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodOrders.edit', [$foodOrder->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodOrders.delete', [$foodOrder->id]) !!}" onclick="return confirm('@lang('foodOrders/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
