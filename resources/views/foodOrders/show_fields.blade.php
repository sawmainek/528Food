<!-- Order Date Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('order_date', Lang::get('foodOrders/show_fields.order_date'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrder->order_date !!}</p>
        </div>
    </div>

</div>

<!-- User Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('user_id', Lang::get('foodOrders/show_fields.user_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrder['user']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Paid Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('paid', Lang::get('foodOrders/show_fields.paid'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrder->paid !!}</p>
        </div>
    </div>

</div>

<!-- Total Amount Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('total_amount', Lang::get('foodOrders/show_fields.total_amount'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodOrder->total_amount !!}</p>
        </div>
    </div>

</div>

