<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('users/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->name !!}</p>
        </div>
    </div>

</div>

<!-- Email Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('email', Lang::get('users/show_fields.email'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->email !!}</p>
        </div>
    </div>

</div>

<!-- Password Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('password', Lang::get('users/show_fields.password'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->password !!}</p>
        </div>
    </div>

</div>

<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('users/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->image !!}</p>
        </div>
    </div>

</div>

<!-- Phone Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('phone', Lang::get('users/show_fields.phone'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->phone !!}</p>
        </div>
    </div>

</div>

<!-- Lat Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('lat', Lang::get('users/show_fields.lat'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->lat !!}</p>
        </div>
    </div>

</div>

<!-- Lng Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('lng', Lang::get('users/show_fields.lng'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->lng !!}</p>
        </div>
    </div>

</div>

<!-- On Session Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('on_session', Lang::get('users/show_fields.on_session'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->on_session !!}</p>
        </div>
    </div>

</div>

<!-- Session Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('session_id', Lang::get('users/show_fields.session_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $user->session_id !!}</p>
        </div>
    </div>

</div>

