<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('users/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Email Field -->
<div class="row">
	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    {!! Form::label('email', Lang::get('users/fields.email'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::email('email', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('email'))
	            <span class="help-block">
	                <strong>{{ $errors->first('email') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Password Field -->
<div class="row">
	<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	    {!! Form::label('password', Lang::get('users/fields.password'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::password('password', ['class' => 'form-control']) !!}
	        @if ($errors->has('password'))
	            <span class="help-block">
	                <strong>{{ $errors->first('password') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Image Field -->
<div class="row">
	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
	    {!! Form::label('image', Lang::get('users/fields.image'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                	@if(isset($user))
	                	<img src="{{asset('users/x400/'.$user->image)}}">
	                @else
	                	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image">
	                @endif
                </div>
                <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                <div>
                  	<span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
			{!! Form::file('image') !!}</span><a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                </div>
            </div>
	        @if ($errors->has('image'))
	            <span class="help-block">
	                <strong>{{ $errors->first('image') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Phone Field -->
<div class="row">
	<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	    {!! Form::label('phone', Lang::get('users/fields.phone'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('phone', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('phone'))
	            <span class="help-block">
	                <strong>{{ $errors->first('phone') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Lat Field -->
<div class="row">
	<div class="form-group{{ $errors->has('lat') ? ' has-error' : '' }}">
	    {!! Form::label('lat', Lang::get('users/fields.lat'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('lat', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('lat'))
	            <span class="help-block">
	                <strong>{{ $errors->first('lat') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Lng Field -->
<div class="row">
	<div class="form-group{{ $errors->has('lng') ? ' has-error' : '' }}">
	    {!! Form::label('lng', Lang::get('users/fields.lng'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('lng', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('lng'))
	            <span class="help-block">
	                <strong>{{ $errors->first('lng') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- On Session Field -->
<div class="row">
	<div class="form-group{{ $errors->has('on_session') ? ' has-error' : '' }}">
	    {!! Form::label('on_session', Lang::get('users/fields.on_session'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			<div class="radio-inline">
						<label>
									{!! Form::radio('on_session', 'Off', null) !!} <span>Off</span>
						</label>
			</div>
			<div class="radio-inline">
						<label>
									{!! Form::radio('on_session', 'On', null) !!} <span>On</span>
						</label>
			</div>
	        @if ($errors->has('on_session'))
	            <span class="help-block">
	                <strong>{{ $errors->first('on_session') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Session Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('session_id') ? ' has-error' : '' }}">
	    {!! Form::label('session_id', Lang::get('users/fields.session_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('session_id', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('session_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('session_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('users/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.users.index') !!}">@lang('users/fields.cancel')</a>
	    </div>
	</div>
</div>
