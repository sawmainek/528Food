<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('users/table.name')</th>
			<th>@lang('users/table.email')</th>
			<th class="hidden">@lang('users/table.password')</th>
			<th>@lang('users/table.image')</th>
			<th>@lang('users/table.phone')</th>
			<th>@lang('users/table.lat')/@lang('users/table.lng')</th>
			<th>@lang('users/table.on_session')</th>
			<th>@lang('users/table.session_id')</th>
    <th width="100px;">@lang('users/table.action')</th>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! $user->name !!}</td>
			<td>{!! $user->email !!}</td>
			<td class="hidden">{!! $user->password !!}</td>
			<td><img width="150" src="{!! asset('users/x400/'.$user->image) !!}"></td>
			<td>{!! $user->phone !!}</td>
			<td>{!! $user->lat !!},{!! $user->lng !!}</td>
			<td>{!! $user->on_session !!}</td>
			<td>{!! $user->session_id !!}</td>
            <td>
                <a href="{!! route('administration.users.show', [$user->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.users.edit', [$user->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.users.delete', [$user->id]) !!}" onclick="return confirm('@lang('users/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
