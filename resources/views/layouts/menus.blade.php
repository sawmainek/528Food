@foreach(Auth::user()->permission as $permission)

	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodCategories/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodCategories') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodCategory::count() !!}</div>
			<p>@lang('foodCategories/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foods/index.model_name'))
	<li>
		<a href="{{ asset('administration/foods') }}">
			<div class="notifications label label-warning">{!! App\Models\Food::count() !!}</div>
			<p>@lang('foods/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodSizes/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodSizes') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodSize::count() !!}</div>
			<p>@lang('foodSizes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('ingredients/index.model_name'))
	<li>
		<a href="{{ asset('administration/ingredients') }}">
			<div class="notifications label label-warning">{!! App\Models\Ingredient::count() !!}</div>
			<p>@lang('ingredients/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodPhotos/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodPhotos') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodPhoto::count() !!}</div>
			<p>@lang('foodPhotos/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodReviews/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodReviews') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodReview::count() !!}</div>
			<p>@lang('foodReviews/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('shoptypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/shoptypes') }}">
			<div class="notifications label label-warning">{!! App\Models\Shoptype::count() !!}</div>
			<p>@lang('shoptypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('shops/index.model_name'))
	<li>
		<a href="{{ asset('administration/shops') }}">
			<div class="notifications label label-warning">{!! App\Models\Shop::count() !!}</div>
			<p>@lang('shops/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('shopReviews/index.model_name'))
	<li>
		<a href="{{ asset('administration/shopReviews') }}">
			<div class="notifications label label-warning">{!! App\Models\ShopReview::count() !!}</div>
			<p>@lang('shopReviews/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodOrders/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodOrders') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodOrder::count() !!}</div>
			<p>@lang('foodOrders/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('orderDetails/index.model_name'))
	<li>
		<a href="{{ asset('administration/orderDetails') }}">
			<div class="notifications label label-warning">{!! App\Models\OrderDetail::count() !!}</div>
			<p>@lang('orderDetails/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('paymentTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/paymentTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\PaymentType::count() !!}</div>
			<p>@lang('paymentTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('foodOrderPayments/index.model_name'))
	<li>
		<a href="{{ asset('administration/foodOrderPayments') }}">
			<div class="notifications label label-warning">{!! App\Models\FoodOrderPayment::count() !!}</div>
			<p>@lang('foodOrderPayments/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('shopUsers/index.model_name'))
	<li>
		<a href="{{ asset('administration/shopUsers') }}">
			<div class="notifications label label-warning">{!! App\Models\ShopUser::count() !!}</div>
			<p>@lang('shopUsers/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('agents/index.model_name'))
	<li>
		<a href="{{ asset('administration/agents') }}">
			<div class="notifications label label-warning">{!! App\Models\Agent::count() !!}</div>
			<p>@lang('agents/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('agentUsers/index.model_name'))
	<li>
		<a href="{{ asset('administration/agentUsers') }}">
			<div class="notifications label label-warning">{!! App\Models\AgentUser::count() !!}</div>
			<p>@lang('agentUsers/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('agentReviews/index.model_name'))
	<li>
		<a href="{{ asset('administration/agentReviews') }}">
			<div class="notifications label label-warning">{!! App\Models\AgentReview::count() !!}</div>
			<p>@lang('agentReviews/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('roomTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/roomTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\RoomType::count() !!}</div>
			<p>@lang('roomTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('hotelTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/hotelTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\HotelType::count() !!}</div>
			<p>@lang('hotelTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('hotels/index.model_name'))
	<li>
		<a href="{{ asset('administration/hotels') }}">
			<div class="notifications label label-warning">{!! App\Models\Hotel::count() !!}</div>
			<p>@lang('hotels/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('roomPrices/index.model_name'))
	<li>
		<a href="{{ asset('administration/roomPrices') }}">
			<div class="notifications label label-warning">{!! App\Models\RoomPrice::count() !!}</div>
			<p>@lang('roomPrices/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('hotelPhotos/index.model_name'))
	<li>
		<a href="{{ asset('administration/hotelPhotos') }}">
			<div class="notifications label label-warning">{!! App\Models\HotelPhoto::count() !!}</div>
			<p>@lang('hotelPhotos/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('hotelReviews/index.model_name'))
	<li>
		<a href="{{ asset('administration/hotelReviews') }}">
			<div class="notifications label label-warning">{!! App\Models\HotelReview::count() !!}</div>
			<p>@lang('hotelReviews/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('ticketTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/ticketTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\TicketType::count() !!}</div>
			<p>@lang('ticketTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('tickets/index.model_name'))
	<li>
		<a href="{{ asset('administration/tickets') }}">
			<div class="notifications label label-warning">{!! App\Models\Ticket::count() !!}</div>
			<p>@lang('tickets/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('classsTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/classsTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\ClasssType::count() !!}</div>
			<p>@lang('classsTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('operatorTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/operatorTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\OperatorType::count() !!}</div>
			<p>@lang('operatorTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('ticketOperators/index.model_name'))
	<li>
		<a href="{{ asset('administration/ticketOperators') }}">
			<div class="notifications label label-warning">{!! App\Models\TicketOperator::count() !!}</div>
			<p>@lang('ticketOperators/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('tourPackageTypes/index.model_name'))
	<li>
		<a href="{{ asset('administration/tourPackageTypes') }}">
			<div class="notifications label label-warning">{!! App\Models\TourPackageType::count() !!}</div>
			<p>@lang('tourPackageTypes/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('tourPackages/index.model_name'))
	<li>
		<a href="{{ asset('administration/tourPackages') }}">
			<div class="notifications label label-warning">{!! App\Models\TourPackage::count() !!}</div>
			<p>@lang('tourPackages/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('itineraries/index.model_name'))
	<li>
		<a href="{{ asset('administration/itineraries') }}">
			<div class="notifications label label-warning">{!! App\Models\Itinerary::count() !!}</div>
			<p>@lang('itineraries/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('countries/index.model_name'))
	<li>
		<a href="{{ asset('administration/countries') }}">
			<div class="notifications label label-warning">{!! App\Models\Country::count() !!}</div>
			<p>@lang('countries/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('cities/index.model_name'))
	<li>
		<a href="{{ asset('administration/cities') }}">
			<div class="notifications label label-warning">{!! App\Models\City::count() !!}</div>
			<p>@lang('cities/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('users/index.model_name'))
	<li>
		<a href="{{ asset('administration/users') }}">
			<div class="notifications label label-warning">{!! App\Models\User::count() !!}</div>
			<p>@lang('users/index.model_name')</p>
		</a>
	</li>

	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('userRoles/index.model_name'))
	<li>
		<a href="{{ asset('administration/userRoles') }}">
			<div class="notifications label label-warning">{!! App\Models\UserRole::count() !!}</div>
			<p>@lang('userRoles/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('roles/index.model_name'))
	<li>
		<a href="{{ asset('administration/roles') }}">
			<div class="notifications label label-warning">{!! App\Models\Role::count() !!}</div>
			<p>@lang('roles/index.model_name')</p>
		</a>
	</li>
	@endif
	@if(Auth::user()->userRole->role_id == $permission->role_id && $permission->name == Lang::get('permissions/index.model_name'))
	<li>
		<a href="{{ asset('administration/permissions') }}">
			<div class="notifications label label-warning">{!! App\Models\Permission::count() !!}</div>
			<p>@lang('permissions/index.model_name')</p>
		</a>
	</li>
	@endif
@endforeach


<li>
	<a href="{{ asset('administration/userMessages') }}">
		<div class="notifications label label-warning">{!! App\Models\UserMessage::count() !!}</div>
		<p>@lang('userMessages/index.model_name')</p>
	</a>
</li>

<li>
	<a href="{{ asset('administration/userLikes') }}">
		<div class="notifications label label-warning">{!! App\Models\UserLike::count() !!}</div>
		<p>@lang('userLikes/index.model_name')</p>
	</a>
</li>