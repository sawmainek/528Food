<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('hotelPhotos/table.hotel_id')</th>
			<th>@lang('hotelPhotos/table.image')</th>
    <th width="100px;">@lang('hotelPhotos/table.action')</th>
    </thead>
    <tbody>
    @foreach($hotelPhotos as $hotelPhoto)
        <tr>
            <td>{!! $hotelPhoto['hotel']['name'] !!}</td>
			<td><img width="150" src="{!! asset('hotelPhotos/x400/'.$hotelPhoto->image) !!}"></td>
            <td>
                <a href="{!! route('administration.hotelPhotos.show', [$hotelPhoto->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.hotelPhotos.edit', [$hotelPhoto->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.hotelPhotos.delete', [$hotelPhoto->id]) !!}" onclick="return confirm('@lang('hotelPhotos/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
