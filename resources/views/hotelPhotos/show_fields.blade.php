<!-- Hotel Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('hotel_id', Lang::get('hotelPhotos/show_fields.hotel_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotelPhoto['hotel']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('hotelPhotos/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $hotelPhoto->image !!}</p>
        </div>
    </div>

</div>

