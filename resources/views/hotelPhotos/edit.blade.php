@extends('layouts.admin')

@section('content')
<div class="container">

	<h1>
        @lang('hotelPhotos/edit.edit_model')
    </h1>

    @include('common.errors')

    <div class="widget-container fluid-height clearfix">
        <div class="heading">
            <i class="fa fa-th-list"></i>@lang('hotelPhotos/edit.edit_model')
        </div>
        <div class="clearfix">

		    {!! Form::model($hotelPhoto, ['route' => ['administration.hotelPhotos.update', $hotelPhoto->id], 'enctype'=>'multipart/form-data', 'method' => 'patch', 'class'=>'form-horizontal', 'role'=>'form']) !!}

		        @include('hotelPhotos.fields')

		    {!! Form::close() !!}
		    
		</div>
	</div>
</div>
@endsection
