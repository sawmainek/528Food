<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodSizes/table.size')</th>
			<th>@lang('foodSizes/table.food_id')</th>
			<th>@lang('foodSizes/table.price')</th>
    <th width="100px;">@lang('foodSizes/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodSizes as $foodSize)
        <tr>
            <td>{!! $foodSize->size !!}</td>
			<td>{!! $foodSize['food']['name'] !!}</td>
			<td>{!! $foodSize->price !!}</td>
            <td>
                <a href="{!! route('administration.foodSizes.show', [$foodSize->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodSizes.edit', [$foodSize->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodSizes.delete', [$foodSize->id]) !!}" onclick="return confirm('@lang('foodSizes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
