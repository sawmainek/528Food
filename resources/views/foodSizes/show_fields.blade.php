<!-- Size Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('size', Lang::get('foodSizes/show_fields.size'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodSize->size !!}</p>
        </div>
    </div>

</div>

<!-- Food Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('food_id', Lang::get('foodSizes/show_fields.food_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodSize['food']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('price', Lang::get('foodSizes/show_fields.price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodSize->price !!}</p>
        </div>
    </div>

</div>

