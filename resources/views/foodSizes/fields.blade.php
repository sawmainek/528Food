<!-- Size Field -->
<div class="row">
	<div class="form-group{{ $errors->has('size') ? ' has-error' : '' }}">
	    {!! Form::label('size', Lang::get('foodSizes/fields.size'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('size', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('size'))
	            <span class="help-block">
	                <strong>{{ $errors->first('size') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Food Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('food_id') ? ' has-error' : '' }}">
	    {!! Form::label('food_id', Lang::get('foodSizes/fields.food_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('food_id', $foods, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('food_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('food_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	    {!! Form::label('price', Lang::get('foodSizes/fields.price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('foodSizes/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.foodSizes.index') !!}">@lang('foodSizes/fields.cancel')</a>
	    </div>
	</div>
</div>
