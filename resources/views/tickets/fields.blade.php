<!-- Ticket Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('ticket_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('ticket_type_id', Lang::get('tickets/fields.ticket_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('ticket_type_id', $ticketTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('ticket_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('ticket_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Operator Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('operator_id') ? ' has-error' : '' }}">
	    {!! Form::label('operator_id', Lang::get('tickets/fields.operator_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('operator_id', $ticketOperators, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('operator_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('operator_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Agent Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }}">
	    {!! Form::label('agent_id', Lang::get('tickets/fields.agent_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('agent_id', $agents, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('agent_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('agent_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- From City Field -->
<div class="row">
	<div class="form-group{{ $errors->has('from_city') ? ' has-error' : '' }}">
	    {!! Form::label('from_city', Lang::get('tickets/fields.from_city'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('from_city', $cities, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('from_city'))
	            <span class="help-block">
	                <strong>{{ $errors->first('from_city') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- To City Field -->
<div class="row">
	<div class="form-group{{ $errors->has('to_city') ? ' has-error' : '' }}">
	    {!! Form::label('to_city', Lang::get('tickets/fields.to_city'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('to_city', $cities, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('to_city'))
	            <span class="help-block">
	                <strong>{{ $errors->first('to_city') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- From Time Field -->
<div class="row">
	<div class="form-group{{ $errors->has('from_time') ? ' has-error' : '' }}">
	    {!! Form::label('from_time', Lang::get('tickets/fields.from_time'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('from_time', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('from_time'))
	            <span class="help-block">
	                <strong>{{ $errors->first('from_time') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- To Time Field -->
<div class="row">
	<div class="form-group{{ $errors->has('to_time') ? ' has-error' : '' }}">
	    {!! Form::label('to_time', Lang::get('tickets/fields.to_time'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('to_time', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('to_time'))
	            <span class="help-block">
	                <strong>{{ $errors->first('to_time') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- From Country Field -->
<div class="row">
	<div class="form-group{{ $errors->has('from_country') ? ' has-error' : '' }}">
	    {!! Form::label('from_country', Lang::get('tickets/fields.from_country'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('from_country', $countries, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('from_country'))
	            <span class="help-block">
	                <strong>{{ $errors->first('from_country') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- To Country Field -->
<div class="row">
	<div class="form-group{{ $errors->has('to_country') ? ' has-error' : '' }}">
	    {!! Form::label('to_country', Lang::get('tickets/fields.to_country'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('to_country', $countries, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('to_country'))
	            <span class="help-block">
	                <strong>{{ $errors->first('to_country') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
	    {!! Form::label('price', Lang::get('tickets/fields.price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Return Price Field -->
<div class="row">
	<div class="form-group{{ $errors->has('return_price') ? ' has-error' : '' }}">
	    {!! Form::label('return_price', Lang::get('tickets/fields.return_price'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('return_price', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('return_price'))
	            <span class="help-block">
	                <strong>{{ $errors->first('return_price') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Class Type Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('class_type_id') ? ' has-error' : '' }}">
	    {!! Form::label('class_type_id', Lang::get('tickets/fields.class_type_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('class_type_id', $classsTypes, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('class_type_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('class_type_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Currency Unit Field -->
<div class="row">
	<div class="form-group{{ $errors->has('currency_unit') ? ' has-error' : '' }}">
	    {!! Form::label('currency_unit', Lang::get('tickets/fields.currency_unit'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('currency_unit', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('currency_unit'))
	            <span class="help-block">
	                <strong>{{ $errors->first('currency_unit') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('tickets/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.tickets.index') !!}">@lang('tickets/fields.cancel')</a>
	    </div>
	</div>
</div>
