<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('tickets/table.ticket_type_id')</th>
			<th>@lang('tickets/table.operator_id')</th>
			<th>@lang('tickets/table.agent_id')</th>
			<th>@lang('tickets/table.from_city')</th>
			<th>@lang('tickets/table.to_city')</th>
			<th>@lang('tickets/table.from_time')</th>
			<th>@lang('tickets/table.to_time')</th>
			<th>@lang('tickets/table.from_country')</th>
			<th>@lang('tickets/table.to_country')</th>
			<th>@lang('tickets/table.price')</th>
			<th>@lang('tickets/table.return_price')</th>
			<th>@lang('tickets/table.class_type_id')</th>
			<th>@lang('tickets/table.currency_unit')</th>
    <th width="100px;">@lang('tickets/table.action')</th>
    </thead>
    <tbody>
    @foreach($tickets as $ticket)
        <tr>
            <td>{!! $ticket['ticketType']['name'] !!}</td>
			<td>{!! $ticket['ticketOperator']['name'] !!}</td>
			<td>{!! $ticket['agent']['name'] !!}</td>
			<td>{!! $ticket['city']['name'] !!}</td>
			<td>{!! $ticket['city']['name'] !!}</td>
			<td>{!! $ticket->from_time !!}</td>
			<td>{!! $ticket->to_time !!}</td>
			<td>{!! $ticket['country']['name'] !!}</td>
			<td>{!! $ticket['country']['name'] !!}</td>
			<td>{!! $ticket->price !!}</td>
			<td>{!! $ticket->return_price !!}</td>
			<td>{!! $ticket['classsType']['name'] !!}</td>
			<td>{!! $ticket->currency_unit !!}</td>
            <td>
                <a href="{!! route('administration.tickets.show', [$ticket->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.tickets.edit', [$ticket->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.tickets.delete', [$ticket->id]) !!}" onclick="return confirm('@lang('tickets/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
