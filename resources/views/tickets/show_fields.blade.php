<!-- Ticket Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ticket_type_id', Lang::get('tickets/show_fields.ticket_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['ticketType']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Operator Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('operator_id', Lang::get('tickets/show_fields.operator_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['ticketOperator']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Agent Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('agent_id', Lang::get('tickets/show_fields.agent_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['agent']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- From City Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('from_city', Lang::get('tickets/show_fields.from_city'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['city']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- To City Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('to_city', Lang::get('tickets/show_fields.to_city'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['city']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- From Time Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('from_time', Lang::get('tickets/show_fields.from_time'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket->from_time !!}</p>
        </div>
    </div>

</div>

<!-- To Time Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('to_time', Lang::get('tickets/show_fields.to_time'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket->to_time !!}</p>
        </div>
    </div>

</div>

<!-- From Country Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('from_country', Lang::get('tickets/show_fields.from_country'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['country']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- To Country Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('to_country', Lang::get('tickets/show_fields.to_country'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['country']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('price', Lang::get('tickets/show_fields.price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket->price !!}</p>
        </div>
    </div>

</div>

<!-- Return Price Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('return_price', Lang::get('tickets/show_fields.return_price'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket->return_price !!}</p>
        </div>
    </div>

</div>

<!-- Class Type Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('class_type_id', Lang::get('tickets/show_fields.class_type_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket['classsType']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Currency Unit Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('currency_unit', Lang::get('tickets/show_fields.currency_unit'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticket->currency_unit !!}</p>
        </div>
    </div>

</div>

