<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('ingredients/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Food Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('food_id') ? ' has-error' : '' }}">
	    {!! Form::label('food_id', Lang::get('ingredients/fields.food_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('food_id', $foods, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('food_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('food_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Ingredient Amt Field -->
<div class="row">
	<div class="form-group{{ $errors->has('ingredient_amt') ? ' has-error' : '' }}">
	    {!! Form::label('ingredient_amt', Lang::get('ingredients/fields.ingredient_amt'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::number('ingredient_amt', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('ingredient_amt'))
	            <span class="help-block">
	                <strong>{{ $errors->first('ingredient_amt') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Ingredient Unit Field -->
<div class="row">
	<div class="form-group{{ $errors->has('ingredient_unit') ? ' has-error' : '' }}">
	    {!! Form::label('ingredient_unit', Lang::get('ingredients/fields.ingredient_unit'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('ingredient_unit', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('ingredient_unit'))
	            <span class="help-block">
	                <strong>{{ $errors->first('ingredient_unit') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('ingredients/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.ingredients.index') !!}">@lang('ingredients/fields.cancel')</a>
	    </div>
	</div>
</div>
