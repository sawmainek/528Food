<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('ingredients/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ingredient->name !!}</p>
        </div>
    </div>

</div>

<!-- Food Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('food_id', Lang::get('ingredients/show_fields.food_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ingredient['food']['name'] !!}</p>
        </div>
    </div>

</div>

<!-- Ingredient Amt Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ingredient_amt', Lang::get('ingredients/show_fields.ingredient_amt'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ingredient->ingredient_amt !!}</p>
        </div>
    </div>

</div>

<!-- Ingredient Unit Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('ingredient_unit', Lang::get('ingredients/show_fields.ingredient_unit'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ingredient->ingredient_unit !!}</p>
        </div>
    </div>

</div>

