<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('ingredients/table.name')</th>
			<th>@lang('ingredients/table.food_id')</th>
			<th>@lang('ingredients/table.ingredient_amt')</th>
			<th>@lang('ingredients/table.ingredient_unit')</th>
    <th width="100px;">@lang('ingredients/table.action')</th>
    </thead>
    <tbody>
    @foreach($ingredients as $ingredient)
        <tr>
            <td>{!! $ingredient->name !!}</td>
			<td>{!! $ingredient['food']['name'] !!}</td>
			<td>{!! $ingredient->ingredient_amt !!}</td>
			<td>{!! $ingredient->ingredient_unit !!}</td>
            <td>
                <a href="{!! route('administration.ingredients.show', [$ingredient->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.ingredients.edit', [$ingredient->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.ingredients.delete', [$ingredient->id]) !!}" onclick="return confirm('@lang('ingredients/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
