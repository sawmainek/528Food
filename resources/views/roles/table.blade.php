<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('roles/table.name')</th>
			<th>@lang('roles/table.description')</th>
    <th width="100px;">@lang('roles/table.action')</th>
    </thead>
    <tbody>
    @foreach($roles as $role)
        <tr>
            <td>{!! $role->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($role->description, 16,'....') !!}</td>
            <td>
                <a href="{!! route('administration.roles.show', [$role->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.roles.edit', [$role->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.roles.delete', [$role->id]) !!}" onclick="return confirm('@lang('roles/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
