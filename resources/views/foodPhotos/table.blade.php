<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('foodPhotos/table.image')</th>
			<th>@lang('foodPhotos/table.food_id')</th>
    <th width="100px;">@lang('foodPhotos/table.action')</th>
    </thead>
    <tbody>
    @foreach($foodPhotos as $foodPhoto)
        <tr>
            <td><img width="150" src="{!! asset('foodPhotos/x400/'.$foodPhoto->image) !!}"></td>
			<td>{!! $foodPhoto['food']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.foodPhotos.show', [$foodPhoto->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.foodPhotos.edit', [$foodPhoto->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.foodPhotos.delete', [$foodPhoto->id]) !!}" onclick="return confirm('@lang('foodPhotos/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
