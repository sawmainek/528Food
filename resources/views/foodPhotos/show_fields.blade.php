<!-- Image Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('image', Lang::get('foodPhotos/show_fields.image'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodPhoto->image !!}</p>
        </div>
    </div>

</div>

<!-- Food Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('food_id', Lang::get('foodPhotos/show_fields.food_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $foodPhoto['food']['name'] !!}</p>
        </div>
    </div>

</div>

