<!-- Name Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    {!! Form::label('name', Lang::get('cities/fields.name'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Name Mm Field -->
<div class="row">
	<div class="form-group{{ $errors->has('name_mm') ? ' has-error' : '' }}">
	    {!! Form::label('name_mm', Lang::get('cities/fields.name_mm'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	        
			{!! Form::text('name_mm', null, ['class' => 'form-control']) !!}
	        @if ($errors->has('name_mm'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name_mm') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>

<!-- Country Id Field -->
<div class="row">
	<div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
	    {!! Form::label('country_id', Lang::get('cities/fields.country_id'),['class' => 'col-sm-4 col-lg-2 control-label']) !!}
	    <div class="col-sm-6 col-lg-6"> 
	    	{!! Form::select('country_id', $countries, null, ['class' => 'select2able']) !!}
	        @if ($errors->has('country_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('country_id') }}</strong>
	            </span>
	        @endif
	    </div>
	</div>

</div>


<!-- Submit Field -->
<div class="row">
	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-4 col-md-offset-4 col-lg-offset-2">
		    {!! Form::submit(Lang::get('cities/fields.save'), ['class' => 'btn btn-primary']) !!}
		    <a class="btn btn-default-outline" href="{!! route('administration.cities.index') !!}">@lang('cities/fields.cancel')</a>
	    </div>
	</div>
</div>
