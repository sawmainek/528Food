<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('cities/table.name')</th>
			<th>@lang('cities/table.name_mm')</th>
			<th>@lang('cities/table.country_id')</th>
    <th width="100px;">@lang('cities/table.action')</th>
    </thead>
    <tbody>
    @foreach($cities as $city)
        <tr>
            <td>{!! $city->name !!}</td>
			<td>{!! $city->name_mm !!}</td>
			<td>{!! $city['country']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.cities.show', [$city->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.cities.edit', [$city->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.cities.delete', [$city->id]) !!}" onclick="return confirm('@lang('cities/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
