<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('cities/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $city->name !!}</p>
        </div>
    </div>

</div>

<!-- Name Mm Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name_mm', Lang::get('cities/show_fields.name_mm'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $city->name_mm !!}</p>
        </div>
    </div>

</div>

<!-- Country Id Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('country_id', Lang::get('cities/show_fields.country_id'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $city['country']['name'] !!}</p>
        </div>
    </div>

</div>

