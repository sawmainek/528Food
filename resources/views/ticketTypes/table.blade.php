<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('ticketTypes/table.name')</th>
			<th>@lang('ticketTypes/table.description')</th>
    <th width="100px;">@lang('ticketTypes/table.action')</th>
    </thead>
    <tbody>
    @foreach($ticketTypes as $ticketType)
        <tr>
            <td>{!! $ticketType->name !!}</td>
			<td>{!! Illuminate\Support\Str::words($ticketType->description, 16,'....') !!}</td>
            <td>
                <a href="{!! route('administration.ticketTypes.show', [$ticketType->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.ticketTypes.edit', [$ticketType->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.ticketTypes.delete', [$ticketType->id]) !!}" onclick="return confirm('@lang('ticketTypes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
