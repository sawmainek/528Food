<!-- Name Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('name', Lang::get('ticketTypes/show_fields.name'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticketType->name !!}</p>
        </div>
    </div>

</div>

<!-- Description Field -->
<div class="row">
    <div class="form-group">
        {!! Form::label('description', Lang::get('ticketTypes/show_fields.description'), ['class' => 'col-sm-4 col-lg-2 control-label']) !!}
        <div class="col-sm-6 col-lg-6">
            <p>{!! $ticketType->description !!}</p>
        </div>
    </div>

</div>

