<table class="table table-bordered table-striped dataTable">
    <thead>
    <th>@lang('userLikes/table.food_id')</th>
			<th>@lang('userLikes/table.user_id')</th>
    <th width="100px;">@lang('userLikes/table.action')</th>
    </thead>
    <tbody>
    @foreach($userLikes as $userLike)
        <tr>
            <td>{!! $userLike['food']['name'] !!}</td>
			<td>{!! $userLike['user']['name'] !!}</td>
            <td>
                <a href="{!! route('administration.userLikes.show', [$userLike->id]) !!}"><i class="fa fa-eye"></i></a>
                <a href="{!! route('administration.userLikes.edit', [$userLike->id]) !!}"><i class="fa fa-pencil"></i></a>
                <a href="{!! route('administration.userLikes.delete', [$userLike->id]) !!}" onclick="return confirm('@lang('userLikes/table.delete_confirm_message')')"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
