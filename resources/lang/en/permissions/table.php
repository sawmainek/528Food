<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Permission Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'description'=>'Description',
'role_id'=>'Role Id',
'action'=>'Action',
'delete_confirm_message'=>'Are you sure wants to delete this Permission?',


];
