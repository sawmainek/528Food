<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Agent Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'model_name'=>'Agents',
'add_new'=>'Add New',
'no_model_found'=>'No Agents found.',


];
