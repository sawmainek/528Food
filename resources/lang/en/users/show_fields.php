<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'email'=>'Email',
'password'=>'Password',
'image'=>'Image',
'phone'=>'Phone',
'lat'=>'Lat',
'lng'=>'Lng',
'on_session'=>'On Session',
'session_id'=>'Session Id',


];
