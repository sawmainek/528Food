<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Hotel Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'address'=>'Address',
'phone'=>'Phone',
'email'=>'Email',
'agent_id'=>'Agent Id',
'hotel_type_id'=>'Hotel Type Id',


];
