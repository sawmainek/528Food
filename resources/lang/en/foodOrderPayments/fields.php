<?php

return [

    /*
    |--------------------------------------------------------------------------
    | FoodOrderPayment Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'order_id'=>'Order Id',
'amount'=>'Amount',
'payment_type_id'=>'Payment Type Id',
'payment_status'=>'Payment Status',
'payment_date'=>'Payment Date',
'save'=>'Save',
'cancel'=>'Cancel',


];
