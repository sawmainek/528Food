<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Food Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'category_id'=>'Category Id',
'description'=>'Description',
'shop_id'=>'Shop Id',
'total_ratings'=>'Total Ratings',
'total_reviews'=>'Total Reviews',
'action'=>'Action',
'delete_confirm_message'=>'Are you sure wants to delete this Food?',


];
