<?php

return [

    /*
    |--------------------------------------------------------------------------
    | TourPackage Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'agent_id'=>'Agent Id',
'package_type_id'=>'Package Type Id',
'duration'=>'Duration',
'start_end_point'=>'Start End Point',
'price'=>'Price',
'brief_itinerary'=>'Brief Itinerary',
'save'=>'Save',
'cancel'=>'Cancel',


];
