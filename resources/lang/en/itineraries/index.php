<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Itinerary Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'model_name'=>'Itineraries',
'add_new'=>'Add New',
'no_model_found'=>'No Itineraries found.',


];
