<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ingredient Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'food_id'=>'Food Id',
'ingredient_amt'=>'Ingredient Amt',
'ingredient_unit'=>'Ingredient Unit',
'action'=>'Action',
'delete_confirm_message'=>'Are you sure wants to delete this Ingredient?',


];
