<?php

return [

    /*
    |--------------------------------------------------------------------------
    | RoomPrice Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'hotel_id'=>'Hotel Id',
'room_type_id'=>'Room Type Id',
'price'=>'Price',
'views'=>'Views',
'floor_no'=>'Floor No',
'image'=>'Image',
'facilities'=>'Facilities',


];
