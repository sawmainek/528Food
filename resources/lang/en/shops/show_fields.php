<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name'=>'Name',
'address'=>'Address',
'phone'=>'Phone',
'image'=>'Image',
'shop_type_id'=>'Shop Type Id',
'open_hr'=>'Open Hr',
'lat'=>'Lat',
'lng'=>'Lng',
'total_ratings'=>'Total Ratings',
'total_reviews'=>'Total Reviews',


];
