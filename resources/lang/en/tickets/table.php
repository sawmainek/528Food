<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ticket Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'ticket_type_id'=>'Ticket Type Id',
'operator_id'=>'Operator Id',
'agent_id'=>'Agent Id',
'from_city'=>'From City',
'to_city'=>'To City',
'from_time'=>'From Time',
'to_time'=>'To Time',
'from_country'=>'From Country',
'to_country'=>'To Country',
'price'=>'Price',
'return_price'=>'Return Price',
'class_type_id'=>'Class Type Id',
'currency_unit'=>'Currency Unit',
'action'=>'Action',
'delete_confirm_message'=>'Are you sure wants to delete this Ticket?',


];
