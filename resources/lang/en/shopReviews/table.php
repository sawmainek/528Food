<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ShopReview Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'reviews'=>'Reviews',
'ratings'=>'Ratings',
'user_id'=>'User Id',
'shop_id'=>'Shop Id',
'action'=>'Action',
'delete_confirm_message'=>'Are you sure wants to delete this ShopReview?',


];
