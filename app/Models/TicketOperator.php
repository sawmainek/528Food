<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketOperator extends Model
{
    use SoftDeletes;

	public $table = "ticketOperators";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"operator_type_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"operator_type_id" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"operator_type_id" => "required"
	];

    	/**
	 * Get the agent object.
	 */
	public function agent()
	{
	    return $this->belongsTo('App\Models\Agent','name');
	}
	/**
	 * Get the operatorType object.
	 */
	public function operatorType()
	{
	    return $this->belongsTo('App\Models\OperatorType','operator_type_id');
	}


}
