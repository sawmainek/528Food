<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourPackage extends Model
{
    use SoftDeletes;

	public $table = "tourPackages";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"agent_id",
		"package_type_id",
		"duration",
		"start_end_point",
		"price",
		"brief_itinerary"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"agent_id" => "integer",
		"package_type_id" => "integer",
		"duration" => "string",
		"start_end_point" => "string",
		"price" => "integer",
		"brief_itinerary" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"agent_id" => "required",
		"package_type_id" => "required",
		"duration" => "required",
		"start_end_point" => "required",
		"price" => "required",
		"brief_itinerary" => "required"
	];

    	/**
	 * Get the agent object.
	 */
	public function agent()
	{
	    return $this->belongsTo('App\Models\Agent','agent_id');
	}
	/**
	 * Get the tourPackageType object.
	 */
	public function tourPackageType()
	{
	    return $this->belongsTo('App\Models\TourPackageType','package_type_id');
	}


}
