<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itinerary extends Model
{
    use SoftDeletes;

	public $table = "itineraries";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "tour_package_id",
		"day",
		"description",
		"image"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "tour_package_id" => "integer",
		"day" => "string",
		"description" => "string",
		"image" => "string"
    ];

	public static $rules = [
	    "tour_package_id" => "required",
		"day" => "required",
		"description" => "required",
	];

    	/**
	 * Get the tourPackage object.
	 */
	public function tourPackage()
	{
	    return $this->belongsTo('App\Models\TourPackage','tour_package_id');
	}


}
