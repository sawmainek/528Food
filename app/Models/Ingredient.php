<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredient extends Model
{
    use SoftDeletes;

	public $table = "ingredients";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"food_id",
		"ingredient_amt",
		"ingredient_unit"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"food_id" => "integer",
		"ingredient_amt" => "double",
		"ingredient_unit" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"food_id" => "required",
		"ingredient_amt" => "required",
		"ingredient_unit" => "required"
	];

    	/**
	 * Get the food object.
	 */
	public function food()
	{
	    return $this->belongsTo('App\Models\Food','food_id');
	}


}
