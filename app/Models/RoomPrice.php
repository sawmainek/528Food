<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomPrice extends Model
{
    use SoftDeletes;

	public $table = "roomPrices";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "hotel_id",
		"room_type_id",
		"price",
		"views",
		"floor_no",
		"image",
		"facilities"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "hotel_id" => "integer",
		"room_type_id" => "integer",
		"price" => "integer",
		"views" => "string",
		"floor_no" => "integer",
		"image" => "string",
		"facilities" => "string"
    ];

	public static $rules = [
	    "hotel_id" => "required",
		"room_type_id" => "required",
		"price" => "required",
		"views" => "required",
		"floor_no" => "required",
		"facilities" => "required"
	];

    	/**
	 * Get the hotel object.
	 */
	public function hotel()
	{
	    return $this->belongsTo('App\Models\Hotel','hotel_id');
	}
	/**
	 * Get the roomType object.
	 */
	public function roomType()
	{
	    return $this->belongsTo('App\Models\RoomType','room_type_id');
	}


}
