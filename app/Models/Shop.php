<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

	public $table = "shops";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"address",
		"phone",
		"image",
		"shop_type_id",
		"open_hr",
		"lat",
		"lng",
		"total_ratings",
		"total_reviews"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"address" => "string",
		"phone" => "string",
		"image" => "string",
		"shop_type_id" => "integer",
		"open_hr" => "string",
		"lat" => "double",
		"lng" => "double",
		"total_ratings" => "double",
		"total_reviews" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"address" => "required",
		"phone" => "required",
		"shop_type_id" => "required",
		"open_hr" => "required",
		"lat" => "required",
		"lng" => "required",
		"total_ratings" => "required",
		"total_reviews" => "required"
	];

    	/**
	 * Get the shoptype object.
	 */
	public function shoptype()
	{
	    return $this->belongsTo('App\Models\Shoptype','shop_type_id');
	}


}
