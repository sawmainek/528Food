<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;

	public $table = "agents";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"phone",
		"email",
		"address"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"phone" => "string",
		"email" => "string",
		"address" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"phone" => "required",
		"email" => "required",
		"address" => "required"
	];

    

}
