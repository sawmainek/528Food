<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopUser extends Model
{
    use SoftDeletes;

	public $table = "shopUsers";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "user_id",
		"shop_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "user_id" => "integer",
		"shop_id" => "integer"
    ];

	public static $rules = [
	    "user_id" => "required",
		"shop_id" => "required"
	];

    	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}
	/**
	 * Get the shop object.
	 */
	public function shop()
	{
	    return $this->belongsTo('App\Models\Shop','shop_id');
	}


}
