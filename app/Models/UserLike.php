<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLike extends Model
{
    use SoftDeletes;

	public $table = "userLikes";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "food_id",
		"user_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "food_id" => "integer",
		"user_id" => "integer"
    ];

	public static $rules = [
	    "food_id" => "required",
		"user_id" => "required"
	];

    	/**
	 * Get the food object.
	 */
	public function food()
	{
	    return $this->belongsTo('App\Models\Food','food_id');
	}
	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
