<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    use SoftDeletes;

	public $table = "userRoles";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "role_id",
		"user_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "role_id" => "integer",
		"user_id" => "integer"
    ];

	public static $rules = [
	    "role_id" => "required",
		"user_id" => "required"
	];

    	/**
	 * Get the role object.
	 */
	public function role()
	{
	    return $this->belongsTo('App\Models\Role','role_id');
	}
	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
