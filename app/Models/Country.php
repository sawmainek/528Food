<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

	public $table = "countries";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"name_mm",
		"flag"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"name_mm" => "string",
		"flag" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"name_mm" => "required",
		"flag" => "required"
	];

    

}
