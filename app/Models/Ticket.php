<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

	public $table = "tickets";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "ticket_type_id",
		"operator_id",
		"agent_id",
		"from_city",
		"to_city",
		"from_time",
		"to_time",
		"from_country",
		"to_country",
		"price",
		"return_price",
		"class_type_id",
		"currency_unit"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "ticket_type_id" => "integer",
		"operator_id" => "integer",
		"agent_id" => "integer",
		"from_city" => "integer",
		"to_city" => "integer",
		"from_time" => "string",
		"to_time" => "string",
		"from_country" => "integer",
		"to_country" => "integer",
		"price" => "integer",
		"return_price" => "integer",
		"class_type_id" => "integer",
		"currency_unit" => "integer"
    ];

	public static $rules = [
	    "ticket_type_id" => "required",
		"operator_id" => "required",
		"agent_id" => "required",
		"from_city" => "required",
		"to_city" => "required",
		"from_time" => "required",
		"to_time" => "required",
		"from_country" => "required",
		"to_country" => "required",
		"price" => "required",
		"return_price" => "required",
		"class_type_id" => "required",
		"currency_unit" => "required"
	];

    	/**
	 * Get the ticketType object.
	 */
	public function ticketType()
	{
	    return $this->belongsTo('App\Models\TicketType','ticket_type_id');
	}
	/**
	 * Get the ticketOperator object.
	 */
	public function ticketOperator()
	{
	    return $this->belongsTo('App\Models\TicketOperator','operator_id');
	}
	/**
	 * Get the agent object.
	 */
	public function agent()
	{
	    return $this->belongsTo('App\Models\Agent','agent_id');
	}
	/**
	 * Get the city object.
	 */
	public function from_city()
	{
	    return $this->belongsTo('App\Models\City','from_city');
	}
	/**
	 * Get the city object.
	 */
	public function to_city()
	{
	    return $this->belongsTo('App\Models\City','to_city');
	}
	/**
	 * Get the country object.
	 */
	public function from_country()
	{
	    return $this->belongsTo('App\Models\Country','from_country');
	}
	/**
	 * Get the country object.
	 */
	public function to_country()
	{
	    return $this->belongsTo('App\Models\Country','to_country');
	}
	/**
	 * Get the classsType object.
	 */
	public function classsType()
	{
	    return $this->belongsTo('App\Models\ClasssType','class_type_id');
	}


}
