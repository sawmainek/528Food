<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

	public $table = "orderDetails";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "order_id",
		"food_id",
		"size_id",
		"qty",
		"price",
		"sub_total",
		"note"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "order_id" => "integer",
		"food_id" => "integer",
		"size_id" => "integer",
		"qty" => "integer",
		"price" => "integer",
		"sub_total" => "double",
		"note" => "string"
    ];

	public static $rules = [
	    "order_id" => "required",
		"food_id" => "required",
		"size_id" => "required",
		"qty" => "required",
		"price" => "required",
		"sub_total" => "required",
		"note" => "required"
	];

    	/**
	 * Get the foodOrder object.
	 */
	public function foodOrder()
	{
	    return $this->belongsTo('App\Models\FoodOrder','order_id');
	}
	/**
	 * Get the food object.
	 */
	public function food()
	{
	    return $this->belongsTo('App\Models\Food','food_id');
	}
	/**
	 * Get the foodSize object.
	 */
	public function foodSize()
	{
	    return $this->belongsTo('App\Models\FoodSize','size_id');
	}


}
