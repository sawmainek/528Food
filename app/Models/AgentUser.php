<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentUser extends Model
{
    use SoftDeletes;

	public $table = "agentUsers";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "agent_id",
		"user_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "agent_id" => "integer",
		"user_id" => "integer"
    ];

	public static $rules = [
	    "agent_id" => "required",
		"user_id" => "required"
	];

    	/**
	 * Get the agent object.
	 */
	public function agent()
	{
	    return $this->belongsTo('App\Models\Agent','agent_id');
	}
	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
