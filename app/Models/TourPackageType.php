<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourPackageType extends Model
{
    use SoftDeletes;

	public $table = "tourPackageTypes";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"description"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"description" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"description" => "required"
	];

    

}
