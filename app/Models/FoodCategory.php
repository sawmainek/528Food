<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodCategory extends Model
{
    use SoftDeletes;

	public $table = "foodCategories";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"description",
		"icon",
		"background",
		"color"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"description" => "string",
		"icon" => "string",
		"background" => "string",
		"color" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"description" => "required",
		"color" => "required"
	];

	public function foods()
    {
        return $this->hasMany('App\Models\Food','category_id');
    }

    

}
