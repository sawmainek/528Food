<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodSize extends Model
{
    use SoftDeletes;

	public $table = "foodSizes";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "size",
		"food_id",
		"price"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "size" => "string",
		"food_id" => "integer",
		"price" => "integer"
    ];

	public static $rules = [
	    "size" => "required",
		"food_id" => "required",
		"price" => "required"
	];

    	/**
	 * Get the food object.
	 */
	public function food()
	{
	    return $this->belongsTo('App\Models\Food','food_id');
	}


}
