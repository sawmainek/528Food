<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodOrder extends Model
{
    use SoftDeletes;

	public $table = "foodOrders";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "order_date",
		"user_id",
		"paid",
		"total_amount"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "order_date" => "string",
		"user_id" => "integer",
		"paid" => "integer",
		"total_amount" => "double"
    ];

	public static $rules = [
	    "order_date" => "required",
		"user_id" => "required",
		"paid" => "required",
		"total_amount" => "required"
	];

	public static $api_rules = [
		"user_id" => "required",
		"order_menus" => "required",
	];

    	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
