<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClasssType extends Model
{
    use SoftDeletes;

	public $table = "classsTypes";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"facilities"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"facilities" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"facilities" => "required"
	];

    

}
