<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMessage extends Model
{
    use SoftDeletes;

	public $table = "userMessages";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "message",
		"user_id",
		"seen"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "message" => "string",
		"user_id" => "integer",
		"seen" => "boolean"
    ];

	public static $rules = [
	    "message" => "required",
		"user_id" => "required",
		"seen" => "required"
	];

    	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
