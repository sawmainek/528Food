<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;

	public $table = "permissions";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"description",
		"role_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"description" => "string",
		"role_id" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"description" => "required",
		"role_id" => "required"
	];

    	/**
	 * Get the role object.
	 */
	public function role()
	{
	    return $this->belongsTo('App\Models\Role','role_id');
	}


}
