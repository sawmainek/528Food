<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food extends Model
{
    use SoftDeletes;

	public $table = "foods";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"category_id",
		"description",
		"shop_id",
		"total_ratings",
		"total_reviews"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"category_id" => "integer",
		"description" => "string",
		"shop_id" => "integer",
		"total_ratings" => "double",
		"total_reviews" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"category_id" => "required",
		"description" => "required",
		"shop_id" => "required",
		"total_ratings" => "required",
		"total_reviews" => "required"
	];

    	/**
	 * Get the foodCategory object.
	 */
	public function foodCategory()
	{
	    return $this->belongsTo('App\Models\FoodCategory','category_id');
	}
	/**
	 * Get the shop object.
	 */
	public function shop()
	{
	    return $this->belongsTo('App\Models\Shop','shop_id');
	}

	public function foodPhotos()
    {
        return $this->hasMany('App\Models\FoodPhoto', 'food_id');
    }

    public function foodSizes()
    {
        return $this->hasMany('App\Models\FoodSize', 'food_id');
    }

    public function foodReviews()
    {
        return $this->hasMany('App\Models\FoodReview', 'food_id');
    }


}
