<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelPhoto extends Model
{
    use SoftDeletes;

	public $table = "hotelPhotos";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "hotel_id",
		"image"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "hotel_id" => "integer",
		"image" => "string"
    ];

	public static $rules = [
	    "hotel_id" => "required",
	];

    	/**
	 * Get the hotel object.
	 */
	public function hotel()
	{
	    return $this->belongsTo('App\Models\Hotel','hotel_id');
	}


}
