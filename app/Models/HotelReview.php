<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelReview extends Model
{
    use SoftDeletes;

	public $table = "hotelReviews";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "reviews",
		"ratings",
		"user_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "reviews" => "string",
		"ratings" => "double",
		"user_id" => "integer"
    ];

	public static $rules = [
	    "reviews" => "required",
		"ratings" => "required",
		"user_id" => "required"
	];

    	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}


}
