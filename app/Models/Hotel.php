<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;

	public $table = "hotels";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"address",
		"phone",
		"email",
		"agent_id",
		"hotel_type_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"address" => "string",
		"phone" => "string",
		"email" => "string",
		"agent_id" => "integer",
		"hotel_type_id" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"address" => "required",
		"phone" => "required",
		"email" => "required",
		"agent_id" => "required",
		"hotel_type_id" => "required"
	];

    	/**
	 * Get the agent object.
	 */
	public function agent()
	{
	    return $this->belongsTo('App\Models\Agent','agent_id');
	}
	/**
	 * Get the hotelType object.
	 */
	public function hotelType()
	{
	    return $this->belongsTo('App\Models\HotelType','hotel_type_id');
	}


}
