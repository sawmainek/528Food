<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopReview extends Model
{
    use SoftDeletes;

	public $table = "shopReviews";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "reviews",
		"ratings",
		"user_id",
		"shop_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "reviews" => "string",
		"ratings" => "double",
		"user_id" => "integer",
		"shop_id" => "integer"
    ];

	public static $rules = [
	    "reviews" => "required",
		"ratings" => "required",
		"user_id" => "required",
		"shop_id" => "required"
	];

    	/**
	 * Get the user object.
	 */
	public function user()
	{
	    return $this->belongsTo('App\Models\User','user_id');
	}
	/**
	 * Get the shop object.
	 */
	public function shop()
	{
	    return $this->belongsTo('App\Models\Shop','shop_id');
	}


}
