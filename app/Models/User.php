<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    /*use SoftDeletes;*/

	public $table = "users";
    
	/*protected $dates = ['deleted_at'];*/


	public $fillable = [
	    "name",
		"email",
		"password",
		"image",
		"phone",
		"lat",
		"lng",
		"on_session",
		"session_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"email" => "string",
		"password" => "string",
		"image" => "string",
		"phone" => "string",
		"lat" => "double",
		"lng" => "double",
		"on_session" => "boolean",
		"session_id" => "string"
    ];

	public static $rules = [
	    "name" => "required",
		"email" => "required",
		"password" => "required",
		//"image" => "required",
		"phone" => "required",
		"lat" => "required",
		"lng" => "required",
		"on_session" => "required",
		"session_id" => "required"
	];

    

}
