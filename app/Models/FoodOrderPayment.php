<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodOrderPayment extends Model
{
    use SoftDeletes;

	public $table = "foodOrderPayments";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "order_id",
		"amount",
		"payment_type_id",
		"payment_status",
		"payment_date"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "order_id" => "integer",
		"amount" => "integer",
		"payment_type_id" => "integer",
		"payment_status" => "boolean",
		"payment_date" => "string"
    ];

	public static $rules = [
	    "order_id" => "required",
		"amount" => "required",
		"payment_type_id" => "required",
		"payment_status" => "required",
		"payment_date" => "required"
	];

    	/**
	 * Get the foodOrder object.
	 */
	public function foodOrder()
	{
	    return $this->belongsTo('App\Models\FoodOrder','order_id');
	}
	/**
	 * Get the paymentType object.
	 */
	public function paymentType()
	{
	    return $this->belongsTo('App\Models\PaymentType','payment_type_id');
	}


}
