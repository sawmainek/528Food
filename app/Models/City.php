<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

	public $table = "cities";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name",
		"name_mm",
		"country_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"name_mm" => "string",
		"country_id" => "integer"
    ];

	public static $rules = [
	    "name" => "required",
		"name_mm" => "required",
		"country_id" => "required"
	];

    	/**
	 * Get the country object.
	 */
	public function country()
	{
	    return $this->belongsTo('App\Models\Country','country_id');
	}


}
