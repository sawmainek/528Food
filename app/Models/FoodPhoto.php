<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodPhoto extends Model
{
    use SoftDeletes;

	public $table = "foodPhotos";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "image",
		"food_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "image" => "string",
		"food_id" => "integer"
    ];

	public static $rules = [
		"food_id" => "required"
	];

    	/**
	 * Get the food object.
	 */
	public function food()
	{
	    return $this->belongsTo('App\Models\Food','food_id');
	}


}
