<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\UserRole;
use App\Models\Permission;
use Illuminate\Support\Str;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        } else{
           $userRole = UserRole::where('user_id',Auth::user()->id)->first();
           if($userRole){
                $permission = Permission::where('role_id', $userRole->role_id)->get();
                $has_permission = false;
                /*$uri = explode("/", $request->path());
                foreach ($permission as $value) {
                    if(count($uri) > 1){
                        if($userRole->role_id == $value->role_id && $uri[1] == Str::camel($value->name)){
                            $has_permission = true;
                            break;
                        }
                    }else{
                        $has_permission = true;
                    }
                }
                if(!$has_permission){
                    return response()->view('errors.404', [], 404);
                }*/
                Auth::user()->userRole = $userRole;
                Auth::user()->permission = $permission;

           }else{
                return response()->view('errors.404', [], 404);
           }
        }

        return $next($request);
    }
}
