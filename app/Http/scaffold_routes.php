<?php

/*
|--------------------------------------------------------------------------
| Scaffold Routes
|--------------------------------------------------------------------------
|
| Here is where all scaffold routes are defined.
|
*/

Route::resource('generators', 'GeneratorController');

Route::get('generators/{id}/delete', [
    'as' => 'generators.delete',
    'uses' => 'GeneratorController@destroy',
]);


Route::resource('foodCategories', 'FoodCategoryController');

Route::get('foodCategories/{id}/delete', [
    'as' => 'administration.foodCategories.delete',
    'uses' => 'FoodCategoryController@destroy',
]);


Route::resource('foods', 'FoodController');

Route::get('foods/{id}/delete', [
    'as' => 'administration.foods.delete',
    'uses' => 'FoodController@destroy',
]);


Route::resource('shoptypes', 'ShoptypeController');

Route::get('shoptypes/{id}/delete', [
    'as' => 'administration.shoptypes.delete',
    'uses' => 'ShoptypeController@destroy',
]);


Route::resource('shops', 'ShopController');

Route::get('shops/{id}/delete', [
    'as' => 'administration.shops.delete',
    'uses' => 'ShopController@destroy',
]);


Route::resource('foods', 'FoodController');

Route::get('foods/{id}/delete', [
    'as' => 'administration.foods.delete',
    'uses' => 'FoodController@destroy',
]);


Route::resource('foods', 'FoodController');

Route::get('foods/{id}/delete', [
    'as' => 'administration.foods.delete',
    'uses' => 'FoodController@destroy',
]);


Route::resource('foodSizes', 'FoodSizeController');

Route::get('foodSizes/{id}/delete', [
    'as' => 'administration.foodSizes.delete',
    'uses' => 'FoodSizeController@destroy',
]);


Route::resource('ingredients', 'IngredientController');

Route::get('ingredients/{id}/delete', [
    'as' => 'administration.ingredients.delete',
    'uses' => 'IngredientController@destroy',
]);


Route::resource('foodPhotos', 'FoodPhotoController');

Route::get('foodPhotos/{id}/delete', [
    'as' => 'administration.foodPhotos.delete',
    'uses' => 'FoodPhotoController@destroy',
]);


Route::resource('users', 'UserController');

Route::get('users/{id}/delete', [
    'as' => 'administration.users.delete',
    'uses' => 'UserController@destroy',
]);


Route::resource('foodReviews', 'FoodReviewController');

Route::get('foodReviews/{id}/delete', [
    'as' => 'administration.foodReviews.delete',
    'uses' => 'FoodReviewController@destroy',
]);


Route::resource('shopReviews', 'ShopReviewController');

Route::get('shopReviews/{id}/delete', [
    'as' => 'administration.shopReviews.delete',
    'uses' => 'ShopReviewController@destroy',
]);


Route::resource('shopUsers', 'ShopUserController');

Route::get('shopUsers/{id}/delete', [
    'as' => 'administration.shopUsers.delete',
    'uses' => 'ShopUserController@destroy',
]);


Route::resource('shopUsers', 'ShopUserController');

Route::get('shopUsers/{id}/delete', [
    'as' => 'administration.shopUsers.delete',
    'uses' => 'ShopUserController@destroy',
]);


Route::resource('shops', 'ShopController');

Route::get('shops/{id}/delete', [
    'as' => 'administration.shops.delete',
    'uses' => 'ShopController@destroy',
]);


Route::resource('foodOrders', 'FoodOrderController');

Route::get('foodOrders/{id}/delete', [
    'as' => 'administration.foodOrders.delete',
    'uses' => 'FoodOrderController@destroy',
]);


Route::resource('orderDetails', 'OrderDetailController');

Route::get('orderDetails/{id}/delete', [
    'as' => 'administration.orderDetails.delete',
    'uses' => 'OrderDetailController@destroy',
]);


Route::resource('paymentTypes', 'PaymentTypeController');

Route::get('paymentTypes/{id}/delete', [
    'as' => 'administration.paymentTypes.delete',
    'uses' => 'PaymentTypeController@destroy',
]);


Route::resource('foodOrderPayments', 'FoodOrderPaymentController');

Route::get('foodOrderPayments/{id}/delete', [
    'as' => 'administration.foodOrderPayments.delete',
    'uses' => 'FoodOrderPaymentController@destroy',
]);


Route::resource('roomTypes', 'RoomTypeController');

Route::get('roomTypes/{id}/delete', [
    'as' => 'administration.roomTypes.delete',
    'uses' => 'RoomTypeController@destroy',
]);


Route::resource('roomTypes', 'RoomTypeController');

Route::get('roomTypes/{id}/delete', [
    'as' => 'administration.roomTypes.delete',
    'uses' => 'RoomTypeController@destroy',
]);


Route::resource('hotelTypes', 'HotelTypeController');

Route::get('hotelTypes/{id}/delete', [
    'as' => 'administration.hotelTypes.delete',
    'uses' => 'HotelTypeController@destroy',
]);


Route::resource('agents', 'AgentController');

Route::get('agents/{id}/delete', [
    'as' => 'administration.agents.delete',
    'uses' => 'AgentController@destroy',
]);


Route::resource('hotels', 'HotelController');

Route::get('hotels/{id}/delete', [
    'as' => 'administration.hotels.delete',
    'uses' => 'HotelController@destroy',
]);


Route::resource('roomPrices', 'RoomPriceController');

Route::get('roomPrices/{id}/delete', [
    'as' => 'administration.roomPrices.delete',
    'uses' => 'RoomPriceController@destroy',
]);


Route::resource('hotelPhotos', 'HotelPhotoController');

Route::get('hotelPhotos/{id}/delete', [
    'as' => 'administration.hotelPhotos.delete',
    'uses' => 'HotelPhotoController@destroy',
]);


Route::resource('hotelReviews', 'HotelReviewController');

Route::get('hotelReviews/{id}/delete', [
    'as' => 'administration.hotelReviews.delete',
    'uses' => 'HotelReviewController@destroy',
]);


Route::resource('agentUsers', 'AgentUserController');

Route::get('agentUsers/{id}/delete', [
    'as' => 'administration.agentUsers.delete',
    'uses' => 'AgentUserController@destroy',
]);


Route::resource('agentReviews', 'AgentReviewController');

Route::get('agentReviews/{id}/delete', [
    'as' => 'administration.agentReviews.delete',
    'uses' => 'AgentReviewController@destroy',
]);


Route::resource('ticketTypes', 'TicketTypeController');

Route::get('ticketTypes/{id}/delete', [
    'as' => 'administration.ticketTypes.delete',
    'uses' => 'TicketTypeController@destroy',
]);


Route::resource('operatorTypes', 'OperatorTypeController');

Route::get('operatorTypes/{id}/delete', [
    'as' => 'administration.operatorTypes.delete',
    'uses' => 'OperatorTypeController@destroy',
]);


Route::resource('classsTypes', 'ClasssTypeController');

Route::get('classsTypes/{id}/delete', [
    'as' => 'administration.classsTypes.delete',
    'uses' => 'ClasssTypeController@destroy',
]);


Route::resource('ticketOperators', 'TicketOperatorController');

Route::get('ticketOperators/{id}/delete', [
    'as' => 'administration.ticketOperators.delete',
    'uses' => 'TicketOperatorController@destroy',
]);


Route::resource('tickets', 'TicketController');

Route::get('tickets/{id}/delete', [
    'as' => 'administration.tickets.delete',
    'uses' => 'TicketController@destroy',
]);


Route::resource('tourPackageTypes', 'TourPackageTypeController');

Route::get('tourPackageTypes/{id}/delete', [
    'as' => 'administration.tourPackageTypes.delete',
    'uses' => 'TourPackageTypeController@destroy',
]);


Route::resource('tourPackages', 'TourPackageController');

Route::get('tourPackages/{id}/delete', [
    'as' => 'administration.tourPackages.delete',
    'uses' => 'TourPackageController@destroy',
]);


Route::resource('itineraries', 'ItineraryController');

Route::get('itineraries/{id}/delete', [
    'as' => 'administration.itineraries.delete',
    'uses' => 'ItineraryController@destroy',
]);


Route::resource('countries', 'CountryController');

Route::get('countries/{id}/delete', [
    'as' => 'administration.countries.delete',
    'uses' => 'CountryController@destroy',
]);


Route::resource('cities', 'CityController');

Route::get('cities/{id}/delete', [
    'as' => 'administration.cities.delete',
    'uses' => 'CityController@destroy',
]);


Route::resource('tickets', 'TicketController');

Route::get('tickets/{id}/delete', [
    'as' => 'administration.tickets.delete',
    'uses' => 'TicketController@destroy',
]);


Route::resource('shops', 'ShopController');

Route::get('shops/{id}/delete', [
    'as' => 'administration.shops.delete',
    'uses' => 'ShopController@destroy',
]);


Route::resource('userRoles', 'UserRoleController');

Route::get('userRoles/{id}/delete', [
    'as' => 'administration.userRoles.delete',
    'uses' => 'UserRoleController@destroy',
]);


Route::resource('roles', 'RoleController');

Route::get('roles/{id}/delete', [
    'as' => 'administration.roles.delete',
    'uses' => 'RoleController@destroy',
]);


Route::resource('permissions', 'PermissionController');

Route::get('permissions/{id}/delete', [
    'as' => 'administration.permissions.delete',
    'uses' => 'PermissionController@destroy',
]);





Route::resource('userMessages', 'UserMessageController');

Route::get('userMessages/{id}/delete', [
    'as' => 'administration.userMessages.delete',
    'uses' => 'UserMessageController@destroy',
]);


Route::resource('userLikes', 'UserLikeController');

Route::get('userLikes/{id}/delete', [
    'as' => 'administration.userLikes.delete',
    'uses' => 'UserLikeController@destroy',
]);
