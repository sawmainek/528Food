<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/





Route::resource("foodCategories", "FoodCategoryAPIController");

Route::resource("foods", "FoodAPIController");

Route::resource("shoptypes", "ShoptypeAPIController");

Route::resource("shops", "ShopAPIController");

Route::resource("foods", "FoodAPIController");

Route::resource("foods", "FoodAPIController");

Route::resource("foodSizes", "FoodSizeAPIController");

Route::resource("ingredients", "IngredientAPIController");

Route::resource("users", "UserAPIController");

Route::resource("foodReviews", "FoodReviewAPIController");

Route::resource("shopReviews", "ShopReviewAPIController");

Route::resource("shopUsers", "ShopUserAPIController");

Route::resource("shops", "ShopAPIController");

Route::resource("foodOrders", "FoodOrderAPIController");

Route::resource("orderDetails", "OrderDetailAPIController");

Route::resource("paymentTypes", "PaymentTypeAPIController");

Route::resource("foodOrderPayments", "FoodOrderPaymentAPIController");

Route::resource("roomTypes", "RoomTypeAPIController");

Route::resource("roomTypes", "RoomTypeAPIController");

Route::resource("hotelTypes", "HotelTypeAPIController");

Route::resource("agents", "AgentAPIController");

Route::resource("hotels", "HotelAPIController");

Route::resource("roomPrices", "RoomPriceAPIController");

Route::resource("hotelPhotos", "HotelPhotoAPIController");

Route::resource("hotelReviews", "HotelReviewAPIController");

Route::resource("agentUsers", "AgentUserAPIController");

Route::resource("agentReviews", "AgentReviewAPIController");

Route::resource("ticketTypes", "TicketTypeAPIController");

Route::resource("operatorTypes", "OperatorTypeAPIController");

Route::resource("classsTypes", "ClasssTypeAPIController");

Route::resource("ticketOperators", "TicketOperatorAPIController");

Route::resource("tickets", "TicketAPIController");

Route::resource("tourPackageTypes", "TourPackageTypeAPIController");

Route::resource("tourPackages", "TourPackageAPIController");

Route::resource("itineraries", "ItineraryAPIController");

Route::resource("countries", "CountryAPIController");

Route::resource("cities", "CityAPIController");

Route::resource("tickets", "TicketAPIController");

Route::resource("shops", "ShopAPIController");


Route::resource("userRoles", "UserRoleAPIController");

Route::resource("roles", "RoleAPIController");



Route::resource("permissions", "PermissionAPIController");



Route::resource("userMessages", "UserMessageAPIController");

Route::resource("userLikes", "UserLikeAPIController");