<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTicketTypeRequest;
use App\Http\Requests\UpdateTicketTypeRequest;
use App\Libraries\Repositories\TicketTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\TicketType;

class TicketTypeController extends AppBaseController
{

	/** @var  TicketTypeRepository */
	private $ticketTypeRepository;

	function __construct(TicketTypeRepository $ticketTypeRepo)
	{
		$this->ticketTypeRepository = $ticketTypeRepo;
	}

	/**
	 * Display a listing of the TicketType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ticketTypes = $this->ticketTypeRepository->paginate(10);

		return view('ticketTypes.index')
			->with('ticketTypes', $ticketTypes);
	}

	/**
	 * Show the form for creating a new TicketType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('ticketTypes.create');
	}

	/**
	 * Store a newly created TicketType in storage.
	 *
	 * @param CreateTicketTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTicketTypeRequest $request)
	{
		$input = $request->all();

		

		$ticketType = $this->ticketTypeRepository->create($input);

		Flash::success('TicketType saved successfully.');

		return redirect(route('administration.ticketTypes.index'));
	}

	/**
	 * Display the specified TicketType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticketType = $this->ticketTypeRepository->find($id);

		if(empty($ticketType))
		{
			Flash::error('TicketType not found');

			return redirect(route('administration.ticketTypes.index'));
		}

		return view('ticketTypes.show')->with('ticketType', $ticketType);
	}

	/**
	 * Show the form for editing the specified TicketType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$ticketType = $this->ticketTypeRepository->find($id);

		if(empty($ticketType))
		{
			Flash::error('TicketType not found');

			return redirect(route('administration.ticketTypes.index'));
		}

		return view('ticketTypes.edit')->with('ticketType', $ticketType);
	}

	/**
	 * Update the specified TicketType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTicketTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTicketTypeRequest $request)
	{
		$ticketType = $this->ticketTypeRepository->find($id);

		if(empty($ticketType))
		{
			Flash::error('TicketType not found');

			return redirect(route('administration.ticketTypes.index'));
		}

		$input = $request->all();

		
		
		$this->ticketTypeRepository->updateRich($input, $id);

		Flash::success('TicketType updated successfully.');

		return redirect(route('administration.ticketTypes.index'));
	}

	/**
	 * Remove the specified TicketType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$ticketType = $this->ticketTypeRepository->find($id);

		if(empty($ticketType))
		{
			Flash::error('TicketType not found');

			return redirect(route('administration.ticketTypes.index'));
		}

		

		$this->ticketTypeRepository->delete($id);

		Flash::success('TicketType deleted successfully.');

		return redirect(route('administration.ticketTypes.index'));
	}
}
