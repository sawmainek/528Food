<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAgentUserRequest;
use App\Http\Requests\UpdateAgentUserRequest;
use App\Libraries\Repositories\AgentUserRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\AgentUser;
use App\Models\Agent;
use App\Models\User;

class AgentUserController extends AppBaseController
{

	/** @var  AgentUserRepository */
	private $agentUserRepository;

	function __construct(AgentUserRepository $agentUserRepo)
	{
		$this->agentUserRepository = $agentUserRepo;
	}

	/**
	 * Display a listing of the AgentUser.
	 *
	 * @return Response
	 */
	public function index()
	{
		$agentUsers = AgentUser::with(['agent', 'user'])->paginate(10);

		return view('agentUsers.index')
			->with('agentUsers', $agentUsers);
	}

	/**
	 * Show the form for creating a new AgentUser.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('agentUsers.create')->with(['agents'=>$agents, 'users'=>$users]);
	}

	/**
	 * Store a newly created AgentUser in storage.
	 *
	 * @param CreateAgentUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateAgentUserRequest $request)
	{
		$input = $request->all();

		

		$agentUser = $this->agentUserRepository->create($input);

		Flash::success('AgentUser saved successfully.');

		return redirect(route('administration.agentUsers.index'));
	}

	/**
	 * Display the specified AgentUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$agentUser = AgentUser::with(['agent', 'user'])->where('id', $id)->first();

		if(empty($agentUser))
		{
			Flash::error('AgentUser not found');

			return redirect(route('administration.agentUsers.index'));
		}

		return view('agentUsers.show')->with('agentUser', $agentUser);
	}

	/**
	 * Show the form for editing the specified AgentUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$agentUser = $this->agentUserRepository->find($id);

		if(empty($agentUser))
		{
			Flash::error('AgentUser not found');

			return redirect(route('administration.agentUsers.index'));
		}

		return view('agentUsers.edit')->with(['agents'=>$agents, 'users'=>$users, 'agentUser'=>$agentUser]);
	}

	/**
	 * Update the specified AgentUser in storage.
	 *
	 * @param  int              $id
	 * @param UpdateAgentUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAgentUserRequest $request)
	{
		$agentUser = $this->agentUserRepository->find($id);

		if(empty($agentUser))
		{
			Flash::error('AgentUser not found');

			return redirect(route('administration.agentUsers.index'));
		}

		$input = $request->all();

		

		$this->agentUserRepository->updateRich($input, $id);

		Flash::success('AgentUser updated successfully.');

		return redirect(route('administration.agentUsers.index'));
	}

	/**
	 * Remove the specified AgentUser from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$agentUser = $this->agentUserRepository->find($id);

		if(empty($agentUser))
		{
			Flash::error('AgentUser not found');

			return redirect(route('administration.agentUsers.index'));
		}

		

		$this->agentUserRepository->delete($id);

		Flash::success('AgentUser deleted successfully.');

		return redirect(route('administration.agentUsers.index'));
	}
}
