<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTourPackageTypeRequest;
use App\Http\Requests\UpdateTourPackageTypeRequest;
use App\Libraries\Repositories\TourPackageTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\TourPackageType;

class TourPackageTypeController extends AppBaseController
{

	/** @var  TourPackageTypeRepository */
	private $tourPackageTypeRepository;

	function __construct(TourPackageTypeRepository $tourPackageTypeRepo)
	{
		$this->tourPackageTypeRepository = $tourPackageTypeRepo;
	}

	/**
	 * Display a listing of the TourPackageType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tourPackageTypes = $this->tourPackageTypeRepository->paginate(10);

		return view('tourPackageTypes.index')
			->with('tourPackageTypes', $tourPackageTypes);
	}

	/**
	 * Show the form for creating a new TourPackageType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tourPackageTypes.create');
	}

	/**
	 * Store a newly created TourPackageType in storage.
	 *
	 * @param CreateTourPackageTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTourPackageTypeRequest $request)
	{
		$input = $request->all();

		

		$tourPackageType = $this->tourPackageTypeRepository->create($input);

		Flash::success('TourPackageType saved successfully.');

		return redirect(route('administration.tourPackageTypes.index'));
	}

	/**
	 * Display the specified TourPackageType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tourPackageType = $this->tourPackageTypeRepository->find($id);

		if(empty($tourPackageType))
		{
			Flash::error('TourPackageType not found');

			return redirect(route('administration.tourPackageTypes.index'));
		}

		return view('tourPackageTypes.show')->with('tourPackageType', $tourPackageType);
	}

	/**
	 * Show the form for editing the specified TourPackageType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$tourPackageType = $this->tourPackageTypeRepository->find($id);

		if(empty($tourPackageType))
		{
			Flash::error('TourPackageType not found');

			return redirect(route('administration.tourPackageTypes.index'));
		}

		return view('tourPackageTypes.edit')->with('tourPackageType', $tourPackageType);
	}

	/**
	 * Update the specified TourPackageType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTourPackageTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTourPackageTypeRequest $request)
	{
		$tourPackageType = $this->tourPackageTypeRepository->find($id);

		if(empty($tourPackageType))
		{
			Flash::error('TourPackageType not found');

			return redirect(route('administration.tourPackageTypes.index'));
		}

		$input = $request->all();

		
		
		$this->tourPackageTypeRepository->updateRich($input, $id);

		Flash::success('TourPackageType updated successfully.');

		return redirect(route('administration.tourPackageTypes.index'));
	}

	/**
	 * Remove the specified TourPackageType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tourPackageType = $this->tourPackageTypeRepository->find($id);

		if(empty($tourPackageType))
		{
			Flash::error('TourPackageType not found');

			return redirect(route('administration.tourPackageTypes.index'));
		}

		

		$this->tourPackageTypeRepository->delete($id);

		Flash::success('TourPackageType deleted successfully.');

		return redirect(route('administration.tourPackageTypes.index'));
	}
}
