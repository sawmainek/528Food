<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Libraries\Repositories\CountryRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Country;

class CountryController extends AppBaseController
{

	/** @var  CountryRepository */
	private $countryRepository;

	function __construct(CountryRepository $countryRepo)
	{
		$this->countryRepository = $countryRepo;
	}

	/**
	 * Display a listing of the Country.
	 *
	 * @return Response
	 */
	public function index()
	{
		$countries = $this->countryRepository->paginate(10);

		return view('countries.index')
			->with('countries', $countries);
	}

	/**
	 * Show the form for creating a new Country.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('countries.create');
	}

	/**
	 * Store a newly created Country in storage.
	 *
	 * @param CreateCountryRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCountryRequest $request)
	{
		$input = $request->all();

				if($request->file('flag')){
			$image = $this->uploadImage($request->file('flag'),'/countries/');
			$input['flag'] = $image['name'];
		}

		$country = $this->countryRepository->create($input);

		Flash::success('Country saved successfully.');

		return redirect(route('administration.countries.index'));
	}

	/**
	 * Display the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('administration.countries.index'));
		}

		return view('countries.show')->with('country', $country);
	}

	/**
	 * Show the form for editing the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('administration.countries.index'));
		}

		return view('countries.edit')->with('country', $country);
	}

	/**
	 * Update the specified Country in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCountryRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCountryRequest $request)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('administration.countries.index'));
		}

		$input = $request->all();

				if($request->file('flag')){
			$image = $this->uploadImage($request->file('flag'),'/countries/');
			$input['flag'] = $image['name'];
					@unlink(public_path('/countries/'.$country->flag));
		@unlink(public_path('/countries/x100/'.$country->flag));
		@unlink(public_path('/countries/x200/'.$country->flag));
		@unlink(public_path('/countries/x400/'.$country->flag));

		}
		
		$this->countryRepository->updateRich($input, $id);

		Flash::success('Country updated successfully.');

		return redirect(route('administration.countries.index'));
	}

	/**
	 * Remove the specified Country from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$country = $this->countryRepository->find($id);

		if(empty($country))
		{
			Flash::error('Country not found');

			return redirect(route('administration.countries.index'));
		}

				@unlink(public_path('/countries/'.$country->flag));
		@unlink(public_path('/countries/x100/'.$country->flag));
		@unlink(public_path('/countries/x200/'.$country->flag));
		@unlink(public_path('/countries/x400/'.$country->flag));


		$this->countryRepository->delete($id);

		Flash::success('Country deleted successfully.');

		return redirect(route('administration.countries.index'));
	}
}
