<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateShopReviewRequest;
use App\Http\Requests\UpdateShopReviewRequest;
use App\Libraries\Repositories\ShopReviewRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\ShopReview;
use App\Models\User;
use App\Models\Shop;

class ShopReviewController extends AppBaseController
{

	/** @var  ShopReviewRepository */
	private $shopReviewRepository;

	function __construct(ShopReviewRepository $shopReviewRepo)
	{
		$this->shopReviewRepository = $shopReviewRepo;
	}

	/**
	 * Display a listing of the ShopReview.
	 *
	 * @return Response
	 */
	public function index()
	{
		$shopReviews = ShopReview::with(['user', 'shop'])->paginate(10);

		return view('shopReviews.index')
			->with('shopReviews', $shopReviews);
	}

	/**
	 * Show the form for creating a new ShopReview.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}
		return view('shopReviews.create')->with(['users'=>$users, 'shops'=>$shops]);
	}

	/**
	 * Store a newly created ShopReview in storage.
	 *
	 * @param CreateShopReviewRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateShopReviewRequest $request)
	{
		$input = $request->all();

		

		$shopReview = $this->shopReviewRepository->create($input);

		Flash::success('ShopReview saved successfully.');

		return redirect(route('administration.shopReviews.index'));
	}

	/**
	 * Display the specified ShopReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shopReview = ShopReview::with(['user', 'shop'])->where('id', $id)->first();

		if(empty($shopReview))
		{
			Flash::error('ShopReview not found');

			return redirect(route('administration.shopReviews.index'));
		}

		return view('shopReviews.show')->with('shopReview', $shopReview);
	}

	/**
	 * Show the form for editing the specified ShopReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}

		$shopReview = $this->shopReviewRepository->find($id);

		if(empty($shopReview))
		{
			Flash::error('ShopReview not found');

			return redirect(route('administration.shopReviews.index'));
		}

		return view('shopReviews.edit')->with(['users'=>$users, 'shops'=>$shops, 'shopReview'=>$shopReview]);
	}

	/**
	 * Update the specified ShopReview in storage.
	 *
	 * @param  int              $id
	 * @param UpdateShopReviewRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateShopReviewRequest $request)
	{
		$shopReview = $this->shopReviewRepository->find($id);

		if(empty($shopReview))
		{
			Flash::error('ShopReview not found');

			return redirect(route('administration.shopReviews.index'));
		}

		$input = $request->all();

		

		$this->shopReviewRepository->updateRich($input, $id);

		Flash::success('ShopReview updated successfully.');

		return redirect(route('administration.shopReviews.index'));
	}

	/**
	 * Remove the specified ShopReview from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$shopReview = $this->shopReviewRepository->find($id);

		if(empty($shopReview))
		{
			Flash::error('ShopReview not found');

			return redirect(route('administration.shopReviews.index'));
		}

		

		$this->shopReviewRepository->delete($id);

		Flash::success('ShopReview deleted successfully.');

		return redirect(route('administration.shopReviews.index'));
	}
}
