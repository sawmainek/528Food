<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateShoptypeRequest;
use App\Http\Requests\UpdateShoptypeRequest;
use App\Libraries\Repositories\ShoptypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Shoptype;

class ShoptypeController extends AppBaseController
{

	/** @var  ShoptypeRepository */
	private $shoptypeRepository;

	function __construct(ShoptypeRepository $shoptypeRepo)
	{
		$this->shoptypeRepository = $shoptypeRepo;
	}

	/**
	 * Display a listing of the Shoptype.
	 *
	 * @return Response
	 */
	public function index()
	{
		$shoptypes = $this->shoptypeRepository->paginate(10);

		return view('shoptypes.index')
			->with('shoptypes', $shoptypes);
	}

	/**
	 * Show the form for creating a new Shoptype.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('shoptypes.create');
	}

	/**
	 * Store a newly created Shoptype in storage.
	 *
	 * @param CreateShoptypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateShoptypeRequest $request)
	{
		$input = $request->all();

		

		$shoptype = $this->shoptypeRepository->create($input);

		Flash::success('Shoptype saved successfully.');

		return redirect(route('administration.shoptypes.index'));
	}

	/**
	 * Display the specified Shoptype.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shoptype = $this->shoptypeRepository->find($id);

		if(empty($shoptype))
		{
			Flash::error('Shoptype not found');

			return redirect(route('administration.shoptypes.index'));
		}

		return view('shoptypes.show')->with('shoptype', $shoptype);
	}

	/**
	 * Show the form for editing the specified Shoptype.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$shoptype = $this->shoptypeRepository->find($id);

		if(empty($shoptype))
		{
			Flash::error('Shoptype not found');

			return redirect(route('administration.shoptypes.index'));
		}

		return view('shoptypes.edit')->with('shoptype', $shoptype);
	}

	/**
	 * Update the specified Shoptype in storage.
	 *
	 * @param  int              $id
	 * @param UpdateShoptypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateShoptypeRequest $request)
	{
		$shoptype = $this->shoptypeRepository->find($id);

		if(empty($shoptype))
		{
			Flash::error('Shoptype not found');

			return redirect(route('administration.shoptypes.index'));
		}

		$input = $request->all();

		
		
		$this->shoptypeRepository->updateRich($input, $id);

		Flash::success('Shoptype updated successfully.');

		return redirect(route('administration.shoptypes.index'));
	}

	/**
	 * Remove the specified Shoptype from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$shoptype = $this->shoptypeRepository->find($id);

		if(empty($shoptype))
		{
			Flash::error('Shoptype not found');

			return redirect(route('administration.shoptypes.index'));
		}

		

		$this->shoptypeRepository->delete($id);

		Flash::success('Shoptype deleted successfully.');

		return redirect(route('administration.shoptypes.index'));
	}
}
