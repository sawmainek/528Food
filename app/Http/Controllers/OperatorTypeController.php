<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOperatorTypeRequest;
use App\Http\Requests\UpdateOperatorTypeRequest;
use App\Libraries\Repositories\OperatorTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\OperatorType;

class OperatorTypeController extends AppBaseController
{

	/** @var  OperatorTypeRepository */
	private $operatorTypeRepository;

	function __construct(OperatorTypeRepository $operatorTypeRepo)
	{
		$this->operatorTypeRepository = $operatorTypeRepo;
	}

	/**
	 * Display a listing of the OperatorType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$operatorTypes = $this->operatorTypeRepository->paginate(10);

		return view('operatorTypes.index')
			->with('operatorTypes', $operatorTypes);
	}

	/**
	 * Show the form for creating a new OperatorType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('operatorTypes.create');
	}

	/**
	 * Store a newly created OperatorType in storage.
	 *
	 * @param CreateOperatorTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOperatorTypeRequest $request)
	{
		$input = $request->all();

		

		$operatorType = $this->operatorTypeRepository->create($input);

		Flash::success('OperatorType saved successfully.');

		return redirect(route('administration.operatorTypes.index'));
	}

	/**
	 * Display the specified OperatorType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$operatorType = $this->operatorTypeRepository->find($id);

		if(empty($operatorType))
		{
			Flash::error('OperatorType not found');

			return redirect(route('administration.operatorTypes.index'));
		}

		return view('operatorTypes.show')->with('operatorType', $operatorType);
	}

	/**
	 * Show the form for editing the specified OperatorType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$operatorType = $this->operatorTypeRepository->find($id);

		if(empty($operatorType))
		{
			Flash::error('OperatorType not found');

			return redirect(route('administration.operatorTypes.index'));
		}

		return view('operatorTypes.edit')->with('operatorType', $operatorType);
	}

	/**
	 * Update the specified OperatorType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateOperatorTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOperatorTypeRequest $request)
	{
		$operatorType = $this->operatorTypeRepository->find($id);

		if(empty($operatorType))
		{
			Flash::error('OperatorType not found');

			return redirect(route('administration.operatorTypes.index'));
		}

		$input = $request->all();

		
		
		$this->operatorTypeRepository->updateRich($input, $id);

		Flash::success('OperatorType updated successfully.');

		return redirect(route('administration.operatorTypes.index'));
	}

	/**
	 * Remove the specified OperatorType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$operatorType = $this->operatorTypeRepository->find($id);

		if(empty($operatorType))
		{
			Flash::error('OperatorType not found');

			return redirect(route('administration.operatorTypes.index'));
		}

		

		$this->operatorTypeRepository->delete($id);

		Flash::success('OperatorType deleted successfully.');

		return redirect(route('administration.operatorTypes.index'));
	}
}
