<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Libraries\Repositories\ShopRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Shop;
use App\Models\Shoptype;

class ShopController extends AppBaseController
{

	/** @var  ShopRepository */
	private $shopRepository;

	function __construct(ShopRepository $shopRepo)
	{
		$this->shopRepository = $shopRepo;
	}

	/**
	 * Display a listing of the Shop.
	 *
	 * @return Response
	 */
	public function index()
	{
		$shops = Shop::with(['shoptype'])->paginate(10);

		return view('shops.index')
			->with('shops', $shops);
	}

	/**
	 * Show the form for creating a new Shop.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$shoptype=Shoptype::all();
		$shoptypes = [];
		foreach ($shoptype as $key => $value) {
			$shoptypes[$value->id] = $value->name;
		}
		return view('shops.create')->with(['shoptypes'=>$shoptypes]);
	}

	/**
	 * Store a newly created Shop in storage.
	 *
	 * @param CreateShopRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateShopRequest $request)
	{
		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/shops/');
			$input['image'] = $image['name'];
		}

		$shop = $this->shopRepository->create($input);

		Flash::success('Shop saved successfully.');

		return redirect(route('administration.shops.index'));
	}

	/**
	 * Display the specified Shop.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shop = Shop::with(['shoptype'])->where('id', $id)->first();

		if(empty($shop))
		{
			Flash::error('Shop not found');

			return redirect(route('administration.shops.index'));
		}

		return view('shops.show')->with('shop', $shop);
	}

	/**
	 * Show the form for editing the specified Shop.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$shoptype=Shoptype::all();
		$shoptypes = [];
		foreach ($shoptype as $key => $value) {
			$shoptypes[$value->id] = $value->name;
		}

		$shop = $this->shopRepository->find($id);

		if(empty($shop))
		{
			Flash::error('Shop not found');

			return redirect(route('administration.shops.index'));
		}

		return view('shops.edit')->with(['shoptypes'=>$shoptypes, 'shop'=>$shop]);
	}

	/**
	 * Update the specified Shop in storage.
	 *
	 * @param  int              $id
	 * @param UpdateShopRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateShopRequest $request)
	{
		$shop = $this->shopRepository->find($id);

		if(empty($shop))
		{
			Flash::error('Shop not found');

			return redirect(route('administration.shops.index'));
		}

		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/shops/');
			$input['image'] = $image['name'];
					@unlink(public_path('/shops/'.$shop->image));
		@unlink(public_path('/shops/x100/'.$shop->image));
		@unlink(public_path('/shops/x200/'.$shop->image));
		@unlink(public_path('/shops/x400/'.$shop->image));

		}

		$this->shopRepository->updateRich($input, $id);

		Flash::success('Shop updated successfully.');

		return redirect(route('administration.shops.index'));
	}

	/**
	 * Remove the specified Shop from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$shop = $this->shopRepository->find($id);

		if(empty($shop))
		{
			Flash::error('Shop not found');

			return redirect(route('administration.shops.index'));
		}

				@unlink(public_path('/shops/'.$shop->image));
		@unlink(public_path('/shops/x100/'.$shop->image));
		@unlink(public_path('/shops/x200/'.$shop->image));
		@unlink(public_path('/shops/x400/'.$shop->image));


		$this->shopRepository->delete($id);

		Flash::success('Shop deleted successfully.');

		return redirect(route('administration.shops.index'));
	}
}
