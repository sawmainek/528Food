<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRoomTypeRequest;
use App\Http\Requests\UpdateRoomTypeRequest;
use App\Libraries\Repositories\RoomTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\RoomType;

class RoomTypeController extends AppBaseController
{

	/** @var  RoomTypeRepository */
	private $roomTypeRepository;

	function __construct(RoomTypeRepository $roomTypeRepo)
	{
		$this->roomTypeRepository = $roomTypeRepo;
	}

	/**
	 * Display a listing of the RoomType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roomTypes = $this->roomTypeRepository->paginate(10);

		return view('roomTypes.index')
			->with('roomTypes', $roomTypes);
	}

	/**
	 * Show the form for creating a new RoomType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('roomTypes.create');
	}

	/**
	 * Store a newly created RoomType in storage.
	 *
	 * @param CreateRoomTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateRoomTypeRequest $request)
	{
		$input = $request->all();

		

		$roomType = $this->roomTypeRepository->create($input);

		Flash::success('RoomType saved successfully.');

		return redirect(route('administration.roomTypes.index'));
	}

	/**
	 * Display the specified RoomType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roomType = $this->roomTypeRepository->find($id);

		if(empty($roomType))
		{
			Flash::error('RoomType not found');

			return redirect(route('administration.roomTypes.index'));
		}

		return view('roomTypes.show')->with('roomType', $roomType);
	}

	/**
	 * Show the form for editing the specified RoomType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$roomType = $this->roomTypeRepository->find($id);

		if(empty($roomType))
		{
			Flash::error('RoomType not found');

			return redirect(route('administration.roomTypes.index'));
		}

		return view('roomTypes.edit')->with('roomType', $roomType);
	}

	/**
	 * Update the specified RoomType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateRoomTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateRoomTypeRequest $request)
	{
		$roomType = $this->roomTypeRepository->find($id);

		if(empty($roomType))
		{
			Flash::error('RoomType not found');

			return redirect(route('administration.roomTypes.index'));
		}

		$input = $request->all();

		
		
		$this->roomTypeRepository->updateRich($input, $id);

		Flash::success('RoomType updated successfully.');

		return redirect(route('administration.roomTypes.index'));
	}

	/**
	 * Remove the specified RoomType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$roomType = $this->roomTypeRepository->find($id);

		if(empty($roomType))
		{
			Flash::error('RoomType not found');

			return redirect(route('administration.roomTypes.index'));
		}

		

		$this->roomTypeRepository->delete($id);

		Flash::success('RoomType deleted successfully.');

		return redirect(route('administration.roomTypes.index'));
	}
}
