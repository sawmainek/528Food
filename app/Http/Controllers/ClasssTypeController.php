<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClasssTypeRequest;
use App\Http\Requests\UpdateClasssTypeRequest;
use App\Libraries\Repositories\ClasssTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\ClasssType;

class ClasssTypeController extends AppBaseController
{

	/** @var  ClasssTypeRepository */
	private $classsTypeRepository;

	function __construct(ClasssTypeRepository $classsTypeRepo)
	{
		$this->classsTypeRepository = $classsTypeRepo;
	}

	/**
	 * Display a listing of the ClasssType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$classsTypes = $this->classsTypeRepository->paginate(10);

		return view('classsTypes.index')
			->with('classsTypes', $classsTypes);
	}

	/**
	 * Show the form for creating a new ClasssType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('classsTypes.create');
	}

	/**
	 * Store a newly created ClasssType in storage.
	 *
	 * @param CreateClasssTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateClasssTypeRequest $request)
	{
		$input = $request->all();

		

		$classsType = $this->classsTypeRepository->create($input);

		Flash::success('ClasssType saved successfully.');

		return redirect(route('administration.classsTypes.index'));
	}

	/**
	 * Display the specified ClasssType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classsType = $this->classsTypeRepository->find($id);

		if(empty($classsType))
		{
			Flash::error('ClasssType not found');

			return redirect(route('administration.classsTypes.index'));
		}

		return view('classsTypes.show')->with('classsType', $classsType);
	}

	/**
	 * Show the form for editing the specified ClasssType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$classsType = $this->classsTypeRepository->find($id);

		if(empty($classsType))
		{
			Flash::error('ClasssType not found');

			return redirect(route('administration.classsTypes.index'));
		}

		return view('classsTypes.edit')->with('classsType', $classsType);
	}

	/**
	 * Update the specified ClasssType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateClasssTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateClasssTypeRequest $request)
	{
		$classsType = $this->classsTypeRepository->find($id);

		if(empty($classsType))
		{
			Flash::error('ClasssType not found');

			return redirect(route('administration.classsTypes.index'));
		}

		$input = $request->all();

		
		
		$this->classsTypeRepository->updateRich($input, $id);

		Flash::success('ClasssType updated successfully.');

		return redirect(route('administration.classsTypes.index'));
	}

	/**
	 * Remove the specified ClasssType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$classsType = $this->classsTypeRepository->find($id);

		if(empty($classsType))
		{
			Flash::error('ClasssType not found');

			return redirect(route('administration.classsTypes.index'));
		}

		

		$this->classsTypeRepository->delete($id);

		Flash::success('ClasssType deleted successfully.');

		return redirect(route('administration.classsTypes.index'));
	}
}
