<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodOrderRequest;
use App\Http\Requests\UpdateFoodOrderRequest;
use App\Libraries\Repositories\FoodOrderRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodOrder;
use App\Models\User;

class FoodOrderController extends AppBaseController
{

	/** @var  FoodOrderRepository */
	private $foodOrderRepository;

	function __construct(FoodOrderRepository $foodOrderRepo)
	{
		$this->foodOrderRepository = $foodOrderRepo;
	}

	/**
	 * Display a listing of the FoodOrder.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodOrders = FoodOrder::with(['user'])->paginate(10);

		return view('foodOrders.index')
			->with('foodOrders', $foodOrders);
	}

	/**
	 * Show the form for creating a new FoodOrder.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('foodOrders.create')->with(['users'=>$users]);
	}

	/**
	 * Store a newly created FoodOrder in storage.
	 *
	 * @param CreateFoodOrderRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodOrderRequest $request)
	{
		$input = $request->all();

		

		$foodOrder = $this->foodOrderRepository->create($input);

		Flash::success('FoodOrder saved successfully.');

		return redirect(route('administration.foodOrders.index'));
	}

	/**
	 * Display the specified FoodOrder.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodOrder = FoodOrder::with(['user'])->where('id', $id)->first();

		if(empty($foodOrder))
		{
			Flash::error('FoodOrder not found');

			return redirect(route('administration.foodOrders.index'));
		}

		return view('foodOrders.show')->with('foodOrder', $foodOrder);
	}

	/**
	 * Show the form for editing the specified FoodOrder.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$foodOrder = $this->foodOrderRepository->find($id);

		if(empty($foodOrder))
		{
			Flash::error('FoodOrder not found');

			return redirect(route('administration.foodOrders.index'));
		}

		return view('foodOrders.edit')->with(['users'=>$users, 'foodOrder'=>$foodOrder]);
	}

	/**
	 * Update the specified FoodOrder in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodOrderRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodOrderRequest $request)
	{
		$foodOrder = $this->foodOrderRepository->find($id);

		if(empty($foodOrder))
		{
			Flash::error('FoodOrder not found');

			return redirect(route('administration.foodOrders.index'));
		}

		$input = $request->all();

		

		$this->foodOrderRepository->updateRich($input, $id);

		Flash::success('FoodOrder updated successfully.');

		return redirect(route('administration.foodOrders.index'));
	}

	/**
	 * Remove the specified FoodOrder from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodOrder = $this->foodOrderRepository->find($id);

		if(empty($foodOrder))
		{
			Flash::error('FoodOrder not found');

			return redirect(route('administration.foodOrders.index'));
		}

		

		$this->foodOrderRepository->delete($id);

		Flash::success('FoodOrder deleted successfully.');

		return redirect(route('administration.foodOrders.index'));
	}
}
