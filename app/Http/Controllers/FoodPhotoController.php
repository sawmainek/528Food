<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodPhotoRequest;
use App\Http\Requests\UpdateFoodPhotoRequest;
use App\Libraries\Repositories\FoodPhotoRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodPhoto;
use App\Models\Food;

class FoodPhotoController extends AppBaseController
{

	/** @var  FoodPhotoRepository */
	private $foodPhotoRepository;

	function __construct(FoodPhotoRepository $foodPhotoRepo)
	{
		$this->foodPhotoRepository = $foodPhotoRepo;
	}

	/**
	 * Display a listing of the FoodPhoto.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodPhotos = FoodPhoto::with(['food'])->paginate(10);

		return view('foodPhotos.index')
			->with('foodPhotos', $foodPhotos);
	}

	/**
	 * Show the form for creating a new FoodPhoto.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}
		return view('foodPhotos.create')->with(['foods'=>$foods]);
	}

	/**
	 * Store a newly created FoodPhoto in storage.
	 *
	 * @param CreateFoodPhotoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodPhotoRequest $request)
	{
		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/foodPhotos/');
			$input['image'] = $image['name'];
		}

		$foodPhoto = $this->foodPhotoRepository->create($input);

		Flash::success('FoodPhoto saved successfully.');

		return redirect(route('administration.foodPhotos.index'));
	}

	/**
	 * Display the specified FoodPhoto.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodPhoto = FoodPhoto::with(['food'])->where('id', $id)->first();

		if(empty($foodPhoto))
		{
			Flash::error('FoodPhoto not found');

			return redirect(route('administration.foodPhotos.index'));
		}

		return view('foodPhotos.show')->with('foodPhoto', $foodPhoto);
	}

	/**
	 * Show the form for editing the specified FoodPhoto.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$foodPhoto = $this->foodPhotoRepository->find($id);

		if(empty($foodPhoto))
		{
			Flash::error('FoodPhoto not found');

			return redirect(route('administration.foodPhotos.index'));
		}

		return view('foodPhotos.edit')->with(['foods'=>$foods, 'foodPhoto'=>$foodPhoto]);
	}

	/**
	 * Update the specified FoodPhoto in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodPhotoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodPhotoRequest $request)
	{
		$foodPhoto = $this->foodPhotoRepository->find($id);

		if(empty($foodPhoto))
		{
			Flash::error('FoodPhoto not found');

			return redirect(route('administration.foodPhotos.index'));
		}

		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/foodPhotos/');
			$input['image'] = $image['name'];
					@unlink(public_path('/foodPhotos/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x100/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x200/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x400/'.$foodPhoto->image));

		}

		$this->foodPhotoRepository->updateRich($input, $id);

		Flash::success('FoodPhoto updated successfully.');

		return redirect(route('administration.foodPhotos.index'));
	}

	/**
	 * Remove the specified FoodPhoto from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodPhoto = $this->foodPhotoRepository->find($id);

		if(empty($foodPhoto))
		{
			Flash::error('FoodPhoto not found');

			return redirect(route('administration.foodPhotos.index'));
		}

				@unlink(public_path('/foodPhotos/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x100/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x200/'.$foodPhoto->image));
		@unlink(public_path('/foodPhotos/x400/'.$foodPhoto->image));


		$this->foodPhotoRepository->delete($id);

		Flash::success('FoodPhoto deleted successfully.');

		return redirect(route('administration.foodPhotos.index'));
	}
}
