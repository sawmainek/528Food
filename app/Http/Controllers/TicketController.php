<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Libraries\Repositories\TicketRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Ticket;
use App\Models\TicketType;
use App\Models\TicketOperator;
use App\Models\Agent;
use App\Models\City;
use App\Models\City;
use App\Models\Country;
use App\Models\Country;
use App\Models\ClasssType;

class TicketController extends AppBaseController
{

	/** @var  TicketRepository */
	private $ticketRepository;

	function __construct(TicketRepository $ticketRepo)
	{
		$this->ticketRepository = $ticketRepo;
	}

	/**
	 * Display a listing of the Ticket.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tickets = Ticket::with(['ticketType', 'ticketOperator', 'agent', 'city', 'city', 'country', 'country', 'classsType'])->paginate(10);

		return view('tickets.index')
			->with('tickets', $tickets);
	}

	/**
	 * Show the form for creating a new Ticket.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$ticketType=TicketType::all();
		$ticketTypes = [];
		foreach ($ticketType as $key => $value) {
			$ticketTypes[$value->id] = $value->name;
		}

		$ticketOperator=TicketOperator::all();
		$ticketOperators = [];
		foreach ($ticketOperator as $key => $value) {
			$ticketOperators[$value->id] = $value->name;
		}

		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$city=City::all();
		$cities = [];
		foreach ($city as $key => $value) {
			$cities[$value->id] = $value->name;
		}

		$city=City::all();
		$cities = [];
		foreach ($city as $key => $value) {
			$cities[$value->id] = $value->name;
		}

		$country=Country::all();
		$countries = [];
		foreach ($country as $key => $value) {
			$countries[$value->id] = $value->name;
		}

		$country=Country::all();
		$countries = [];
		foreach ($country as $key => $value) {
			$countries[$value->id] = $value->name;
		}

		$classsType=ClasssType::all();
		$classsTypes = [];
		foreach ($classsType as $key => $value) {
			$classsTypes[$value->id] = $value->name;
		}
		return view('tickets.create')->with(['ticketTypes'=>$ticketTypes, 'ticketOperators'=>$ticketOperators, 'agents'=>$agents, 'cities'=>$cities, 'cities'=>$cities, 'countries'=>$countries, 'countries'=>$countries, 'classsTypes'=>$classsTypes]);
	}

	/**
	 * Store a newly created Ticket in storage.
	 *
	 * @param CreateTicketRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTicketRequest $request)
	{
		$input = $request->all();

		

		$ticket = $this->ticketRepository->create($input);

		Flash::success('Ticket saved successfully.');

		return redirect(route('administration.tickets.index'));
	}

	/**
	 * Display the specified Ticket.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticket = Ticket::with(['ticketType', 'ticketOperator', 'agent', 'city', 'city', 'country', 'country', 'classsType'])->where('id', $id)->first();

		if(empty($ticket))
		{
			Flash::error('Ticket not found');

			return redirect(route('administration.tickets.index'));
		}

		return view('tickets.show')->with('ticket', $ticket);
	}

	/**
	 * Show the form for editing the specified Ticket.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$ticketType=TicketType::all();
		$ticketTypes = [];
		foreach ($ticketType as $key => $value) {
			$ticketTypes[$value->id] = $value->name;
		}

		$ticketOperator=TicketOperator::all();
		$ticketOperators = [];
		foreach ($ticketOperator as $key => $value) {
			$ticketOperators[$value->id] = $value->name;
		}

		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$city=City::all();
		$cities = [];
		foreach ($city as $key => $value) {
			$cities[$value->id] = $value->name;
		}

		$city=City::all();
		$cities = [];
		foreach ($city as $key => $value) {
			$cities[$value->id] = $value->name;
		}

		$country=Country::all();
		$countries = [];
		foreach ($country as $key => $value) {
			$countries[$value->id] = $value->name;
		}

		$country=Country::all();
		$countries = [];
		foreach ($country as $key => $value) {
			$countries[$value->id] = $value->name;
		}

		$classsType=ClasssType::all();
		$classsTypes = [];
		foreach ($classsType as $key => $value) {
			$classsTypes[$value->id] = $value->name;
		}

		$ticket = $this->ticketRepository->find($id);

		if(empty($ticket))
		{
			Flash::error('Ticket not found');

			return redirect(route('administration.tickets.index'));
		}

		return view('tickets.edit')->with(['ticketTypes'=>$ticketTypes, 'ticketOperators'=>$ticketOperators, 'agents'=>$agents, 'cities'=>$cities, 'cities'=>$cities, 'countries'=>$countries, 'countries'=>$countries, 'classsTypes'=>$classsTypes, 'ticket'=>$ticket]);
	}

	/**
	 * Update the specified Ticket in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTicketRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTicketRequest $request)
	{
		$ticket = $this->ticketRepository->find($id);

		if(empty($ticket))
		{
			Flash::error('Ticket not found');

			return redirect(route('administration.tickets.index'));
		}

		$input = $request->all();

		

		$this->ticketRepository->updateRich($input, $id);

		Flash::success('Ticket updated successfully.');

		return redirect(route('administration.tickets.index'));
	}

	/**
	 * Remove the specified Ticket from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$ticket = $this->ticketRepository->find($id);

		if(empty($ticket))
		{
			Flash::error('Ticket not found');

			return redirect(route('administration.tickets.index'));
		}

		

		$this->ticketRepository->delete($id);

		Flash::success('Ticket deleted successfully.');

		return redirect(route('administration.tickets.index'));
	}
}
