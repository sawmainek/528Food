<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePaymentTypeRequest;
use App\Http\Requests\UpdatePaymentTypeRequest;
use App\Libraries\Repositories\PaymentTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\PaymentType;

class PaymentTypeController extends AppBaseController
{

	/** @var  PaymentTypeRepository */
	private $paymentTypeRepository;

	function __construct(PaymentTypeRepository $paymentTypeRepo)
	{
		$this->paymentTypeRepository = $paymentTypeRepo;
	}

	/**
	 * Display a listing of the PaymentType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$paymentTypes = $this->paymentTypeRepository->paginate(10);

		return view('paymentTypes.index')
			->with('paymentTypes', $paymentTypes);
	}

	/**
	 * Show the form for creating a new PaymentType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('paymentTypes.create');
	}

	/**
	 * Store a newly created PaymentType in storage.
	 *
	 * @param CreatePaymentTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePaymentTypeRequest $request)
	{
		$input = $request->all();

		

		$paymentType = $this->paymentTypeRepository->create($input);

		Flash::success('PaymentType saved successfully.');

		return redirect(route('administration.paymentTypes.index'));
	}

	/**
	 * Display the specified PaymentType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$paymentType = $this->paymentTypeRepository->find($id);

		if(empty($paymentType))
		{
			Flash::error('PaymentType not found');

			return redirect(route('administration.paymentTypes.index'));
		}

		return view('paymentTypes.show')->with('paymentType', $paymentType);
	}

	/**
	 * Show the form for editing the specified PaymentType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$paymentType = $this->paymentTypeRepository->find($id);

		if(empty($paymentType))
		{
			Flash::error('PaymentType not found');

			return redirect(route('administration.paymentTypes.index'));
		}

		return view('paymentTypes.edit')->with('paymentType', $paymentType);
	}

	/**
	 * Update the specified PaymentType in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePaymentTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePaymentTypeRequest $request)
	{
		$paymentType = $this->paymentTypeRepository->find($id);

		if(empty($paymentType))
		{
			Flash::error('PaymentType not found');

			return redirect(route('administration.paymentTypes.index'));
		}

		$input = $request->all();

		
		
		$this->paymentTypeRepository->updateRich($input, $id);

		Flash::success('PaymentType updated successfully.');

		return redirect(route('administration.paymentTypes.index'));
	}

	/**
	 * Remove the specified PaymentType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$paymentType = $this->paymentTypeRepository->find($id);

		if(empty($paymentType))
		{
			Flash::error('PaymentType not found');

			return redirect(route('administration.paymentTypes.index'));
		}

		

		$this->paymentTypeRepository->delete($id);

		Flash::success('PaymentType deleted successfully.');

		return redirect(route('administration.paymentTypes.index'));
	}
}
