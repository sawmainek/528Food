<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserLikeRequest;
use App\Http\Requests\UpdateUserLikeRequest;
use App\Libraries\Repositories\UserLikeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\UserLike;
use App\Models\Food;
use App\Models\User;

class UserLikeController extends AppBaseController
{

	/** @var  UserLikeRepository */
	private $userLikeRepository;

	function __construct(UserLikeRepository $userLikeRepo)
	{
		$this->userLikeRepository = $userLikeRepo;
	}

	/**
	 * Display a listing of the UserLike.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userLikes = UserLike::with(['food', 'user'])->paginate(10);

		return view('userLikes.index')
			->with('userLikes', $userLikes);
	}

	/**
	 * Show the form for creating a new UserLike.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('userLikes.create')->with(['foods'=>$foods, 'users'=>$users]);
	}

	/**
	 * Store a newly created UserLike in storage.
	 *
	 * @param CreateUserLikeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserLikeRequest $request)
	{
		$input = $request->all();

		

		$userLike = $this->userLikeRepository->create($input);

		Flash::success('UserLike saved successfully.');

		return redirect(route('administration.userLikes.index'));
	}

	/**
	 * Display the specified UserLike.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userLike = UserLike::with(['food', 'user'])->where('id', $id)->first();

		if(empty($userLike))
		{
			Flash::error('UserLike not found');

			return redirect(route('administration.userLikes.index'));
		}

		return view('userLikes.show')->with('userLike', $userLike);
	}

	/**
	 * Show the form for editing the specified UserLike.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$userLike = $this->userLikeRepository->find($id);

		if(empty($userLike))
		{
			Flash::error('UserLike not found');

			return redirect(route('administration.userLikes.index'));
		}

		return view('userLikes.edit')->with(['foods'=>$foods, 'users'=>$users, 'userLike'=>$userLike]);
	}

	/**
	 * Update the specified UserLike in storage.
	 *
	 * @param  int              $id
	 * @param UpdateUserLikeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserLikeRequest $request)
	{
		$userLike = $this->userLikeRepository->find($id);

		if(empty($userLike))
		{
			Flash::error('UserLike not found');

			return redirect(route('administration.userLikes.index'));
		}

		$input = $request->all();

		

		$this->userLikeRepository->updateRich($input, $id);

		Flash::success('UserLike updated successfully.');

		return redirect(route('administration.userLikes.index'));
	}

	/**
	 * Remove the specified UserLike from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$userLike = $this->userLikeRepository->find($id);

		if(empty($userLike))
		{
			Flash::error('UserLike not found');

			return redirect(route('administration.userLikes.index'));
		}

		

		$this->userLikeRepository->delete($id);

		Flash::success('UserLike deleted successfully.');

		return redirect(route('administration.userLikes.index'));
	}
}
