<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRoomPriceRequest;
use App\Http\Requests\UpdateRoomPriceRequest;
use App\Libraries\Repositories\RoomPriceRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\RoomPrice;
use App\Models\Hotel;
use App\Models\RoomType;

class RoomPriceController extends AppBaseController
{

	/** @var  RoomPriceRepository */
	private $roomPriceRepository;

	function __construct(RoomPriceRepository $roomPriceRepo)
	{
		$this->roomPriceRepository = $roomPriceRepo;
	}

	/**
	 * Display a listing of the RoomPrice.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roomPrices = RoomPrice::with(['hotel', 'roomType'])->paginate(10);

		return view('roomPrices.index')
			->with('roomPrices', $roomPrices);
	}

	/**
	 * Show the form for creating a new RoomPrice.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$hotel=Hotel::all();
		$hotels = [];
		foreach ($hotel as $key => $value) {
			$hotels[$value->id] = $value->name;
		}

		$roomType=RoomType::all();
		$roomTypes = [];
		foreach ($roomType as $key => $value) {
			$roomTypes[$value->id] = $value->name;
		}
		return view('roomPrices.create')->with(['hotels'=>$hotels, 'roomTypes'=>$roomTypes]);
	}

	/**
	 * Store a newly created RoomPrice in storage.
	 *
	 * @param CreateRoomPriceRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateRoomPriceRequest $request)
	{
		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/roomPrices/');
			$input['image'] = $image['name'];
		}

		$roomPrice = $this->roomPriceRepository->create($input);

		Flash::success('RoomPrice saved successfully.');

		return redirect(route('administration.roomPrices.index'));
	}

	/**
	 * Display the specified RoomPrice.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roomPrice = RoomPrice::with(['hotel', 'roomType'])->where('id', $id)->first();

		if(empty($roomPrice))
		{
			Flash::error('RoomPrice not found');

			return redirect(route('administration.roomPrices.index'));
		}

		return view('roomPrices.show')->with('roomPrice', $roomPrice);
	}

	/**
	 * Show the form for editing the specified RoomPrice.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$hotel=Hotel::all();
		$hotels = [];
		foreach ($hotel as $key => $value) {
			$hotels[$value->id] = $value->name;
		}

		$roomType=RoomType::all();
		$roomTypes = [];
		foreach ($roomType as $key => $value) {
			$roomTypes[$value->id] = $value->name;
		}

		$roomPrice = $this->roomPriceRepository->find($id);

		if(empty($roomPrice))
		{
			Flash::error('RoomPrice not found');

			return redirect(route('administration.roomPrices.index'));
		}

		return view('roomPrices.edit')->with(['hotels'=>$hotels, 'roomTypes'=>$roomTypes, 'roomPrice'=>$roomPrice]);
	}

	/**
	 * Update the specified RoomPrice in storage.
	 *
	 * @param  int              $id
	 * @param UpdateRoomPriceRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateRoomPriceRequest $request)
	{
		$roomPrice = $this->roomPriceRepository->find($id);

		if(empty($roomPrice))
		{
			Flash::error('RoomPrice not found');

			return redirect(route('administration.roomPrices.index'));
		}

		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/roomPrices/');
			$input['image'] = $image['name'];
					@unlink(public_path('/roomPrices/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x100/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x200/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x400/'.$roomPrice->image));

		}

		$this->roomPriceRepository->updateRich($input, $id);

		Flash::success('RoomPrice updated successfully.');

		return redirect(route('administration.roomPrices.index'));
	}

	/**
	 * Remove the specified RoomPrice from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$roomPrice = $this->roomPriceRepository->find($id);

		if(empty($roomPrice))
		{
			Flash::error('RoomPrice not found');

			return redirect(route('administration.roomPrices.index'));
		}

				@unlink(public_path('/roomPrices/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x100/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x200/'.$roomPrice->image));
		@unlink(public_path('/roomPrices/x400/'.$roomPrice->image));


		$this->roomPriceRepository->delete($id);

		Flash::success('RoomPrice deleted successfully.');

		return redirect(route('administration.roomPrices.index'));
	}
}
