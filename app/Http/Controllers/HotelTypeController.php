<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateHotelTypeRequest;
use App\Http\Requests\UpdateHotelTypeRequest;
use App\Libraries\Repositories\HotelTypeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\HotelType;

class HotelTypeController extends AppBaseController
{

	/** @var  HotelTypeRepository */
	private $hotelTypeRepository;

	function __construct(HotelTypeRepository $hotelTypeRepo)
	{
		$this->hotelTypeRepository = $hotelTypeRepo;
	}

	/**
	 * Display a listing of the HotelType.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hotelTypes = $this->hotelTypeRepository->paginate(10);

		return view('hotelTypes.index')
			->with('hotelTypes', $hotelTypes);
	}

	/**
	 * Show the form for creating a new HotelType.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('hotelTypes.create');
	}

	/**
	 * Store a newly created HotelType in storage.
	 *
	 * @param CreateHotelTypeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateHotelTypeRequest $request)
	{
		$input = $request->all();

		

		$hotelType = $this->hotelTypeRepository->create($input);

		Flash::success('HotelType saved successfully.');

		return redirect(route('administration.hotelTypes.index'));
	}

	/**
	 * Display the specified HotelType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelType = $this->hotelTypeRepository->find($id);

		if(empty($hotelType))
		{
			Flash::error('HotelType not found');

			return redirect(route('administration.hotelTypes.index'));
		}

		return view('hotelTypes.show')->with('hotelType', $hotelType);
	}

	/**
	 * Show the form for editing the specified HotelType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$hotelType = $this->hotelTypeRepository->find($id);

		if(empty($hotelType))
		{
			Flash::error('HotelType not found');

			return redirect(route('administration.hotelTypes.index'));
		}

		return view('hotelTypes.edit')->with('hotelType', $hotelType);
	}

	/**
	 * Update the specified HotelType in storage.
	 *
	 * @param  int              $id
	 * @param UpdateHotelTypeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateHotelTypeRequest $request)
	{
		$hotelType = $this->hotelTypeRepository->find($id);

		if(empty($hotelType))
		{
			Flash::error('HotelType not found');

			return redirect(route('administration.hotelTypes.index'));
		}

		$input = $request->all();

		
		
		$this->hotelTypeRepository->updateRich($input, $id);

		Flash::success('HotelType updated successfully.');

		return redirect(route('administration.hotelTypes.index'));
	}

	/**
	 * Remove the specified HotelType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$hotelType = $this->hotelTypeRepository->find($id);

		if(empty($hotelType))
		{
			Flash::error('HotelType not found');

			return redirect(route('administration.hotelTypes.index'));
		}

		

		$this->hotelTypeRepository->delete($id);

		Flash::success('HotelType deleted successfully.');

		return redirect(route('administration.hotelTypes.index'));
	}
}
