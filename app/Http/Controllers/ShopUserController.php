<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateShopUserRequest;
use App\Http\Requests\UpdateShopUserRequest;
use App\Libraries\Repositories\ShopUserRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\ShopUser;
use App\Models\User;
use App\Models\Shop;

class ShopUserController extends AppBaseController
{

	/** @var  ShopUserRepository */
	private $shopUserRepository;

	function __construct(ShopUserRepository $shopUserRepo)
	{
		$this->shopUserRepository = $shopUserRepo;
	}

	/**
	 * Display a listing of the ShopUser.
	 *
	 * @return Response
	 */
	public function index()
	{
		$shopUsers = ShopUser::with(['user', 'shop'])->paginate(10);

		return view('shopUsers.index')
			->with('shopUsers', $shopUsers);
	}

	/**
	 * Show the form for creating a new ShopUser.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}
		return view('shopUsers.create')->with(['users'=>$users, 'shops'=>$shops]);
	}

	/**
	 * Store a newly created ShopUser in storage.
	 *
	 * @param CreateShopUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateShopUserRequest $request)
	{
		$input = $request->all();

		

		$shopUser = $this->shopUserRepository->create($input);

		Flash::success('ShopUser saved successfully.');

		return redirect(route('administration.shopUsers.index'));
	}

	/**
	 * Display the specified ShopUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shopUser = ShopUser::with(['user', 'shop'])->where('id', $id)->first();

		if(empty($shopUser))
		{
			Flash::error('ShopUser not found');

			return redirect(route('administration.shopUsers.index'));
		}

		return view('shopUsers.show')->with('shopUser', $shopUser);
	}

	/**
	 * Show the form for editing the specified ShopUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}

		$shopUser = $this->shopUserRepository->find($id);

		if(empty($shopUser))
		{
			Flash::error('ShopUser not found');

			return redirect(route('administration.shopUsers.index'));
		}

		return view('shopUsers.edit')->with(['users'=>$users, 'shops'=>$shops, 'shopUser'=>$shopUser]);
	}

	/**
	 * Update the specified ShopUser in storage.
	 *
	 * @param  int              $id
	 * @param UpdateShopUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateShopUserRequest $request)
	{
		$shopUser = $this->shopUserRepository->find($id);

		if(empty($shopUser))
		{
			Flash::error('ShopUser not found');

			return redirect(route('administration.shopUsers.index'));
		}

		$input = $request->all();

		

		$this->shopUserRepository->updateRich($input, $id);

		Flash::success('ShopUser updated successfully.');

		return redirect(route('administration.shopUsers.index'));
	}

	/**
	 * Remove the specified ShopUser from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$shopUser = $this->shopUserRepository->find($id);

		if(empty($shopUser))
		{
			Flash::error('ShopUser not found');

			return redirect(route('administration.shopUsers.index'));
		}

		

		$this->shopUserRepository->delete($id);

		Flash::success('ShopUser deleted successfully.');

		return redirect(route('administration.shopUsers.index'));
	}
}
