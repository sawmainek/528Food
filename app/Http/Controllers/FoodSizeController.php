<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodSizeRequest;
use App\Http\Requests\UpdateFoodSizeRequest;
use App\Libraries\Repositories\FoodSizeRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodSize;
use App\Models\Food;

class FoodSizeController extends AppBaseController
{

	/** @var  FoodSizeRepository */
	private $foodSizeRepository;

	function __construct(FoodSizeRepository $foodSizeRepo)
	{
		$this->foodSizeRepository = $foodSizeRepo;
	}

	/**
	 * Display a listing of the FoodSize.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodSizes = FoodSize::with(['food'])->paginate(10);

		return view('foodSizes.index')
			->with('foodSizes', $foodSizes);
	}

	/**
	 * Show the form for creating a new FoodSize.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}
		return view('foodSizes.create')->with(['foods'=>$foods]);
	}

	/**
	 * Store a newly created FoodSize in storage.
	 *
	 * @param CreateFoodSizeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodSizeRequest $request)
	{
		$input = $request->all();

		

		$foodSize = $this->foodSizeRepository->create($input);

		Flash::success('FoodSize saved successfully.');

		return redirect(route('administration.foodSizes.index'));
	}

	/**
	 * Display the specified FoodSize.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodSize = FoodSize::with(['food'])->where('id', $id)->first();

		if(empty($foodSize))
		{
			Flash::error('FoodSize not found');

			return redirect(route('administration.foodSizes.index'));
		}

		return view('foodSizes.show')->with('foodSize', $foodSize);
	}

	/**
	 * Show the form for editing the specified FoodSize.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$foodSize = $this->foodSizeRepository->find($id);

		if(empty($foodSize))
		{
			Flash::error('FoodSize not found');

			return redirect(route('administration.foodSizes.index'));
		}

		return view('foodSizes.edit')->with(['foods'=>$foods, 'foodSize'=>$foodSize]);
	}

	/**
	 * Update the specified FoodSize in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodSizeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodSizeRequest $request)
	{
		$foodSize = $this->foodSizeRepository->find($id);

		if(empty($foodSize))
		{
			Flash::error('FoodSize not found');

			return redirect(route('administration.foodSizes.index'));
		}

		$input = $request->all();

		

		$this->foodSizeRepository->updateRich($input, $id);

		Flash::success('FoodSize updated successfully.');

		return redirect(route('administration.foodSizes.index'));
	}

	/**
	 * Remove the specified FoodSize from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodSize = $this->foodSizeRepository->find($id);

		if(empty($foodSize))
		{
			Flash::error('FoodSize not found');

			return redirect(route('administration.foodSizes.index'));
		}

		

		$this->foodSizeRepository->delete($id);

		Flash::success('FoodSize deleted successfully.');

		return redirect(route('administration.foodSizes.index'));
	}
}
