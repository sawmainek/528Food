<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodRequest;
use App\Http\Requests\UpdateFoodRequest;
use App\Libraries\Repositories\FoodRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Food;
use App\Models\FoodCategory;
use App\Models\Shop;

class FoodController extends AppBaseController
{

	/** @var  FoodRepository */
	private $foodRepository;

	function __construct(FoodRepository $foodRepo)
	{
		$this->foodRepository = $foodRepo;
	}

	/**
	 * Display a listing of the Food.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foods = Food::with(['foodCategory', 'shop'])->paginate(10);

		return view('foods.index')
			->with('foods', $foods);
	}

	/**
	 * Show the form for creating a new Food.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$foodCategory=FoodCategory::all();
		$foodCategories = [];
		foreach ($foodCategory as $key => $value) {
			$foodCategories[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}
		return view('foods.create')->with(['foodCategories'=>$foodCategories, 'shops'=>$shops]);
	}

	/**
	 * Store a newly created Food in storage.
	 *
	 * @param CreateFoodRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodRequest $request)
	{
		$input = $request->all();

		

		$food = $this->foodRepository->create($input);

		Flash::success('Food saved successfully.');

		return redirect(route('administration.foods.index'));
	}

	/**
	 * Display the specified Food.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$food = Food::with(['foodCategory', 'shop'])->where('id', $id)->first();

		if(empty($food))
		{
			Flash::error('Food not found');

			return redirect(route('administration.foods.index'));
		}

		return view('foods.show')->with('food', $food);
	}

	/**
	 * Show the form for editing the specified Food.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$foodCategory=FoodCategory::all();
		$foodCategories = [];
		foreach ($foodCategory as $key => $value) {
			$foodCategories[$value->id] = $value->name;
		}

		$shop=Shop::all();
		$shops = [];
		foreach ($shop as $key => $value) {
			$shops[$value->id] = $value->name;
		}

		$food = $this->foodRepository->find($id);

		if(empty($food))
		{
			Flash::error('Food not found');

			return redirect(route('administration.foods.index'));
		}

		return view('foods.edit')->with(['foodCategories'=>$foodCategories, 'shops'=>$shops, 'food'=>$food]);
	}

	/**
	 * Update the specified Food in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodRequest $request)
	{
		$food = $this->foodRepository->find($id);

		if(empty($food))
		{
			Flash::error('Food not found');

			return redirect(route('administration.foods.index'));
		}

		$input = $request->all();

		

		$this->foodRepository->updateRich($input, $id);

		Flash::success('Food updated successfully.');

		return redirect(route('administration.foods.index'));
	}

	/**
	 * Remove the specified Food from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$food = $this->foodRepository->find($id);

		if(empty($food))
		{
			Flash::error('Food not found');

			return redirect(route('administration.foods.index'));
		}

		

		$this->foodRepository->delete($id);

		Flash::success('Food deleted successfully.');

		return redirect(route('administration.foods.index'));
	}
}
