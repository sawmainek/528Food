<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateHotelPhotoRequest;
use App\Http\Requests\UpdateHotelPhotoRequest;
use App\Libraries\Repositories\HotelPhotoRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\HotelPhoto;
use App\Models\Hotel;

class HotelPhotoController extends AppBaseController
{

	/** @var  HotelPhotoRepository */
	private $hotelPhotoRepository;

	function __construct(HotelPhotoRepository $hotelPhotoRepo)
	{
		$this->hotelPhotoRepository = $hotelPhotoRepo;
	}

	/**
	 * Display a listing of the HotelPhoto.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hotelPhotos = HotelPhoto::with(['hotel'])->paginate(10);

		return view('hotelPhotos.index')
			->with('hotelPhotos', $hotelPhotos);
	}

	/**
	 * Show the form for creating a new HotelPhoto.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$hotel=Hotel::all();
		$hotels = [];
		foreach ($hotel as $key => $value) {
			$hotels[$value->id] = $value->name;
		}
		return view('hotelPhotos.create')->with(['hotels'=>$hotels]);
	}

	/**
	 * Store a newly created HotelPhoto in storage.
	 *
	 * @param CreateHotelPhotoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateHotelPhotoRequest $request)
	{
		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/hotelPhotos/');
			$input['image'] = $image['name'];
		}

		$hotelPhoto = $this->hotelPhotoRepository->create($input);

		Flash::success('HotelPhoto saved successfully.');

		return redirect(route('administration.hotelPhotos.index'));
	}

	/**
	 * Display the specified HotelPhoto.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelPhoto = HotelPhoto::with(['hotel'])->where('id', $id)->first();

		if(empty($hotelPhoto))
		{
			Flash::error('HotelPhoto not found');

			return redirect(route('administration.hotelPhotos.index'));
		}

		return view('hotelPhotos.show')->with('hotelPhoto', $hotelPhoto);
	}

	/**
	 * Show the form for editing the specified HotelPhoto.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$hotel=Hotel::all();
		$hotels = [];
		foreach ($hotel as $key => $value) {
			$hotels[$value->id] = $value->name;
		}

		$hotelPhoto = $this->hotelPhotoRepository->find($id);

		if(empty($hotelPhoto))
		{
			Flash::error('HotelPhoto not found');

			return redirect(route('administration.hotelPhotos.index'));
		}

		return view('hotelPhotos.edit')->with(['hotels'=>$hotels, 'hotelPhoto'=>$hotelPhoto]);
	}

	/**
	 * Update the specified HotelPhoto in storage.
	 *
	 * @param  int              $id
	 * @param UpdateHotelPhotoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateHotelPhotoRequest $request)
	{
		$hotelPhoto = $this->hotelPhotoRepository->find($id);

		if(empty($hotelPhoto))
		{
			Flash::error('HotelPhoto not found');

			return redirect(route('administration.hotelPhotos.index'));
		}

		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/hotelPhotos/');
			$input['image'] = $image['name'];
					@unlink(public_path('/hotelPhotos/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x100/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x200/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x400/'.$hotelPhoto->image));

		}

		$this->hotelPhotoRepository->updateRich($input, $id);

		Flash::success('HotelPhoto updated successfully.');

		return redirect(route('administration.hotelPhotos.index'));
	}

	/**
	 * Remove the specified HotelPhoto from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$hotelPhoto = $this->hotelPhotoRepository->find($id);

		if(empty($hotelPhoto))
		{
			Flash::error('HotelPhoto not found');

			return redirect(route('administration.hotelPhotos.index'));
		}

				@unlink(public_path('/hotelPhotos/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x100/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x200/'.$hotelPhoto->image));
		@unlink(public_path('/hotelPhotos/x400/'.$hotelPhoto->image));


		$this->hotelPhotoRepository->delete($id);

		Flash::success('HotelPhoto deleted successfully.');

		return redirect(route('administration.hotelPhotos.index'));
	}
}
