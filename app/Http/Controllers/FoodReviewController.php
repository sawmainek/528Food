<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodReviewRequest;
use App\Http\Requests\UpdateFoodReviewRequest;
use App\Libraries\Repositories\FoodReviewRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodReview;
use App\Models\Food;
use App\Models\User;

class FoodReviewController extends AppBaseController
{

	/** @var  FoodReviewRepository */
	private $foodReviewRepository;

	function __construct(FoodReviewRepository $foodReviewRepo)
	{
		$this->foodReviewRepository = $foodReviewRepo;
	}

	/**
	 * Display a listing of the FoodReview.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodReviews = FoodReview::with(['food', 'user'])->paginate(10);

		return view('foodReviews.index')
			->with('foodReviews', $foodReviews);
	}

	/**
	 * Show the form for creating a new FoodReview.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('foodReviews.create')->with(['foods'=>$foods, 'users'=>$users]);
	}

	/**
	 * Store a newly created FoodReview in storage.
	 *
	 * @param CreateFoodReviewRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodReviewRequest $request)
	{
		$input = $request->all();

		

		$foodReview = $this->foodReviewRepository->create($input);

		Flash::success('FoodReview saved successfully.');

		return redirect(route('administration.foodReviews.index'));
	}

	/**
	 * Display the specified FoodReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodReview = FoodReview::with(['food', 'user'])->where('id', $id)->first();

		if(empty($foodReview))
		{
			Flash::error('FoodReview not found');

			return redirect(route('administration.foodReviews.index'));
		}

		return view('foodReviews.show')->with('foodReview', $foodReview);
	}

	/**
	 * Show the form for editing the specified FoodReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$foodReview = $this->foodReviewRepository->find($id);

		if(empty($foodReview))
		{
			Flash::error('FoodReview not found');

			return redirect(route('administration.foodReviews.index'));
		}

		return view('foodReviews.edit')->with(['foods'=>$foods, 'users'=>$users, 'foodReview'=>$foodReview]);
	}

	/**
	 * Update the specified FoodReview in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodReviewRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodReviewRequest $request)
	{
		$foodReview = $this->foodReviewRepository->find($id);

		if(empty($foodReview))
		{
			Flash::error('FoodReview not found');

			return redirect(route('administration.foodReviews.index'));
		}

		$input = $request->all();

		

		$this->foodReviewRepository->updateRich($input, $id);

		Flash::success('FoodReview updated successfully.');

		return redirect(route('administration.foodReviews.index'));
	}

	/**
	 * Remove the specified FoodReview from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodReview = $this->foodReviewRepository->find($id);

		if(empty($foodReview))
		{
			Flash::error('FoodReview not found');

			return redirect(route('administration.foodReviews.index'));
		}

		

		$this->foodReviewRepository->delete($id);

		Flash::success('FoodReview deleted successfully.');

		return redirect(route('administration.foodReviews.index'));
	}
}
