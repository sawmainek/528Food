<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateIngredientRequest;
use App\Http\Requests\UpdateIngredientRequest;
use App\Libraries\Repositories\IngredientRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Ingredient;
use App\Models\Food;

class IngredientController extends AppBaseController
{

	/** @var  IngredientRepository */
	private $ingredientRepository;

	function __construct(IngredientRepository $ingredientRepo)
	{
		$this->ingredientRepository = $ingredientRepo;
	}

	/**
	 * Display a listing of the Ingredient.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ingredients = Ingredient::with(['food'])->paginate(10);

		return view('ingredients.index')
			->with('ingredients', $ingredients);
	}

	/**
	 * Show the form for creating a new Ingredient.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}
		return view('ingredients.create')->with(['foods'=>$foods]);
	}

	/**
	 * Store a newly created Ingredient in storage.
	 *
	 * @param CreateIngredientRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateIngredientRequest $request)
	{
		$input = $request->all();

		

		$ingredient = $this->ingredientRepository->create($input);

		Flash::success('Ingredient saved successfully.');

		return redirect(route('administration.ingredients.index'));
	}

	/**
	 * Display the specified Ingredient.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ingredient = Ingredient::with(['food'])->where('id', $id)->first();

		if(empty($ingredient))
		{
			Flash::error('Ingredient not found');

			return redirect(route('administration.ingredients.index'));
		}

		return view('ingredients.show')->with('ingredient', $ingredient);
	}

	/**
	 * Show the form for editing the specified Ingredient.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$ingredient = $this->ingredientRepository->find($id);

		if(empty($ingredient))
		{
			Flash::error('Ingredient not found');

			return redirect(route('administration.ingredients.index'));
		}

		return view('ingredients.edit')->with(['foods'=>$foods, 'ingredient'=>$ingredient]);
	}

	/**
	 * Update the specified Ingredient in storage.
	 *
	 * @param  int              $id
	 * @param UpdateIngredientRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateIngredientRequest $request)
	{
		$ingredient = $this->ingredientRepository->find($id);

		if(empty($ingredient))
		{
			Flash::error('Ingredient not found');

			return redirect(route('administration.ingredients.index'));
		}

		$input = $request->all();

		

		$this->ingredientRepository->updateRich($input, $id);

		Flash::success('Ingredient updated successfully.');

		return redirect(route('administration.ingredients.index'));
	}

	/**
	 * Remove the specified Ingredient from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$ingredient = $this->ingredientRepository->find($id);

		if(empty($ingredient))
		{
			Flash::error('Ingredient not found');

			return redirect(route('administration.ingredients.index'));
		}

		

		$this->ingredientRepository->delete($id);

		Flash::success('Ingredient deleted successfully.');

		return redirect(route('administration.ingredients.index'));
	}
}
