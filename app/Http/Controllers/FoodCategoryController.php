<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodCategoryRequest;
use App\Http\Requests\UpdateFoodCategoryRequest;
use App\Libraries\Repositories\FoodCategoryRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodCategory;

class FoodCategoryController extends AppBaseController
{

	/** @var  FoodCategoryRepository */
	private $foodCategoryRepository;

	function __construct(FoodCategoryRepository $foodCategoryRepo)
	{
		$this->foodCategoryRepository = $foodCategoryRepo;
	}

	/**
	 * Display a listing of the FoodCategory.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodCategories = $this->foodCategoryRepository->paginate(10);

		return view('foodCategories.index')
			->with('foodCategories', $foodCategories);
	}

	/**
	 * Show the form for creating a new FoodCategory.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('foodCategories.create');
	}

	/**
	 * Store a newly created FoodCategory in storage.
	 *
	 * @param CreateFoodCategoryRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodCategoryRequest $request)
	{
		$input = $request->all();

		if($request->file('icon')){
			$image = $this->uploadImage($request->file('icon'),'/foodCategories/');
			$input['icon'] = $image['name'];
		}
		if($request->file('background')){
			$image = $this->uploadImage($request->file('background'),'/foodCategories/');
			$input['background'] = $image['name'];
		}

		$foodCategory = $this->foodCategoryRepository->create($input);

		Flash::success('FoodCategory saved successfully.');

		return redirect(route('administration.foodCategories.index'));
	}

	/**
	 * Display the specified FoodCategory.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodCategory = $this->foodCategoryRepository->find($id);

		if(empty($foodCategory))
		{
			Flash::error('FoodCategory not found');

			return redirect(route('administration.foodCategories.index'));
		}

		return view('foodCategories.show')->with('foodCategory', $foodCategory);
	}

	/**
	 * Show the form for editing the specified FoodCategory.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$foodCategory = $this->foodCategoryRepository->find($id);

		if(empty($foodCategory))
		{
			Flash::error('FoodCategory not found');

			return redirect(route('administration.foodCategories.index'));
		}

		return view('foodCategories.edit')->with('foodCategory', $foodCategory);
	}

	/**
	 * Update the specified FoodCategory in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodCategoryRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodCategoryRequest $request)
	{
		$foodCategory = $this->foodCategoryRepository->find($id);

		if(empty($foodCategory))
		{
			Flash::error('FoodCategory not found');

			return redirect(route('administration.foodCategories.index'));
		}

		$input = $request->all();

		if($request->file('icon')){
			$image = $this->uploadImage($request->file('icon'),'/foodCategories/');
			$input['icon'] = $image['name'];
			@unlink(public_path('/foodCategories/'.$foodCategory->icon));
			@unlink(public_path('/foodCategories/x100/'.$foodCategory->icon));
			@unlink(public_path('/foodCategories/x200/'.$foodCategory->icon));
			@unlink(public_path('/foodCategories/x400/'.$foodCategory->icon));

		}
		if($request->file('background')){
			$image = $this->uploadImage($request->file('background'),'/foodCategories/');
			$input['background'] = $image['name'];

			@unlink(public_path('/foodCategories/'.$foodCategory->background));
			@unlink(public_path('/foodCategories/x100/'.$foodCategory->background));
			@unlink(public_path('/foodCategories/x200/'.$foodCategory->background));
			@unlink(public_path('/foodCategories/x400/'.$foodCategory->background));

		}
		
		$this->foodCategoryRepository->updateRich($input, $id);

		Flash::success('FoodCategory updated successfully.');

		return redirect(route('administration.foodCategories.index'));
	}

	/**
	 * Remove the specified FoodCategory from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodCategory = $this->foodCategoryRepository->find($id);

		if(empty($foodCategory))
		{
			Flash::error('FoodCategory not found');

			return redirect(route('administration.foodCategories.index'));
		}

		@unlink(public_path('/foodCategories/'.$foodCategory->icon));
		@unlink(public_path('/foodCategories/x100/'.$foodCategory->icon));
		@unlink(public_path('/foodCategories/x200/'.$foodCategory->icon));
		@unlink(public_path('/foodCategories/x400/'.$foodCategory->icon));

		@unlink(public_path('/foodCategories/'.$foodCategory->background));
		@unlink(public_path('/foodCategories/x100/'.$foodCategory->background));
		@unlink(public_path('/foodCategories/x200/'.$foodCategory->background));
		@unlink(public_path('/foodCategories/x400/'.$foodCategory->background));


		$this->foodCategoryRepository->delete($id);

		Flash::success('FoodCategory deleted successfully.');

		return redirect(route('administration.foodCategories.index'));
	}
}
