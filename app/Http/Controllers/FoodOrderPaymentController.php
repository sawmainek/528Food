<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFoodOrderPaymentRequest;
use App\Http\Requests\UpdateFoodOrderPaymentRequest;
use App\Libraries\Repositories\FoodOrderPaymentRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\FoodOrderPayment;
use App\Models\FoodOrder;
use App\Models\PaymentType;

class FoodOrderPaymentController extends AppBaseController
{

	/** @var  FoodOrderPaymentRepository */
	private $foodOrderPaymentRepository;

	function __construct(FoodOrderPaymentRepository $foodOrderPaymentRepo)
	{
		$this->foodOrderPaymentRepository = $foodOrderPaymentRepo;
	}

	/**
	 * Display a listing of the FoodOrderPayment.
	 *
	 * @return Response
	 */
	public function index()
	{
		$foodOrderPayments = FoodOrderPayment::with(['foodOrder', 'paymentType'])->paginate(10);

		return view('foodOrderPayments.index')
			->with('foodOrderPayments', $foodOrderPayments);
	}

	/**
	 * Show the form for creating a new FoodOrderPayment.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$foodOrder=FoodOrder::all();
		$foodOrders = [];
		foreach ($foodOrder as $key => $value) {
			$foodOrders[$value->id] = $value->order_date;
		}

		$paymentType=PaymentType::all();
		$paymentTypes = [];
		foreach ($paymentType as $key => $value) {
			$paymentTypes[$value->id] = $value->name;
		}
		return view('foodOrderPayments.create')->with(['foodOrders'=>$foodOrders, 'paymentTypes'=>$paymentTypes]);
	}

	/**
	 * Store a newly created FoodOrderPayment in storage.
	 *
	 * @param CreateFoodOrderPaymentRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateFoodOrderPaymentRequest $request)
	{
		$input = $request->all();

		

		$foodOrderPayment = $this->foodOrderPaymentRepository->create($input);

		Flash::success('FoodOrderPayment saved successfully.');

		return redirect(route('administration.foodOrderPayments.index'));
	}

	/**
	 * Display the specified FoodOrderPayment.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodOrderPayment = FoodOrderPayment::with(['foodOrder', 'paymentType'])->where('id', $id)->first();

		if(empty($foodOrderPayment))
		{
			Flash::error('FoodOrderPayment not found');

			return redirect(route('administration.foodOrderPayments.index'));
		}

		return view('foodOrderPayments.show')->with('foodOrderPayment', $foodOrderPayment);
	}

	/**
	 * Show the form for editing the specified FoodOrderPayment.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$foodOrder=FoodOrder::all();
		$foodOrders = [];
		foreach ($foodOrder as $key => $value) {
			$foodOrders[$value->id] = $value->order_date;
		}

		$paymentType=PaymentType::all();
		$paymentTypes = [];
		foreach ($paymentType as $key => $value) {
			$paymentTypes[$value->id] = $value->name;
		}

		$foodOrderPayment = $this->foodOrderPaymentRepository->find($id);

		if(empty($foodOrderPayment))
		{
			Flash::error('FoodOrderPayment not found');

			return redirect(route('administration.foodOrderPayments.index'));
		}

		return view('foodOrderPayments.edit')->with(['foodOrders'=>$foodOrders, 'paymentTypes'=>$paymentTypes, 'foodOrderPayment'=>$foodOrderPayment]);
	}

	/**
	 * Update the specified FoodOrderPayment in storage.
	 *
	 * @param  int              $id
	 * @param UpdateFoodOrderPaymentRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateFoodOrderPaymentRequest $request)
	{
		$foodOrderPayment = $this->foodOrderPaymentRepository->find($id);

		if(empty($foodOrderPayment))
		{
			Flash::error('FoodOrderPayment not found');

			return redirect(route('administration.foodOrderPayments.index'));
		}

		$input = $request->all();

		

		$this->foodOrderPaymentRepository->updateRich($input, $id);

		Flash::success('FoodOrderPayment updated successfully.');

		return redirect(route('administration.foodOrderPayments.index'));
	}

	/**
	 * Remove the specified FoodOrderPayment from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$foodOrderPayment = $this->foodOrderPaymentRepository->find($id);

		if(empty($foodOrderPayment))
		{
			Flash::error('FoodOrderPayment not found');

			return redirect(route('administration.foodOrderPayments.index'));
		}

		

		$this->foodOrderPaymentRepository->delete($id);

		Flash::success('FoodOrderPayment deleted successfully.');

		return redirect(route('administration.foodOrderPayments.index'));
	}
}
