<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateHotelRequest;
use App\Http\Requests\UpdateHotelRequest;
use App\Libraries\Repositories\HotelRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Hotel;
use App\Models\Agent;
use App\Models\HotelType;

class HotelController extends AppBaseController
{

	/** @var  HotelRepository */
	private $hotelRepository;

	function __construct(HotelRepository $hotelRepo)
	{
		$this->hotelRepository = $hotelRepo;
	}

	/**
	 * Display a listing of the Hotel.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hotels = Hotel::with(['agent', 'hotelType'])->paginate(10);

		return view('hotels.index')
			->with('hotels', $hotels);
	}

	/**
	 * Show the form for creating a new Hotel.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$hotelType=HotelType::all();
		$hotelTypes = [];
		foreach ($hotelType as $key => $value) {
			$hotelTypes[$value->id] = $value->name;
		}
		return view('hotels.create')->with(['agents'=>$agents, 'hotelTypes'=>$hotelTypes]);
	}

	/**
	 * Store a newly created Hotel in storage.
	 *
	 * @param CreateHotelRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateHotelRequest $request)
	{
		$input = $request->all();

		

		$hotel = $this->hotelRepository->create($input);

		Flash::success('Hotel saved successfully.');

		return redirect(route('administration.hotels.index'));
	}

	/**
	 * Display the specified Hotel.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotel = Hotel::with(['agent', 'hotelType'])->where('id', $id)->first();

		if(empty($hotel))
		{
			Flash::error('Hotel not found');

			return redirect(route('administration.hotels.index'));
		}

		return view('hotels.show')->with('hotel', $hotel);
	}

	/**
	 * Show the form for editing the specified Hotel.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$hotelType=HotelType::all();
		$hotelTypes = [];
		foreach ($hotelType as $key => $value) {
			$hotelTypes[$value->id] = $value->name;
		}

		$hotel = $this->hotelRepository->find($id);

		if(empty($hotel))
		{
			Flash::error('Hotel not found');

			return redirect(route('administration.hotels.index'));
		}

		return view('hotels.edit')->with(['agents'=>$agents, 'hotelTypes'=>$hotelTypes, 'hotel'=>$hotel]);
	}

	/**
	 * Update the specified Hotel in storage.
	 *
	 * @param  int              $id
	 * @param UpdateHotelRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateHotelRequest $request)
	{
		$hotel = $this->hotelRepository->find($id);

		if(empty($hotel))
		{
			Flash::error('Hotel not found');

			return redirect(route('administration.hotels.index'));
		}

		$input = $request->all();

		

		$this->hotelRepository->updateRich($input, $id);

		Flash::success('Hotel updated successfully.');

		return redirect(route('administration.hotels.index'));
	}

	/**
	 * Remove the specified Hotel from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$hotel = $this->hotelRepository->find($id);

		if(empty($hotel))
		{
			Flash::error('Hotel not found');

			return redirect(route('administration.hotels.index'));
		}

		

		$this->hotelRepository->delete($id);

		Flash::success('Hotel deleted successfully.');

		return redirect(route('administration.hotels.index'));
	}
}
