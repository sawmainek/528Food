<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTourPackageRequest;
use App\Http\Requests\UpdateTourPackageRequest;
use App\Libraries\Repositories\TourPackageRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\TourPackage;
use App\Models\Agent;
use App\Models\TourPackageType;

class TourPackageController extends AppBaseController
{

	/** @var  TourPackageRepository */
	private $tourPackageRepository;

	function __construct(TourPackageRepository $tourPackageRepo)
	{
		$this->tourPackageRepository = $tourPackageRepo;
	}

	/**
	 * Display a listing of the TourPackage.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tourPackages = TourPackage::with(['agent', 'tourPackageType'])->paginate(10);

		return view('tourPackages.index')
			->with('tourPackages', $tourPackages);
	}

	/**
	 * Show the form for creating a new TourPackage.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$tourPackageType=TourPackageType::all();
		$tourPackageTypes = [];
		foreach ($tourPackageType as $key => $value) {
			$tourPackageTypes[$value->id] = $value->name;
		}
		return view('tourPackages.create')->with(['agents'=>$agents, 'tourPackageTypes'=>$tourPackageTypes]);
	}

	/**
	 * Store a newly created TourPackage in storage.
	 *
	 * @param CreateTourPackageRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTourPackageRequest $request)
	{
		$input = $request->all();

		

		$tourPackage = $this->tourPackageRepository->create($input);

		Flash::success('TourPackage saved successfully.');

		return redirect(route('administration.tourPackages.index'));
	}

	/**
	 * Display the specified TourPackage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tourPackage = TourPackage::with(['agent', 'tourPackageType'])->where('id', $id)->first();

		if(empty($tourPackage))
		{
			Flash::error('TourPackage not found');

			return redirect(route('administration.tourPackages.index'));
		}

		return view('tourPackages.show')->with('tourPackage', $tourPackage);
	}

	/**
	 * Show the form for editing the specified TourPackage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->name;
		}

		$tourPackageType=TourPackageType::all();
		$tourPackageTypes = [];
		foreach ($tourPackageType as $key => $value) {
			$tourPackageTypes[$value->id] = $value->name;
		}

		$tourPackage = $this->tourPackageRepository->find($id);

		if(empty($tourPackage))
		{
			Flash::error('TourPackage not found');

			return redirect(route('administration.tourPackages.index'));
		}

		return view('tourPackages.edit')->with(['agents'=>$agents, 'tourPackageTypes'=>$tourPackageTypes, 'tourPackage'=>$tourPackage]);
	}

	/**
	 * Update the specified TourPackage in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTourPackageRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTourPackageRequest $request)
	{
		$tourPackage = $this->tourPackageRepository->find($id);

		if(empty($tourPackage))
		{
			Flash::error('TourPackage not found');

			return redirect(route('administration.tourPackages.index'));
		}

		$input = $request->all();

		

		$this->tourPackageRepository->updateRich($input, $id);

		Flash::success('TourPackage updated successfully.');

		return redirect(route('administration.tourPackages.index'));
	}

	/**
	 * Remove the specified TourPackage from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tourPackage = $this->tourPackageRepository->find($id);

		if(empty($tourPackage))
		{
			Flash::error('TourPackage not found');

			return redirect(route('administration.tourPackages.index'));
		}

		

		$this->tourPackageRepository->delete($id);

		Flash::success('TourPackage deleted successfully.');

		return redirect(route('administration.tourPackages.index'));
	}
}
