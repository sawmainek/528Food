<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAgentReviewRequest;
use App\Http\Requests\UpdateAgentReviewRequest;
use App\Libraries\Repositories\AgentReviewRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\AgentReview;
use App\Models\User;

class AgentReviewController extends AppBaseController
{

	/** @var  AgentReviewRepository */
	private $agentReviewRepository;

	function __construct(AgentReviewRepository $agentReviewRepo)
	{
		$this->agentReviewRepository = $agentReviewRepo;
	}

	/**
	 * Display a listing of the AgentReview.
	 *
	 * @return Response
	 */
	public function index()
	{
		$agentReviews = AgentReview::with(['user'])->paginate(10);

		return view('agentReviews.index')
			->with('agentReviews', $agentReviews);
	}

	/**
	 * Show the form for creating a new AgentReview.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('agentReviews.create')->with(['users'=>$users]);
	}

	/**
	 * Store a newly created AgentReview in storage.
	 *
	 * @param CreateAgentReviewRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateAgentReviewRequest $request)
	{
		$input = $request->all();

		

		$agentReview = $this->agentReviewRepository->create($input);

		Flash::success('AgentReview saved successfully.');

		return redirect(route('administration.agentReviews.index'));
	}

	/**
	 * Display the specified AgentReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$agentReview = AgentReview::with(['user'])->where('id', $id)->first();

		if(empty($agentReview))
		{
			Flash::error('AgentReview not found');

			return redirect(route('administration.agentReviews.index'));
		}

		return view('agentReviews.show')->with('agentReview', $agentReview);
	}

	/**
	 * Show the form for editing the specified AgentReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$agentReview = $this->agentReviewRepository->find($id);

		if(empty($agentReview))
		{
			Flash::error('AgentReview not found');

			return redirect(route('administration.agentReviews.index'));
		}

		return view('agentReviews.edit')->with(['users'=>$users, 'agentReview'=>$agentReview]);
	}

	/**
	 * Update the specified AgentReview in storage.
	 *
	 * @param  int              $id
	 * @param UpdateAgentReviewRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateAgentReviewRequest $request)
	{
		$agentReview = $this->agentReviewRepository->find($id);

		if(empty($agentReview))
		{
			Flash::error('AgentReview not found');

			return redirect(route('administration.agentReviews.index'));
		}

		$input = $request->all();

		

		$this->agentReviewRepository->updateRich($input, $id);

		Flash::success('AgentReview updated successfully.');

		return redirect(route('administration.agentReviews.index'));
	}

	/**
	 * Remove the specified AgentReview from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$agentReview = $this->agentReviewRepository->find($id);

		if(empty($agentReview))
		{
			Flash::error('AgentReview not found');

			return redirect(route('administration.agentReviews.index'));
		}

		

		$this->agentReviewRepository->delete($id);

		Flash::success('AgentReview deleted successfully.');

		return redirect(route('administration.agentReviews.index'));
	}
}
