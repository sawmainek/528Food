<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateHotelReviewRequest;
use App\Http\Requests\UpdateHotelReviewRequest;
use App\Libraries\Repositories\HotelReviewRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\HotelReview;
use App\Models\User;

class HotelReviewController extends AppBaseController
{

	/** @var  HotelReviewRepository */
	private $hotelReviewRepository;

	function __construct(HotelReviewRepository $hotelReviewRepo)
	{
		$this->hotelReviewRepository = $hotelReviewRepo;
	}

	/**
	 * Display a listing of the HotelReview.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hotelReviews = HotelReview::with(['user'])->paginate(10);

		return view('hotelReviews.index')
			->with('hotelReviews', $hotelReviews);
	}

	/**
	 * Show the form for creating a new HotelReview.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('hotelReviews.create')->with(['users'=>$users]);
	}

	/**
	 * Store a newly created HotelReview in storage.
	 *
	 * @param CreateHotelReviewRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateHotelReviewRequest $request)
	{
		$input = $request->all();

		

		$hotelReview = $this->hotelReviewRepository->create($input);

		Flash::success('HotelReview saved successfully.');

		return redirect(route('administration.hotelReviews.index'));
	}

	/**
	 * Display the specified HotelReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelReview = HotelReview::with(['user'])->where('id', $id)->first();

		if(empty($hotelReview))
		{
			Flash::error('HotelReview not found');

			return redirect(route('administration.hotelReviews.index'));
		}

		return view('hotelReviews.show')->with('hotelReview', $hotelReview);
	}

	/**
	 * Show the form for editing the specified HotelReview.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$hotelReview = $this->hotelReviewRepository->find($id);

		if(empty($hotelReview))
		{
			Flash::error('HotelReview not found');

			return redirect(route('administration.hotelReviews.index'));
		}

		return view('hotelReviews.edit')->with(['users'=>$users, 'hotelReview'=>$hotelReview]);
	}

	/**
	 * Update the specified HotelReview in storage.
	 *
	 * @param  int              $id
	 * @param UpdateHotelReviewRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateHotelReviewRequest $request)
	{
		$hotelReview = $this->hotelReviewRepository->find($id);

		if(empty($hotelReview))
		{
			Flash::error('HotelReview not found');

			return redirect(route('administration.hotelReviews.index'));
		}

		$input = $request->all();

		

		$this->hotelReviewRepository->updateRich($input, $id);

		Flash::success('HotelReview updated successfully.');

		return redirect(route('administration.hotelReviews.index'));
	}

	/**
	 * Remove the specified HotelReview from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$hotelReview = $this->hotelReviewRepository->find($id);

		if(empty($hotelReview))
		{
			Flash::error('HotelReview not found');

			return redirect(route('administration.hotelReviews.index'));
		}

		

		$this->hotelReviewRepository->delete($id);

		Flash::success('HotelReview deleted successfully.');

		return redirect(route('administration.hotelReviews.index'));
	}
}
