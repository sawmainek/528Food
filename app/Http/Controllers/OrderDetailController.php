<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Libraries\Repositories\OrderDetailRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\OrderDetail;
use App\Models\FoodOrder;
use App\Models\Food;
use App\Models\FoodSize;

class OrderDetailController extends AppBaseController
{

	/** @var  OrderDetailRepository */
	private $orderDetailRepository;

	function __construct(OrderDetailRepository $orderDetailRepo)
	{
		$this->orderDetailRepository = $orderDetailRepo;
	}

	/**
	 * Display a listing of the OrderDetail.
	 *
	 * @return Response
	 */
	public function index()
	{
		$orderDetails = OrderDetail::with(['foodOrder', 'food', 'foodSize'])->paginate(10);

		return view('orderDetails.index')
			->with('orderDetails', $orderDetails);
	}

	/**
	 * Show the form for creating a new OrderDetail.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$foodOrder=FoodOrder::all();
		$foodOrders = [];
		foreach ($foodOrder as $key => $value) {
			$foodOrders[$value->id] = $value->order_date;
		}

		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$foodSize=FoodSize::all();
		$foodSizes = [];
		foreach ($foodSize as $key => $value) {
			$foodSizes[$value->id] = $value->size;
		}
		return view('orderDetails.create')->with(['foodOrders'=>$foodOrders, 'foods'=>$foods, 'foodSizes'=>$foodSizes]);
	}

	/**
	 * Store a newly created OrderDetail in storage.
	 *
	 * @param CreateOrderDetailRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrderDetailRequest $request)
	{
		$input = $request->all();

		

		$orderDetail = $this->orderDetailRepository->create($input);

		Flash::success('OrderDetail saved successfully.');

		return redirect(route('administration.orderDetails.index'));
	}

	/**
	 * Display the specified OrderDetail.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$orderDetail = OrderDetail::with(['foodOrder', 'food', 'foodSize'])->where('id', $id)->first();

		if(empty($orderDetail))
		{
			Flash::error('OrderDetail not found');

			return redirect(route('administration.orderDetails.index'));
		}

		return view('orderDetails.show')->with('orderDetail', $orderDetail);
	}

	/**
	 * Show the form for editing the specified OrderDetail.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$foodOrder=FoodOrder::all();
		$foodOrders = [];
		foreach ($foodOrder as $key => $value) {
			$foodOrders[$value->id] = $value->order_date;
		}

		$food=Food::all();
		$foods = [];
		foreach ($food as $key => $value) {
			$foods[$value->id] = $value->name;
		}

		$foodSize=FoodSize::all();
		$foodSizes = [];
		foreach ($foodSize as $key => $value) {
			$foodSizes[$value->id] = $value->size;
		}

		$orderDetail = $this->orderDetailRepository->find($id);

		if(empty($orderDetail))
		{
			Flash::error('OrderDetail not found');

			return redirect(route('administration.orderDetails.index'));
		}

		return view('orderDetails.edit')->with(['foodOrders'=>$foodOrders, 'foods'=>$foods, 'foodSizes'=>$foodSizes, 'orderDetail'=>$orderDetail]);
	}

	/**
	 * Update the specified OrderDetail in storage.
	 *
	 * @param  int              $id
	 * @param UpdateOrderDetailRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOrderDetailRequest $request)
	{
		$orderDetail = $this->orderDetailRepository->find($id);

		if(empty($orderDetail))
		{
			Flash::error('OrderDetail not found');

			return redirect(route('administration.orderDetails.index'));
		}

		$input = $request->all();

		

		$this->orderDetailRepository->updateRich($input, $id);

		Flash::success('OrderDetail updated successfully.');

		return redirect(route('administration.orderDetails.index'));
	}

	/**
	 * Remove the specified OrderDetail from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$orderDetail = $this->orderDetailRepository->find($id);

		if(empty($orderDetail))
		{
			Flash::error('OrderDetail not found');

			return redirect(route('administration.orderDetails.index'));
		}

		

		$this->orderDetailRepository->delete($id);

		Flash::success('OrderDetail deleted successfully.');

		return redirect(route('administration.orderDetails.index'));
	}
}
