<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserMessageRequest;
use App\Http\Requests\UpdateUserMessageRequest;
use App\Libraries\Repositories\UserMessageRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\UserMessage;
use App\Models\User;

class UserMessageController extends AppBaseController
{

	/** @var  UserMessageRepository */
	private $userMessageRepository;

	function __construct(UserMessageRepository $userMessageRepo)
	{
		$this->userMessageRepository = $userMessageRepo;
	}

	/**
	 * Display a listing of the UserMessage.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userMessages = UserMessage::with(['user'])->paginate(10);

		return view('userMessages.index')
			->with('userMessages', $userMessages);
	}

	/**
	 * Show the form for creating a new UserMessage.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('userMessages.create')->with(['users'=>$users]);
	}

	/**
	 * Store a newly created UserMessage in storage.
	 *
	 * @param CreateUserMessageRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserMessageRequest $request)
	{
		$input = $request->all();

		

		$userMessage = $this->userMessageRepository->create($input);

		Flash::success('UserMessage saved successfully.');

		return redirect(route('administration.userMessages.index'));
	}

	/**
	 * Display the specified UserMessage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userMessage = UserMessage::with(['user'])->where('id', $id)->first();

		if(empty($userMessage))
		{
			Flash::error('UserMessage not found');

			return redirect(route('administration.userMessages.index'));
		}

		return view('userMessages.show')->with('userMessage', $userMessage);
	}

	/**
	 * Show the form for editing the specified UserMessage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$userMessage = $this->userMessageRepository->find($id);

		if(empty($userMessage))
		{
			Flash::error('UserMessage not found');

			return redirect(route('administration.userMessages.index'));
		}

		return view('userMessages.edit')->with(['users'=>$users, 'userMessage'=>$userMessage]);
	}

	/**
	 * Update the specified UserMessage in storage.
	 *
	 * @param  int              $id
	 * @param UpdateUserMessageRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserMessageRequest $request)
	{
		$userMessage = $this->userMessageRepository->find($id);

		if(empty($userMessage))
		{
			Flash::error('UserMessage not found');

			return redirect(route('administration.userMessages.index'));
		}

		$input = $request->all();

		

		$this->userMessageRepository->updateRich($input, $id);

		Flash::success('UserMessage updated successfully.');

		return redirect(route('administration.userMessages.index'));
	}

	/**
	 * Remove the specified UserMessage from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$userMessage = $this->userMessageRepository->find($id);

		if(empty($userMessage))
		{
			Flash::error('UserMessage not found');

			return redirect(route('administration.userMessages.index'));
		}

		

		$this->userMessageRepository->delete($id);

		Flash::success('UserMessage deleted successfully.');

		return redirect(route('administration.userMessages.index'));
	}
}
