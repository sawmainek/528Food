<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodOrderPaymentRepository;
use App\Models\FoodOrderPayment;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class FoodOrderPaymentAPIController extends AppBaseController
{
	/** @var  FoodOrderPaymentRepository */
	private $foodOrderPaymentRepository;

	function __construct(FoodOrderPaymentRepository $foodOrderPaymentRepo)
	{
		$this->foodOrderPaymentRepository = $foodOrderPaymentRepo;
	}

	/**
	 * Display a listing of the FoodOrderPayment.
	 * GET|HEAD /foodOrderPayments
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$foodOrderPayments = FoodOrderPayment::with(['foodOrder', 'paymentType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($foodOrderPayments);
	}

	/**
	 * Show the form for creating a new FoodOrderPayment.
	 * GET|HEAD /foodOrderPayments/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created FoodOrderPayment in storage.
	 * POST /foodOrderPayments
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(FoodOrderPayment::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodOrderPayment::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$foodOrderPayments = $this->foodOrderPaymentRepository->create($input);

		return $this->sendResponse($foodOrderPayments->toArray(), "FoodOrderPayment saved successfully");
	}

	/**
	 * Display the specified FoodOrderPayment.
	 * GET|HEAD /foodOrderPayments/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodOrderPayment = $this->foodOrderPaymentRepository->apiFindOrFail($id);

		return $this->sendResponse($foodOrderPayment->toArray(), "FoodOrderPayment retrieved successfully");
	}

	/**
	 * Show the form for editing the specified FoodOrderPayment.
	 * GET|HEAD /foodOrderPayments/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified FoodOrderPayment in storage.
	 * PUT/PATCH /foodOrderPayments/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(FoodOrderPayment::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodOrderPayment::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var FoodOrderPayment $foodOrderPayment */
		$foodOrderPayment = $this->foodOrderPaymentRepository->apiFindOrFail($id);

		$result = $this->foodOrderPaymentRepository->updateRich($input, $id);

		$foodOrderPayment = $foodOrderPayment->fresh();

		return $this->sendResponse($foodOrderPayment->toArray(), "FoodOrderPayment updated successfully");
	}

	/**
	 * Remove the specified FoodOrderPayment from storage.
	 * DELETE /foodOrderPayments/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodOrderPaymentRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "FoodOrderPayment deleted successfully");
	}
}
