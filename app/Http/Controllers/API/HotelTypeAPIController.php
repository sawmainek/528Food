<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HotelTypeRepository;
use App\Models\HotelType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HotelTypeAPIController extends AppBaseController
{
	/** @var  HotelTypeRepository */
	private $hotelTypeRepository;

	function __construct(HotelTypeRepository $hotelTypeRepo)
	{
		$this->hotelTypeRepository = $hotelTypeRepo;
	}

	/**
	 * Display a listing of the HotelType.
	 * GET|HEAD /hotelTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$hotelTypes = HotelType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($hotelTypes);
	}

	/**
	 * Show the form for creating a new HotelType.
	 * GET|HEAD /hotelTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created HotelType in storage.
	 * POST /hotelTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(HotelType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$hotelTypes = $this->hotelTypeRepository->create($input);

		return $this->sendResponse($hotelTypes->toArray(), "HotelType saved successfully");
	}

	/**
	 * Display the specified HotelType.
	 * GET|HEAD /hotelTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelType = $this->hotelTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($hotelType->toArray(), "HotelType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified HotelType.
	 * GET|HEAD /hotelTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified HotelType in storage.
	 * PUT/PATCH /hotelTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(HotelType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var HotelType $hotelType */
		$hotelType = $this->hotelTypeRepository->apiFindOrFail($id);

		$result = $this->hotelTypeRepository->updateRich($input, $id);

		$hotelType = $hotelType->fresh();

		return $this->sendResponse($hotelType->toArray(), "HotelType updated successfully");
	}

	/**
	 * Remove the specified HotelType from storage.
	 * DELETE /hotelTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->hotelTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "HotelType deleted successfully");
	}
}
