<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TourPackageTypeRepository;
use App\Models\TourPackageType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TourPackageTypeAPIController extends AppBaseController
{
	/** @var  TourPackageTypeRepository */
	private $tourPackageTypeRepository;

	function __construct(TourPackageTypeRepository $tourPackageTypeRepo)
	{
		$this->tourPackageTypeRepository = $tourPackageTypeRepo;
	}

	/**
	 * Display a listing of the TourPackageType.
	 * GET|HEAD /tourPackageTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$tourPackageTypes = TourPackageType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($tourPackageTypes);
	}

	/**
	 * Show the form for creating a new TourPackageType.
	 * GET|HEAD /tourPackageTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TourPackageType in storage.
	 * POST /tourPackageTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TourPackageType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TourPackageType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$tourPackageTypes = $this->tourPackageTypeRepository->create($input);

		return $this->sendResponse($tourPackageTypes->toArray(), "TourPackageType saved successfully");
	}

	/**
	 * Display the specified TourPackageType.
	 * GET|HEAD /tourPackageTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tourPackageType = $this->tourPackageTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($tourPackageType->toArray(), "TourPackageType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TourPackageType.
	 * GET|HEAD /tourPackageTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified TourPackageType in storage.
	 * PUT/PATCH /tourPackageTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(TourPackageType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TourPackageType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var TourPackageType $tourPackageType */
		$tourPackageType = $this->tourPackageTypeRepository->apiFindOrFail($id);

		$result = $this->tourPackageTypeRepository->updateRich($input, $id);

		$tourPackageType = $tourPackageType->fresh();

		return $this->sendResponse($tourPackageType->toArray(), "TourPackageType updated successfully");
	}

	/**
	 * Remove the specified TourPackageType from storage.
	 * DELETE /tourPackageTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tourPackageTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TourPackageType deleted successfully");
	}
}
