<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\UserLikeRepository;
use App\Models\UserLike;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class UserLikeAPIController extends AppBaseController
{
	/** @var  UserLikeRepository */
	private $userLikeRepository;

	function __construct(UserLikeRepository $userLikeRepo)
	{
		$this->userLikeRepository = $userLikeRepo;
	}

	/**
	 * Display a listing of the UserLike.
	 * GET|HEAD /userLikes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$userLikes = UserLike::with(['food', 'user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($userLikes);
	}

	/**
	 * Show the form for creating a new UserLike.
	 * GET|HEAD /userLikes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created UserLike in storage.
	 * POST /userLikes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(UserLike::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserLike::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$userLikes = $this->userLikeRepository->create($input);

		return $this->sendResponse($userLikes->toArray(), "UserLike saved successfully");
	}

	/**
	 * Display the specified UserLike.
	 * GET|HEAD /userLikes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userLike = $this->userLikeRepository->apiFindOrFail($id);

		return $this->sendResponse($userLike->toArray(), "UserLike retrieved successfully");
	}

	/**
	 * Show the form for editing the specified UserLike.
	 * GET|HEAD /userLikes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified UserLike in storage.
	 * PUT/PATCH /userLikes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(UserLike::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserLike::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var UserLike $userLike */
		$userLike = $this->userLikeRepository->apiFindOrFail($id);

		$result = $this->userLikeRepository->updateRich($input, $id);

		$userLike = $userLike->fresh();

		return $this->sendResponse($userLike->toArray(), "UserLike updated successfully");
	}

	/**
	 * Remove the specified UserLike from storage.
	 * DELETE /userLikes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->userLikeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "UserLike deleted successfully");
	}
}
