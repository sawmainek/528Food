<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TicketOperatorRepository;
use App\Models\TicketOperator;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TicketOperatorAPIController extends AppBaseController
{
	/** @var  TicketOperatorRepository */
	private $ticketOperatorRepository;

	function __construct(TicketOperatorRepository $ticketOperatorRepo)
	{
		$this->ticketOperatorRepository = $ticketOperatorRepo;
	}

	/**
	 * Display a listing of the TicketOperator.
	 * GET|HEAD /ticketOperators
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$ticketOperators = TicketOperator::with(['agent', 'operatorType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($ticketOperators);
	}

	/**
	 * Show the form for creating a new TicketOperator.
	 * GET|HEAD /ticketOperators/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TicketOperator in storage.
	 * POST /ticketOperators
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TicketOperator::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TicketOperator::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$ticketOperators = $this->ticketOperatorRepository->create($input);

		return $this->sendResponse($ticketOperators->toArray(), "TicketOperator saved successfully");
	}

	/**
	 * Display the specified TicketOperator.
	 * GET|HEAD /ticketOperators/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticketOperator = $this->ticketOperatorRepository->apiFindOrFail($id);

		return $this->sendResponse($ticketOperator->toArray(), "TicketOperator retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TicketOperator.
	 * GET|HEAD /ticketOperators/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified TicketOperator in storage.
	 * PUT/PATCH /ticketOperators/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(TicketOperator::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TicketOperator::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var TicketOperator $ticketOperator */
		$ticketOperator = $this->ticketOperatorRepository->apiFindOrFail($id);

		$result = $this->ticketOperatorRepository->updateRich($input, $id);

		$ticketOperator = $ticketOperator->fresh();

		return $this->sendResponse($ticketOperator->toArray(), "TicketOperator updated successfully");
	}

	/**
	 * Remove the specified TicketOperator from storage.
	 * DELETE /ticketOperators/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ticketOperatorRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TicketOperator deleted successfully");
	}
}
