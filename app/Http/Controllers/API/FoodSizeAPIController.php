<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodSizeRepository;
use App\Models\FoodSize;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class FoodSizeAPIController extends AppBaseController
{
	/** @var  FoodSizeRepository */
	private $foodSizeRepository;

	function __construct(FoodSizeRepository $foodSizeRepo)
	{
		$this->foodSizeRepository = $foodSizeRepo;
	}

	/**
	 * Display a listing of the FoodSize.
	 * GET|HEAD /foodSizes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$foodSizes = FoodSize::with(['food'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($foodSizes);
	}

	/**
	 * Show the form for creating a new FoodSize.
	 * GET|HEAD /foodSizes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created FoodSize in storage.
	 * POST /foodSizes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(FoodSize::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodSize::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$foodSizes = $this->foodSizeRepository->create($input);

		return $this->sendResponse($foodSizes->toArray(), "FoodSize saved successfully");
	}

	/**
	 * Display the specified FoodSize.
	 * GET|HEAD /foodSizes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodSize = $this->foodSizeRepository->apiFindOrFail($id);

		return $this->sendResponse($foodSize->toArray(), "FoodSize retrieved successfully");
	}

	/**
	 * Show the form for editing the specified FoodSize.
	 * GET|HEAD /foodSizes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified FoodSize in storage.
	 * PUT/PATCH /foodSizes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(FoodSize::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodSize::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var FoodSize $foodSize */
		$foodSize = $this->foodSizeRepository->apiFindOrFail($id);

		$result = $this->foodSizeRepository->updateRich($input, $id);

		$foodSize = $foodSize->fresh();

		return $this->sendResponse($foodSize->toArray(), "FoodSize updated successfully");
	}

	/**
	 * Remove the specified FoodSize from storage.
	 * DELETE /foodSizes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodSizeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "FoodSize deleted successfully");
	}
}
