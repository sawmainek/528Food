<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PaymentTypeRepository;
use App\Models\PaymentType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PaymentTypeAPIController extends AppBaseController
{
	/** @var  PaymentTypeRepository */
	private $paymentTypeRepository;

	function __construct(PaymentTypeRepository $paymentTypeRepo)
	{
		$this->paymentTypeRepository = $paymentTypeRepo;
	}

	/**
	 * Display a listing of the PaymentType.
	 * GET|HEAD /paymentTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$paymentTypes = PaymentType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($paymentTypes);
	}

	/**
	 * Show the form for creating a new PaymentType.
	 * GET|HEAD /paymentTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created PaymentType in storage.
	 * POST /paymentTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(PaymentType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, PaymentType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$paymentTypes = $this->paymentTypeRepository->create($input);

		return $this->sendResponse($paymentTypes->toArray(), "PaymentType saved successfully");
	}

	/**
	 * Display the specified PaymentType.
	 * GET|HEAD /paymentTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$paymentType = $this->paymentTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($paymentType->toArray(), "PaymentType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified PaymentType.
	 * GET|HEAD /paymentTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified PaymentType in storage.
	 * PUT/PATCH /paymentTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(PaymentType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, PaymentType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var PaymentType $paymentType */
		$paymentType = $this->paymentTypeRepository->apiFindOrFail($id);

		$result = $this->paymentTypeRepository->updateRich($input, $id);

		$paymentType = $paymentType->fresh();

		return $this->sendResponse($paymentType->toArray(), "PaymentType updated successfully");
	}

	/**
	 * Remove the specified PaymentType from storage.
	 * DELETE /paymentTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->paymentTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "PaymentType deleted successfully");
	}
}
