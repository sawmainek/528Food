<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RoomPriceRepository;
use App\Models\RoomPrice;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class RoomPriceAPIController extends AppBaseController
{
	/** @var  RoomPriceRepository */
	private $roomPriceRepository;

	function __construct(RoomPriceRepository $roomPriceRepo)
	{
		$this->roomPriceRepository = $roomPriceRepo;
	}

	/**
	 * Display a listing of the RoomPrice.
	 * GET|HEAD /roomPrices
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$roomPrices = RoomPrice::with(['hotel', 'roomType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($roomPrices);
	}

	/**
	 * Show the form for creating a new RoomPrice.
	 * GET|HEAD /roomPrices/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created RoomPrice in storage.
	 * POST /roomPrices
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(RoomPrice::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, RoomPrice::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$roomPrices = $this->roomPriceRepository->create($input);

		return $this->sendResponse($roomPrices->toArray(), "RoomPrice saved successfully");
	}

	/**
	 * Display the specified RoomPrice.
	 * GET|HEAD /roomPrices/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roomPrice = $this->roomPriceRepository->apiFindOrFail($id);

		return $this->sendResponse($roomPrice->toArray(), "RoomPrice retrieved successfully");
	}

	/**
	 * Show the form for editing the specified RoomPrice.
	 * GET|HEAD /roomPrices/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified RoomPrice in storage.
	 * PUT/PATCH /roomPrices/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(RoomPrice::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, RoomPrice::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var RoomPrice $roomPrice */
		$roomPrice = $this->roomPriceRepository->apiFindOrFail($id);

		$result = $this->roomPriceRepository->updateRich($input, $id);

		$roomPrice = $roomPrice->fresh();

		return $this->sendResponse($roomPrice->toArray(), "RoomPrice updated successfully");
	}

	/**
	 * Remove the specified RoomPrice from storage.
	 * DELETE /roomPrices/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->roomPriceRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "RoomPrice deleted successfully");
	}
}
