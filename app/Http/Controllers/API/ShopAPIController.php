<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ShopRepository;
use App\Models\Shop;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ShopAPIController extends AppBaseController
{
	/** @var  ShopRepository */
	private $shopRepository;

	function __construct(ShopRepository $shopRepo)
	{
		$this->shopRepository = $shopRepo;
	}

	/**
	 * Display a listing of the Shop.
	 * GET|HEAD /shops
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$shops = Shop::with(['shoptype'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($shops);
	}

	/**
	 * Show the form for creating a new Shop.
	 * GET|HEAD /shops/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Shop in storage.
	 * POST /shops
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Shop::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Shop::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$shops = $this->shopRepository->create($input);

		return $this->sendResponse($shops->toArray(), "Shop saved successfully");
	}

	/**
	 * Display the specified Shop.
	 * GET|HEAD /shops/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shop = $this->shopRepository->apiFindOrFail($id);

		return $this->sendResponse($shop->toArray(), "Shop retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Shop.
	 * GET|HEAD /shops/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Shop in storage.
	 * PUT/PATCH /shops/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Shop::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Shop::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Shop $shop */
		$shop = $this->shopRepository->apiFindOrFail($id);

		$result = $this->shopRepository->updateRich($input, $id);

		$shop = $shop->fresh();

		return $this->sendResponse($shop->toArray(), "Shop updated successfully");
	}

	/**
	 * Remove the specified Shop from storage.
	 * DELETE /shops/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->shopRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Shop deleted successfully");
	}
}
