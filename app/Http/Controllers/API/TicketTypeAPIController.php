<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TicketTypeRepository;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TicketTypeAPIController extends AppBaseController
{
	/** @var  TicketTypeRepository */
	private $ticketTypeRepository;

	function __construct(TicketTypeRepository $ticketTypeRepo)
	{
		$this->ticketTypeRepository = $ticketTypeRepo;
	}

	/**
	 * Display a listing of the TicketType.
	 * GET|HEAD /ticketTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$ticketTypes = TicketType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($ticketTypes);
	}

	/**
	 * Show the form for creating a new TicketType.
	 * GET|HEAD /ticketTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TicketType in storage.
	 * POST /ticketTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TicketType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TicketType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$ticketTypes = $this->ticketTypeRepository->create($input);

		return $this->sendResponse($ticketTypes->toArray(), "TicketType saved successfully");
	}

	/**
	 * Display the specified TicketType.
	 * GET|HEAD /ticketTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticketType = $this->ticketTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($ticketType->toArray(), "TicketType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TicketType.
	 * GET|HEAD /ticketTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified TicketType in storage.
	 * PUT/PATCH /ticketTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(TicketType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TicketType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var TicketType $ticketType */
		$ticketType = $this->ticketTypeRepository->apiFindOrFail($id);

		$result = $this->ticketTypeRepository->updateRich($input, $id);

		$ticketType = $ticketType->fresh();

		return $this->sendResponse($ticketType->toArray(), "TicketType updated successfully");
	}

	/**
	 * Remove the specified TicketType from storage.
	 * DELETE /ticketTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ticketTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TicketType deleted successfully");
	}
}
