<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\UserRoleRepository;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class UserRoleAPIController extends AppBaseController
{
	/** @var  UserRoleRepository */
	private $userRoleRepository;

	function __construct(UserRoleRepository $userRoleRepo)
	{
		$this->userRoleRepository = $userRoleRepo;
	}

	/**
	 * Display a listing of the UserRole.
	 * GET|HEAD /userRoles
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$userRoles = UserRole::with(['role', 'user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($userRoles);
	}

	/**
	 * Show the form for creating a new UserRole.
	 * GET|HEAD /userRoles/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created UserRole in storage.
	 * POST /userRoles
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(UserRole::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserRole::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$userRoles = $this->userRoleRepository->create($input);

		return $this->sendResponse($userRoles->toArray(), "UserRole saved successfully");
	}

	/**
	 * Display the specified UserRole.
	 * GET|HEAD /userRoles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userRole = $this->userRoleRepository->apiFindOrFail($id);

		return $this->sendResponse($userRole->toArray(), "UserRole retrieved successfully");
	}

	/**
	 * Show the form for editing the specified UserRole.
	 * GET|HEAD /userRoles/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified UserRole in storage.
	 * PUT/PATCH /userRoles/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(UserRole::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserRole::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var UserRole $userRole */
		$userRole = $this->userRoleRepository->apiFindOrFail($id);

		$result = $this->userRoleRepository->updateRich($input, $id);

		$userRole = $userRole->fresh();

		return $this->sendResponse($userRole->toArray(), "UserRole updated successfully");
	}

	/**
	 * Remove the specified UserRole from storage.
	 * DELETE /userRoles/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->userRoleRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "UserRole deleted successfully");
	}
}
