<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\OperatorTypeRepository;
use App\Models\OperatorType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class OperatorTypeAPIController extends AppBaseController
{
	/** @var  OperatorTypeRepository */
	private $operatorTypeRepository;

	function __construct(OperatorTypeRepository $operatorTypeRepo)
	{
		$this->operatorTypeRepository = $operatorTypeRepo;
	}

	/**
	 * Display a listing of the OperatorType.
	 * GET|HEAD /operatorTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$operatorTypes = OperatorType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($operatorTypes);
	}

	/**
	 * Show the form for creating a new OperatorType.
	 * GET|HEAD /operatorTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created OperatorType in storage.
	 * POST /operatorTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(OperatorType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, OperatorType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$operatorTypes = $this->operatorTypeRepository->create($input);

		return $this->sendResponse($operatorTypes->toArray(), "OperatorType saved successfully");
	}

	/**
	 * Display the specified OperatorType.
	 * GET|HEAD /operatorTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$operatorType = $this->operatorTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($operatorType->toArray(), "OperatorType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified OperatorType.
	 * GET|HEAD /operatorTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified OperatorType in storage.
	 * PUT/PATCH /operatorTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(OperatorType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, OperatorType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var OperatorType $operatorType */
		$operatorType = $this->operatorTypeRepository->apiFindOrFail($id);

		$result = $this->operatorTypeRepository->updateRich($input, $id);

		$operatorType = $operatorType->fresh();

		return $this->sendResponse($operatorType->toArray(), "OperatorType updated successfully");
	}

	/**
	 * Remove the specified OperatorType from storage.
	 * DELETE /operatorTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->operatorTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "OperatorType deleted successfully");
	}
}
