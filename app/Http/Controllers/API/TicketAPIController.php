<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TicketRepository;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TicketAPIController extends AppBaseController
{
	/** @var  TicketRepository */
	private $ticketRepository;

	function __construct(TicketRepository $ticketRepo)
	{
		$this->ticketRepository = $ticketRepo;
	}

	/**
	 * Display a listing of the Ticket.
	 * GET|HEAD /tickets
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$tickets = Ticket::with(['ticketType', 'ticketOperator', 'agent', 'city', 'city', 'country', 'country', 'classsType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($tickets);
	}

	/**
	 * Show the form for creating a new Ticket.
	 * GET|HEAD /tickets/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Ticket in storage.
	 * POST /tickets
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Ticket::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Ticket::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$tickets = $this->ticketRepository->create($input);

		return $this->sendResponse($tickets->toArray(), "Ticket saved successfully");
	}

	/**
	 * Display the specified Ticket.
	 * GET|HEAD /tickets/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticket = $this->ticketRepository->apiFindOrFail($id);

		return $this->sendResponse($ticket->toArray(), "Ticket retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Ticket.
	 * GET|HEAD /tickets/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Ticket in storage.
	 * PUT/PATCH /tickets/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Ticket::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Ticket::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Ticket $ticket */
		$ticket = $this->ticketRepository->apiFindOrFail($id);

		$result = $this->ticketRepository->updateRich($input, $id);

		$ticket = $ticket->fresh();

		return $this->sendResponse($ticket->toArray(), "Ticket updated successfully");
	}

	/**
	 * Remove the specified Ticket from storage.
	 * DELETE /tickets/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ticketRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Ticket deleted successfully");
	}
}
