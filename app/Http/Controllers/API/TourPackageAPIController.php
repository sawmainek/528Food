<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TourPackageRepository;
use App\Models\TourPackage;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TourPackageAPIController extends AppBaseController
{
	/** @var  TourPackageRepository */
	private $tourPackageRepository;

	function __construct(TourPackageRepository $tourPackageRepo)
	{
		$this->tourPackageRepository = $tourPackageRepo;
	}

	/**
	 * Display a listing of the TourPackage.
	 * GET|HEAD /tourPackages
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$tourPackages = TourPackage::with(['agent', 'tourPackageType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($tourPackages);
	}

	/**
	 * Show the form for creating a new TourPackage.
	 * GET|HEAD /tourPackages/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TourPackage in storage.
	 * POST /tourPackages
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TourPackage::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TourPackage::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$tourPackages = $this->tourPackageRepository->create($input);

		return $this->sendResponse($tourPackages->toArray(), "TourPackage saved successfully");
	}

	/**
	 * Display the specified TourPackage.
	 * GET|HEAD /tourPackages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tourPackage = $this->tourPackageRepository->apiFindOrFail($id);

		return $this->sendResponse($tourPackage->toArray(), "TourPackage retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TourPackage.
	 * GET|HEAD /tourPackages/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified TourPackage in storage.
	 * PUT/PATCH /tourPackages/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(TourPackage::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, TourPackage::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var TourPackage $tourPackage */
		$tourPackage = $this->tourPackageRepository->apiFindOrFail($id);

		$result = $this->tourPackageRepository->updateRich($input, $id);

		$tourPackage = $tourPackage->fresh();

		return $this->sendResponse($tourPackage->toArray(), "TourPackage updated successfully");
	}

	/**
	 * Remove the specified TourPackage from storage.
	 * DELETE /tourPackages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tourPackageRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TourPackage deleted successfully");
	}
}
