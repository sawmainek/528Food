<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\CountryRepository;
use App\Models\Country;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class CountryAPIController extends AppBaseController
{
	/** @var  CountryRepository */
	private $countryRepository;

	function __construct(CountryRepository $countryRepo)
	{
		$this->countryRepository = $countryRepo;
	}

	/**
	 * Display a listing of the Country.
	 * GET|HEAD /countries
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$countries = Country::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($countries);
	}

	/**
	 * Show the form for creating a new Country.
	 * GET|HEAD /countries/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Country in storage.
	 * POST /countries
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Country::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Country::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$countries = $this->countryRepository->create($input);

		return $this->sendResponse($countries->toArray(), "Country saved successfully");
	}

	/**
	 * Display the specified Country.
	 * GET|HEAD /countries/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$country = $this->countryRepository->apiFindOrFail($id);

		return $this->sendResponse($country->toArray(), "Country retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Country.
	 * GET|HEAD /countries/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Country in storage.
	 * PUT/PATCH /countries/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Country::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Country::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Country $country */
		$country = $this->countryRepository->apiFindOrFail($id);

		$result = $this->countryRepository->updateRich($input, $id);

		$country = $country->fresh();

		return $this->sendResponse($country->toArray(), "Country updated successfully");
	}

	/**
	 * Remove the specified Country from storage.
	 * DELETE /countries/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->countryRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Country deleted successfully");
	}
}
