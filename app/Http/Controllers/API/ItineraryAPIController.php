<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ItineraryRepository;
use App\Models\Itinerary;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ItineraryAPIController extends AppBaseController
{
	/** @var  ItineraryRepository */
	private $itineraryRepository;

	function __construct(ItineraryRepository $itineraryRepo)
	{
		$this->itineraryRepository = $itineraryRepo;
	}

	/**
	 * Display a listing of the Itinerary.
	 * GET|HEAD /itineraries
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$itineraries = Itinerary::with(['tourPackage'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($itineraries);
	}

	/**
	 * Show the form for creating a new Itinerary.
	 * GET|HEAD /itineraries/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Itinerary in storage.
	 * POST /itineraries
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Itinerary::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Itinerary::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$itineraries = $this->itineraryRepository->create($input);

		return $this->sendResponse($itineraries->toArray(), "Itinerary saved successfully");
	}

	/**
	 * Display the specified Itinerary.
	 * GET|HEAD /itineraries/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$itinerary = $this->itineraryRepository->apiFindOrFail($id);

		return $this->sendResponse($itinerary->toArray(), "Itinerary retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Itinerary.
	 * GET|HEAD /itineraries/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Itinerary in storage.
	 * PUT/PATCH /itineraries/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Itinerary::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Itinerary::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Itinerary $itinerary */
		$itinerary = $this->itineraryRepository->apiFindOrFail($id);

		$result = $this->itineraryRepository->updateRich($input, $id);

		$itinerary = $itinerary->fresh();

		return $this->sendResponse($itinerary->toArray(), "Itinerary updated successfully");
	}

	/**
	 * Remove the specified Itinerary from storage.
	 * DELETE /itineraries/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->itineraryRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Itinerary deleted successfully");
	}
}
