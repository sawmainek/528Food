<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HotelReviewRepository;
use App\Models\HotelReview;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HotelReviewAPIController extends AppBaseController
{
	/** @var  HotelReviewRepository */
	private $hotelReviewRepository;

	function __construct(HotelReviewRepository $hotelReviewRepo)
	{
		$this->hotelReviewRepository = $hotelReviewRepo;
	}

	/**
	 * Display a listing of the HotelReview.
	 * GET|HEAD /hotelReviews
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$hotelReviews = HotelReview::with(['user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($hotelReviews);
	}

	/**
	 * Show the form for creating a new HotelReview.
	 * GET|HEAD /hotelReviews/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created HotelReview in storage.
	 * POST /hotelReviews
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(HotelReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$hotelReviews = $this->hotelReviewRepository->create($input);

		return $this->sendResponse($hotelReviews->toArray(), "HotelReview saved successfully");
	}

	/**
	 * Display the specified HotelReview.
	 * GET|HEAD /hotelReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelReview = $this->hotelReviewRepository->apiFindOrFail($id);

		return $this->sendResponse($hotelReview->toArray(), "HotelReview retrieved successfully");
	}

	/**
	 * Show the form for editing the specified HotelReview.
	 * GET|HEAD /hotelReviews/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified HotelReview in storage.
	 * PUT/PATCH /hotelReviews/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(HotelReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var HotelReview $hotelReview */
		$hotelReview = $this->hotelReviewRepository->apiFindOrFail($id);

		$result = $this->hotelReviewRepository->updateRich($input, $id);

		$hotelReview = $hotelReview->fresh();

		return $this->sendResponse($hotelReview->toArray(), "HotelReview updated successfully");
	}

	/**
	 * Remove the specified HotelReview from storage.
	 * DELETE /hotelReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->hotelReviewRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "HotelReview deleted successfully");
	}
}
