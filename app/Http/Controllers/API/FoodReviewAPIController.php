<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodReviewRepository;
use App\Models\FoodReview;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class FoodReviewAPIController extends AppBaseController
{
	/** @var  FoodReviewRepository */
	private $foodReviewRepository;

	function __construct(FoodReviewRepository $foodReviewRepo)
	{
		$this->foodReviewRepository = $foodReviewRepo;
	}

	/**
	 * Display a listing of the FoodReview.
	 * GET|HEAD /foodReviews
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;
		$food_id = $request->input('food_id') ? $request->input('food_id') : 0; 

		$offset  = ($offset - 1) * $limit;
		
		if($food_id > 0){
			$foodReviews = FoodReview::with(['food', 'user'])->where('food_id', $food_id)->orderBy('id','desc')->get();
		}else{
			$foodReviews = FoodReview::with(['food', 'user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
		}
				
		return response()->json($foodReviews);
	}

	/**
	 * Show the form for creating a new FoodReview.
	 * GET|HEAD /foodReviews/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created FoodReview in storage.
	 * POST /foodReviews
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(FoodReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$foodReviews = $this->foodReviewRepository->create($input);

		return $this->sendResponse($foodReviews->toArray(), "FoodReview saved successfully");
	}

	/**
	 * Display the specified FoodReview.
	 * GET|HEAD /foodReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodReview = $this->foodReviewRepository->apiFindOrFail($id);

		return $this->sendResponse($foodReview->toArray(), "FoodReview retrieved successfully");
	}

	/**
	 * Show the form for editing the specified FoodReview.
	 * GET|HEAD /foodReviews/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified FoodReview in storage.
	 * PUT/PATCH /foodReviews/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(FoodReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var FoodReview $foodReview */
		$foodReview = $this->foodReviewRepository->apiFindOrFail($id);

		$result = $this->foodReviewRepository->updateRich($input, $id);

		$foodReview = $foodReview->fresh();

		return $this->sendResponse($foodReview->toArray(), "FoodReview updated successfully");
	}

	/**
	 * Remove the specified FoodReview from storage.
	 * DELETE /foodReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodReviewRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "FoodReview deleted successfully");
	}
}
