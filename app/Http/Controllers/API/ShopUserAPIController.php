<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ShopUserRepository;
use App\Models\ShopUser;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ShopUserAPIController extends AppBaseController
{
	/** @var  ShopUserRepository */
	private $shopUserRepository;

	function __construct(ShopUserRepository $shopUserRepo)
	{
		$this->shopUserRepository = $shopUserRepo;
	}

	/**
	 * Display a listing of the ShopUser.
	 * GET|HEAD /shopUsers
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$shopUsers = ShopUser::with(['user', 'shop'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($shopUsers);
	}

	/**
	 * Show the form for creating a new ShopUser.
	 * GET|HEAD /shopUsers/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ShopUser in storage.
	 * POST /shopUsers
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ShopUser::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ShopUser::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$shopUsers = $this->shopUserRepository->create($input);

		return $this->sendResponse($shopUsers->toArray(), "ShopUser saved successfully");
	}

	/**
	 * Display the specified ShopUser.
	 * GET|HEAD /shopUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shopUser = $this->shopUserRepository->apiFindOrFail($id);

		return $this->sendResponse($shopUser->toArray(), "ShopUser retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ShopUser.
	 * GET|HEAD /shopUsers/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified ShopUser in storage.
	 * PUT/PATCH /shopUsers/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(ShopUser::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ShopUser::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var ShopUser $shopUser */
		$shopUser = $this->shopUserRepository->apiFindOrFail($id);

		$result = $this->shopUserRepository->updateRich($input, $id);

		$shopUser = $shopUser->fresh();

		return $this->sendResponse($shopUser->toArray(), "ShopUser updated successfully");
	}

	/**
	 * Remove the specified ShopUser from storage.
	 * DELETE /shopUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->shopUserRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ShopUser deleted successfully");
	}
}
