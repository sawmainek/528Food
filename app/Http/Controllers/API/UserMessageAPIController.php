<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\UserMessageRepository;
use App\Models\UserMessage;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class UserMessageAPIController extends AppBaseController
{
	/** @var  UserMessageRepository */
	private $userMessageRepository;

	function __construct(UserMessageRepository $userMessageRepo)
	{
		$this->userMessageRepository = $userMessageRepo;
	}

	/**
	 * Display a listing of the UserMessage.
	 * GET|HEAD /userMessages
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$userMessages = UserMessage::with(['user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($userMessages);
	}

	/**
	 * Show the form for creating a new UserMessage.
	 * GET|HEAD /userMessages/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created UserMessage in storage.
	 * POST /userMessages
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(UserMessage::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserMessage::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$userMessages = $this->userMessageRepository->create($input);

		return $this->sendResponse($userMessages->toArray(), "UserMessage saved successfully");
	}

	/**
	 * Display the specified UserMessage.
	 * GET|HEAD /userMessages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userMessage = $this->userMessageRepository->apiFindOrFail($id);

		return $this->sendResponse($userMessage->toArray(), "UserMessage retrieved successfully");
	}

	/**
	 * Show the form for editing the specified UserMessage.
	 * GET|HEAD /userMessages/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified UserMessage in storage.
	 * PUT/PATCH /userMessages/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(UserMessage::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, UserMessage::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var UserMessage $userMessage */
		$userMessage = $this->userMessageRepository->apiFindOrFail($id);

		$result = $this->userMessageRepository->updateRich($input, $id);

		$userMessage = $userMessage->fresh();

		return $this->sendResponse($userMessage->toArray(), "UserMessage updated successfully");
	}

	/**
	 * Remove the specified UserMessage from storage.
	 * DELETE /userMessages/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->userMessageRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "UserMessage deleted successfully");
	}
}
