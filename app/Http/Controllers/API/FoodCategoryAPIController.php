<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodCategoryRepository;
use App\Models\FoodCategory;
use App\Models\FoodSize;
use App\Models\FoodPhoto;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class FoodCategoryAPIController extends AppBaseController
{
	/** @var  FoodCategoryRepository */
	private $foodCategoryRepository;

	function __construct(FoodCategoryRepository $foodCategoryRepo)
	{
		$this->foodCategoryRepository = $foodCategoryRepo;
	}

	/**
	 * Display a listing of the FoodCategory.
	 * GET|HEAD /foodCategories
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;
		$withFoods = $request->input('withFoods') ? $request->input('withFoods') : false;

		$offset  = ($offset - 1) * $limit;
		
		if(!$withFoods){
			$foodCategories = FoodCategory::orderBy('id','desc')->offset($offset)->limit($limit)->get();
		}else{
			$foodCategories = FoodCategory::with('foods')->orderBy('id','desc')->offset($offset)->limit($limit)->get();
			foreach ($foodCategories as $category_key => $foodCategory) {
				if(count($foodCategory->foods) > 0){
					foreach ($foodCategory->foods as $food_key => $food) {
						$foodSize = FoodSize::where('food_id',$food->id)->get();
						if($foodSize)
							$foodCategories[$category_key]['foods'][$food_key]['size'] = $foodSize;
						$foodPhoto = FoodPhoto::where('food_id',$food->id)->first();
						if($foodPhoto)
							$foodCategories[$category_key]['foods'][$food_key]['image'] = $foodPhoto->image;
					}
				}
				
			}
			
		}
				
		return response()->json($foodCategories);
	}

	/**
	 * Show the form for creating a new FoodCategory.
	 * GET|HEAD /foodCategories/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created FoodCategory in storage.
	 * POST /foodCategories
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(FoodCategory::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodCategory::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$foodCategories = $this->foodCategoryRepository->create($input);

		return $this->sendResponse($foodCategories->toArray(), "FoodCategory saved successfully");
	}

	/**
	 * Display the specified FoodCategory.
	 * GET|HEAD /foodCategories/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodCategory = $this->foodCategoryRepository->apiFindOrFail($id);

		return $this->sendResponse($foodCategory->toArray(), "FoodCategory retrieved successfully");
	}

	/**
	 * Show the form for editing the specified FoodCategory.
	 * GET|HEAD /foodCategories/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified FoodCategory in storage.
	 * PUT/PATCH /foodCategories/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(FoodCategory::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodCategory::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var FoodCategory $foodCategory */
		$foodCategory = $this->foodCategoryRepository->apiFindOrFail($id);

		$result = $this->foodCategoryRepository->updateRich($input, $id);

		$foodCategory = $foodCategory->fresh();

		return $this->sendResponse($foodCategory->toArray(), "FoodCategory updated successfully");
	}

	/**
	 * Remove the specified FoodCategory from storage.
	 * DELETE /foodCategories/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodCategoryRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "FoodCategory deleted successfully");
	}
}
