<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AgentReviewRepository;
use App\Models\AgentReview;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AgentReviewAPIController extends AppBaseController
{
	/** @var  AgentReviewRepository */
	private $agentReviewRepository;

	function __construct(AgentReviewRepository $agentReviewRepo)
	{
		$this->agentReviewRepository = $agentReviewRepo;
	}

	/**
	 * Display a listing of the AgentReview.
	 * GET|HEAD /agentReviews
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$agentReviews = AgentReview::with(['user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($agentReviews);
	}

	/**
	 * Show the form for creating a new AgentReview.
	 * GET|HEAD /agentReviews/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created AgentReview in storage.
	 * POST /agentReviews
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(AgentReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, AgentReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$agentReviews = $this->agentReviewRepository->create($input);

		return $this->sendResponse($agentReviews->toArray(), "AgentReview saved successfully");
	}

	/**
	 * Display the specified AgentReview.
	 * GET|HEAD /agentReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$agentReview = $this->agentReviewRepository->apiFindOrFail($id);

		return $this->sendResponse($agentReview->toArray(), "AgentReview retrieved successfully");
	}

	/**
	 * Show the form for editing the specified AgentReview.
	 * GET|HEAD /agentReviews/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified AgentReview in storage.
	 * PUT/PATCH /agentReviews/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(AgentReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, AgentReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var AgentReview $agentReview */
		$agentReview = $this->agentReviewRepository->apiFindOrFail($id);

		$result = $this->agentReviewRepository->updateRich($input, $id);

		$agentReview = $agentReview->fresh();

		return $this->sendResponse($agentReview->toArray(), "AgentReview updated successfully");
	}

	/**
	 * Remove the specified AgentReview from storage.
	 * DELETE /agentReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->agentReviewRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "AgentReview deleted successfully");
	}
}
