<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\RoomTypeRepository;
use App\Models\RoomType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class RoomTypeAPIController extends AppBaseController
{
	/** @var  RoomTypeRepository */
	private $roomTypeRepository;

	function __construct(RoomTypeRepository $roomTypeRepo)
	{
		$this->roomTypeRepository = $roomTypeRepo;
	}

	/**
	 * Display a listing of the RoomType.
	 * GET|HEAD /roomTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$roomTypes = RoomType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($roomTypes);
	}

	/**
	 * Show the form for creating a new RoomType.
	 * GET|HEAD /roomTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created RoomType in storage.
	 * POST /roomTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(RoomType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, RoomType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$roomTypes = $this->roomTypeRepository->create($input);

		return $this->sendResponse($roomTypes->toArray(), "RoomType saved successfully");
	}

	/**
	 * Display the specified RoomType.
	 * GET|HEAD /roomTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roomType = $this->roomTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($roomType->toArray(), "RoomType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified RoomType.
	 * GET|HEAD /roomTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified RoomType in storage.
	 * PUT/PATCH /roomTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(RoomType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, RoomType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var RoomType $roomType */
		$roomType = $this->roomTypeRepository->apiFindOrFail($id);

		$result = $this->roomTypeRepository->updateRich($input, $id);

		$roomType = $roomType->fresh();

		return $this->sendResponse($roomType->toArray(), "RoomType updated successfully");
	}

	/**
	 * Remove the specified RoomType from storage.
	 * DELETE /roomTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->roomTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "RoomType deleted successfully");
	}
}
