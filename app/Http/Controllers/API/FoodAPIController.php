<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodRepository;
use App\Models\Food;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class FoodAPIController extends AppBaseController
{
	/** @var  FoodRepository */
	private $foodRepository;

	function __construct(FoodRepository $foodRepo)
	{
		$this->foodRepository = $foodRepo;
	}

	/**
	 * Display a listing of the Food.
	 * GET|HEAD /foods
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;
		$category_id = $request->input('category_id') ? $request->input('category_id') : 0;

		$offset  = ($offset - 1) * $limit;

		if($category_id > 0){
			$foods = Food::with(['foodCategory', 'shop','foodPhotos','foodSizes','foodReviews'])->where('category_id',$category_id)->orderBy('id','desc')->offset($offset)->limit($limit)->get();
		}else{
			$foods = Food::with(['foodCategory', 'shop','foodPhotos','foodSizes','foodReviews'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
		}
				
		return response()->json($foods);
	}

	/**
	 * Show the form for creating a new Food.
	 * GET|HEAD /foods/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Food in storage.
	 * POST /foods
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Food::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Food::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$foods = $this->foodRepository->create($input);

		return $this->sendResponse($foods->toArray(), "Food saved successfully");
	}

	/**
	 * Display the specified Food.
	 * GET|HEAD /foods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$food = $this->foodRepository->apiFindOrFail($id);

		return $this->sendResponse($food->toArray(), "Food retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Food.
	 * GET|HEAD /foods/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Food in storage.
	 * PUT/PATCH /foods/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Food::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Food::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Food $food */
		$food = $this->foodRepository->apiFindOrFail($id);

		$result = $this->foodRepository->updateRich($input, $id);

		$food = $food->fresh();

		return $this->sendResponse($food->toArray(), "Food updated successfully");
	}

	/**
	 * Remove the specified Food from storage.
	 * DELETE /foods/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Food deleted successfully");
	}
}
