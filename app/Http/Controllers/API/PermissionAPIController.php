<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PermissionRepository;
use App\Models\Permission;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PermissionAPIController extends AppBaseController
{
	/** @var  PermissionRepository */
	private $permissionRepository;

	function __construct(PermissionRepository $permissionRepo)
	{
		$this->permissionRepository = $permissionRepo;
	}

	/**
	 * Display a listing of the Permission.
	 * GET|HEAD /permissions
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$permissions = Permission::with(['role'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($permissions);
	}

	/**
	 * Show the form for creating a new Permission.
	 * GET|HEAD /permissions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Permission in storage.
	 * POST /permissions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Permission::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Permission::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$permissions = $this->permissionRepository->create($input);

		return $this->sendResponse($permissions->toArray(), "Permission saved successfully");
	}

	/**
	 * Display the specified Permission.
	 * GET|HEAD /permissions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$permission = $this->permissionRepository->apiFindOrFail($id);

		return $this->sendResponse($permission->toArray(), "Permission retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Permission.
	 * GET|HEAD /permissions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Permission in storage.
	 * PUT/PATCH /permissions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Permission::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Permission::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Permission $permission */
		$permission = $this->permissionRepository->apiFindOrFail($id);

		$result = $this->permissionRepository->updateRich($input, $id);

		$permission = $permission->fresh();

		return $this->sendResponse($permission->toArray(), "Permission updated successfully");
	}

	/**
	 * Remove the specified Permission from storage.
	 * DELETE /permissions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->permissionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Permission deleted successfully");
	}
}
