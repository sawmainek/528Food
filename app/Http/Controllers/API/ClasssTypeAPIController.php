<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ClasssTypeRepository;
use App\Models\ClasssType;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ClasssTypeAPIController extends AppBaseController
{
	/** @var  ClasssTypeRepository */
	private $classsTypeRepository;

	function __construct(ClasssTypeRepository $classsTypeRepo)
	{
		$this->classsTypeRepository = $classsTypeRepo;
	}

	/**
	 * Display a listing of the ClasssType.
	 * GET|HEAD /classsTypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$classsTypes = ClasssType::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($classsTypes);
	}

	/**
	 * Show the form for creating a new ClasssType.
	 * GET|HEAD /classsTypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ClasssType in storage.
	 * POST /classsTypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ClasssType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ClasssType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$classsTypes = $this->classsTypeRepository->create($input);

		return $this->sendResponse($classsTypes->toArray(), "ClasssType saved successfully");
	}

	/**
	 * Display the specified ClasssType.
	 * GET|HEAD /classsTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$classsType = $this->classsTypeRepository->apiFindOrFail($id);

		return $this->sendResponse($classsType->toArray(), "ClasssType retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ClasssType.
	 * GET|HEAD /classsTypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified ClasssType in storage.
	 * PUT/PATCH /classsTypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(ClasssType::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ClasssType::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var ClasssType $classsType */
		$classsType = $this->classsTypeRepository->apiFindOrFail($id);

		$result = $this->classsTypeRepository->updateRich($input, $id);

		$classsType = $classsType->fresh();

		return $this->sendResponse($classsType->toArray(), "ClasssType updated successfully");
	}

	/**
	 * Remove the specified ClasssType from storage.
	 * DELETE /classsTypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->classsTypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ClasssType deleted successfully");
	}
}
