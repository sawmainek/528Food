<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\FoodOrderRepository;
use App\Models\FoodOrder;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use DB;

class FoodOrderAPIController extends AppBaseController
{
	/** @var  FoodOrderRepository */
	private $foodOrderRepository;

	function __construct(FoodOrderRepository $foodOrderRepo)
	{
		$this->foodOrderRepository = $foodOrderRepo;
	}

	/**
	 * Display a listing of the FoodOrder.
	 * GET|HEAD /foodOrders
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$foodOrders = FoodOrder::with(['user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($foodOrders);
	}

	/**
	 * Show the form for creating a new FoodOrder.
	 * GET|HEAD /foodOrders/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created FoodOrder in storage.
	 * POST /foodOrders
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(FoodOrder::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodOrder::$api_rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$input['order_date'] = date('Y-m-d H:i:s');
		$input['paid'] = false;

		DB::beginTransaction();

		try{
			$foodOrders = $this->foodOrderRepository->create($input);

			if($foodOrders){
				$order_menus = json_decode($input['order_menus']);
				if($order_menus){
					$order_details = [];
					$total_amount = 0;
					foreach ($order_menus->Menus as $key => $value) {
						$order_detail = new OrderDetail;
						$order_detail->order_id = $foodOrders->id;
						$order_detail->food_id = $value->id;
						$order_detail->size_id = $value->selectedSize->id;
						$order_detail->qty = $value->selectedQty;
						$order_detail->price = $value->selectedSize->price;
						$order_detail->note = $value->selectedNote;
						$order_detail->sub_total = $order_detail->price * $order_detail->qty;
						$order_detail->save();

						$total_amount += $order_detail->sub_total;
						$order_details[] = $order_detail;
					}
					$foodOrders->total_amount = $total_amount;
					$foodOrders->update();
					$foodOrders->order_detail = $order_details;

					DB::commit();
				}else{
					DB::rollBack();
					return response()->json('Invalid order menus!', 400);
				}
			}
		}catch (Exception $e) {
            DB::rollBack();
            return response()->json($e, 400);
        }      

		return $this->sendResponse($foodOrders->toArray(), "FoodOrder saved successfully");
	}

	/**
	 * Display the specified FoodOrder.
	 * GET|HEAD /foodOrders/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$foodOrder = $this->foodOrderRepository->apiFindOrFail($id);

		return $this->sendResponse($foodOrder->toArray(), "FoodOrder retrieved successfully");
	}

	/**
	 * Show the form for editing the specified FoodOrder.
	 * GET|HEAD /foodOrders/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified FoodOrder in storage.
	 * PUT/PATCH /foodOrders/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(FoodOrder::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, FoodOrder::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var FoodOrder $foodOrder */
		$foodOrder = $this->foodOrderRepository->apiFindOrFail($id);

		$result = $this->foodOrderRepository->updateRich($input, $id);

		$foodOrder = $foodOrder->fresh();

		return $this->sendResponse($foodOrder->toArray(), "FoodOrder updated successfully");
	}

	/**
	 * Remove the specified FoodOrder from storage.
	 * DELETE /foodOrders/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->foodOrderRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "FoodOrder deleted successfully");
	}
}
