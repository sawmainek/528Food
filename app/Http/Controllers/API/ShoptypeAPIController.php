<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ShoptypeRepository;
use App\Models\Shoptype;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ShoptypeAPIController extends AppBaseController
{
	/** @var  ShoptypeRepository */
	private $shoptypeRepository;

	function __construct(ShoptypeRepository $shoptypeRepo)
	{
		$this->shoptypeRepository = $shoptypeRepo;
	}

	/**
	 * Display a listing of the Shoptype.
	 * GET|HEAD /shoptypes
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$shoptypes = Shoptype::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($shoptypes);
	}

	/**
	 * Show the form for creating a new Shoptype.
	 * GET|HEAD /shoptypes/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Shoptype in storage.
	 * POST /shoptypes
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Shoptype::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Shoptype::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$shoptypes = $this->shoptypeRepository->create($input);

		return $this->sendResponse($shoptypes->toArray(), "Shoptype saved successfully");
	}

	/**
	 * Display the specified Shoptype.
	 * GET|HEAD /shoptypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shoptype = $this->shoptypeRepository->apiFindOrFail($id);

		return $this->sendResponse($shoptype->toArray(), "Shoptype retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Shoptype.
	 * GET|HEAD /shoptypes/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Shoptype in storage.
	 * PUT/PATCH /shoptypes/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Shoptype::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Shoptype::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Shoptype $shoptype */
		$shoptype = $this->shoptypeRepository->apiFindOrFail($id);

		$result = $this->shoptypeRepository->updateRich($input, $id);

		$shoptype = $shoptype->fresh();

		return $this->sendResponse($shoptype->toArray(), "Shoptype updated successfully");
	}

	/**
	 * Remove the specified Shoptype from storage.
	 * DELETE /shoptypes/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->shoptypeRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Shoptype deleted successfully");
	}
}
