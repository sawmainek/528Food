<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AgentUserRepository;
use App\Models\AgentUser;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AgentUserAPIController extends AppBaseController
{
	/** @var  AgentUserRepository */
	private $agentUserRepository;

	function __construct(AgentUserRepository $agentUserRepo)
	{
		$this->agentUserRepository = $agentUserRepo;
	}

	/**
	 * Display a listing of the AgentUser.
	 * GET|HEAD /agentUsers
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$agentUsers = AgentUser::with(['agent', 'user'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($agentUsers);
	}

	/**
	 * Show the form for creating a new AgentUser.
	 * GET|HEAD /agentUsers/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created AgentUser in storage.
	 * POST /agentUsers
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(AgentUser::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, AgentUser::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$agentUsers = $this->agentUserRepository->create($input);

		return $this->sendResponse($agentUsers->toArray(), "AgentUser saved successfully");
	}

	/**
	 * Display the specified AgentUser.
	 * GET|HEAD /agentUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$agentUser = $this->agentUserRepository->apiFindOrFail($id);

		return $this->sendResponse($agentUser->toArray(), "AgentUser retrieved successfully");
	}

	/**
	 * Show the form for editing the specified AgentUser.
	 * GET|HEAD /agentUsers/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified AgentUser in storage.
	 * PUT/PATCH /agentUsers/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(AgentUser::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, AgentUser::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var AgentUser $agentUser */
		$agentUser = $this->agentUserRepository->apiFindOrFail($id);

		$result = $this->agentUserRepository->updateRich($input, $id);

		$agentUser = $agentUser->fresh();

		return $this->sendResponse($agentUser->toArray(), "AgentUser updated successfully");
	}

	/**
	 * Remove the specified AgentUser from storage.
	 * DELETE /agentUsers/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->agentUserRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "AgentUser deleted successfully");
	}
}
