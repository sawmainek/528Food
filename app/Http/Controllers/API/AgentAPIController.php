<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\AgentRepository;
use App\Models\Agent;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class AgentAPIController extends AppBaseController
{
	/** @var  AgentRepository */
	private $agentRepository;

	function __construct(AgentRepository $agentRepo)
	{
		$this->agentRepository = $agentRepo;
	}

	/**
	 * Display a listing of the Agent.
	 * GET|HEAD /agents
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$agents = Agent::orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($agents);
	}

	/**
	 * Show the form for creating a new Agent.
	 * GET|HEAD /agents/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Agent in storage.
	 * POST /agents
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Agent::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Agent::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$agents = $this->agentRepository->create($input);

		return $this->sendResponse($agents->toArray(), "Agent saved successfully");
	}

	/**
	 * Display the specified Agent.
	 * GET|HEAD /agents/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$agent = $this->agentRepository->apiFindOrFail($id);

		return $this->sendResponse($agent->toArray(), "Agent retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Agent.
	 * GET|HEAD /agents/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Agent in storage.
	 * PUT/PATCH /agents/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Agent::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Agent::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Agent $agent */
		$agent = $this->agentRepository->apiFindOrFail($id);

		$result = $this->agentRepository->updateRich($input, $id);

		$agent = $agent->fresh();

		return $this->sendResponse($agent->toArray(), "Agent updated successfully");
	}

	/**
	 * Remove the specified Agent from storage.
	 * DELETE /agents/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->agentRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Agent deleted successfully");
	}
}
