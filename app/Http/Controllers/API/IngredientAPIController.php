<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\IngredientRepository;
use App\Models\Ingredient;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class IngredientAPIController extends AppBaseController
{
	/** @var  IngredientRepository */
	private $ingredientRepository;

	function __construct(IngredientRepository $ingredientRepo)
	{
		$this->ingredientRepository = $ingredientRepo;
	}

	/**
	 * Display a listing of the Ingredient.
	 * GET|HEAD /ingredients
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;
		$food_id = $request->input('food_id') ? $request->input('food_id') : 0; 

		$offset  = ($offset - 1) * $limit;
		
		if($food_id > 0){
			$ingredients = Ingredient::with(['food'])->where('food_id',$food_id)->orderBy('id','desc')->get();
		}else{
			$ingredients = Ingredient::with(['food'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
		}
				
		return response()->json($ingredients);
	}

	/**
	 * Show the form for creating a new Ingredient.
	 * GET|HEAD /ingredients/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Ingredient in storage.
	 * POST /ingredients
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Ingredient::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Ingredient::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$ingredients = $this->ingredientRepository->create($input);

		return $this->sendResponse($ingredients->toArray(), "Ingredient saved successfully");
	}

	/**
	 * Display the specified Ingredient.
	 * GET|HEAD /ingredients/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ingredient = $this->ingredientRepository->apiFindOrFail($id);

		return $this->sendResponse($ingredient->toArray(), "Ingredient retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Ingredient.
	 * GET|HEAD /ingredients/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Ingredient in storage.
	 * PUT/PATCH /ingredients/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Ingredient::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Ingredient::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Ingredient $ingredient */
		$ingredient = $this->ingredientRepository->apiFindOrFail($id);

		$result = $this->ingredientRepository->updateRich($input, $id);

		$ingredient = $ingredient->fresh();

		return $this->sendResponse($ingredient->toArray(), "Ingredient updated successfully");
	}

	/**
	 * Remove the specified Ingredient from storage.
	 * DELETE /ingredients/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ingredientRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Ingredient deleted successfully");
	}
}
