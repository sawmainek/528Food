<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HotelPhotoRepository;
use App\Models\HotelPhoto;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HotelPhotoAPIController extends AppBaseController
{
	/** @var  HotelPhotoRepository */
	private $hotelPhotoRepository;

	function __construct(HotelPhotoRepository $hotelPhotoRepo)
	{
		$this->hotelPhotoRepository = $hotelPhotoRepo;
	}

	/**
	 * Display a listing of the HotelPhoto.
	 * GET|HEAD /hotelPhotos
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$hotelPhotos = HotelPhoto::with(['hotel'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($hotelPhotos);
	}

	/**
	 * Show the form for creating a new HotelPhoto.
	 * GET|HEAD /hotelPhotos/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created HotelPhoto in storage.
	 * POST /hotelPhotos
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(HotelPhoto::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelPhoto::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$hotelPhotos = $this->hotelPhotoRepository->create($input);

		return $this->sendResponse($hotelPhotos->toArray(), "HotelPhoto saved successfully");
	}

	/**
	 * Display the specified HotelPhoto.
	 * GET|HEAD /hotelPhotos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotelPhoto = $this->hotelPhotoRepository->apiFindOrFail($id);

		return $this->sendResponse($hotelPhoto->toArray(), "HotelPhoto retrieved successfully");
	}

	/**
	 * Show the form for editing the specified HotelPhoto.
	 * GET|HEAD /hotelPhotos/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified HotelPhoto in storage.
	 * PUT/PATCH /hotelPhotos/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(HotelPhoto::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, HotelPhoto::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var HotelPhoto $hotelPhoto */
		$hotelPhoto = $this->hotelPhotoRepository->apiFindOrFail($id);

		$result = $this->hotelPhotoRepository->updateRich($input, $id);

		$hotelPhoto = $hotelPhoto->fresh();

		return $this->sendResponse($hotelPhoto->toArray(), "HotelPhoto updated successfully");
	}

	/**
	 * Remove the specified HotelPhoto from storage.
	 * DELETE /hotelPhotos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->hotelPhotoRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "HotelPhoto deleted successfully");
	}
}
