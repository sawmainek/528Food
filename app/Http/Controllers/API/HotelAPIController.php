<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\HotelRepository;
use App\Models\Hotel;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class HotelAPIController extends AppBaseController
{
	/** @var  HotelRepository */
	private $hotelRepository;

	function __construct(HotelRepository $hotelRepo)
	{
		$this->hotelRepository = $hotelRepo;
	}

	/**
	 * Display a listing of the Hotel.
	 * GET|HEAD /hotels
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$hotels = Hotel::with(['agent', 'hotelType'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($hotels);
	}

	/**
	 * Show the form for creating a new Hotel.
	 * GET|HEAD /hotels/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Hotel in storage.
	 * POST /hotels
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Hotel::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Hotel::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$hotels = $this->hotelRepository->create($input);

		return $this->sendResponse($hotels->toArray(), "Hotel saved successfully");
	}

	/**
	 * Display the specified Hotel.
	 * GET|HEAD /hotels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$hotel = $this->hotelRepository->apiFindOrFail($id);

		return $this->sendResponse($hotel->toArray(), "Hotel retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Hotel.
	 * GET|HEAD /hotels/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Hotel in storage.
	 * PUT/PATCH /hotels/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(Hotel::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, Hotel::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var Hotel $hotel */
		$hotel = $this->hotelRepository->apiFindOrFail($id);

		$result = $this->hotelRepository->updateRich($input, $id);

		$hotel = $hotel->fresh();

		return $this->sendResponse($hotel->toArray(), "Hotel updated successfully");
	}

	/**
	 * Remove the specified Hotel from storage.
	 * DELETE /hotels/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->hotelRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Hotel deleted successfully");
	}
}
