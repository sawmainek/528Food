<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ShopReviewRepository;
use App\Models\ShopReview;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ShopReviewAPIController extends AppBaseController
{
	/** @var  ShopReviewRepository */
	private $shopReviewRepository;

	function __construct(ShopReviewRepository $shopReviewRepo)
	{
		$this->shopReviewRepository = $shopReviewRepo;
	}

	/**
	 * Display a listing of the ShopReview.
	 * GET|HEAD /shopReviews
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{

		$offset  = $request->input('offset') ? $request->input('offset') : 1;
		$limit   = $request->input('limit') ? $request->input('limit') : 12;

		$offset  = ($offset - 1) * $limit;
		
		$shopReviews = ShopReview::with(['user', 'shop'])->orderBy('id','desc')->offset($offset)->limit($limit)->get();
				
		return response()->json($shopReviews);
	}

	/**
	 * Show the form for creating a new ShopReview.
	 * GET|HEAD /shopReviews/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created ShopReview in storage.
	 * POST /shopReviews
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(ShopReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ShopReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		$shopReviews = $this->shopReviewRepository->create($input);

		return $this->sendResponse($shopReviews->toArray(), "ShopReview saved successfully");
	}

	/**
	 * Display the specified ShopReview.
	 * GET|HEAD /shopReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$shopReview = $this->shopReviewRepository->apiFindOrFail($id);

		return $this->sendResponse($shopReview->toArray(), "ShopReview retrieved successfully");
	}

	/**
	 * Show the form for editing the specified ShopReview.
	 * GET|HEAD /shopReviews/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified ShopReview in storage.
	 * PUT/PATCH /shopReviews/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if(sizeof(ShopReview::$rules) > 0){
			$validator =  $this->validateRequestOrFail($request, ShopReview::$rules);
			if($validator){
				return $validator;
			}
		}

		$input = $request->all();

		/** @var ShopReview $shopReview */
		$shopReview = $this->shopReviewRepository->apiFindOrFail($id);

		$result = $this->shopReviewRepository->updateRich($input, $id);

		$shopReview = $shopReview->fresh();

		return $this->sendResponse($shopReview->toArray(), "ShopReview updated successfully");
	}

	/**
	 * Remove the specified ShopReview from storage.
	 * DELETE /shopReviews/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->shopReviewRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "ShopReview deleted successfully");
	}
}
