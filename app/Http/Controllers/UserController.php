<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Libraries\Repositories\UserRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\User;

class UserController extends AppBaseController
{

	/** @var  UserRepository */
	private $userRepository;

	function __construct(UserRepository $userRepo)
	{
		$this->userRepository = $userRepo;
	}

	/**
	 * Display a listing of the User.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->userRepository->paginate(10);

		return view('users.index')
			->with('users', $users);
	}

	/**
	 * Show the form for creating a new User.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('users.create');
	}

	/**
	 * Store a newly created User in storage.
	 *
	 * @param CreateUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $request)
	{
		$input = $request->all();

		$input['password'] = bcrypt($input['password']);
		if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/users/');
			$input['image'] = $image['name'];
		}

		$user = $this->userRepository->create($input);

		Flash::success('User saved successfully.');

		return redirect(route('administration.users.index'));
	}

	/**
	 * Display the specified User.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->userRepository->find($id);

		if(empty($user))
		{
			Flash::error('User not found');

			return redirect(route('administration.users.index'));
		}

		return view('users.show')->with('user', $user);
	}

	/**
	 * Show the form for editing the specified User.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->userRepository->find($id);

		if(empty($user))
		{
			Flash::error('User not found');

			return redirect(route('administration.users.index'));
		}

		return view('users.edit')->with('user', $user);
	}

	/**
	 * Update the specified User in storage.
	 *
	 * @param  int              $id
	 * @param UpdateUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserRequest $request)
	{
		$user = $this->userRepository->find($id);

		if(empty($user))
		{
			Flash::error('User not found');

			return redirect(route('administration.users.index'));
		}

		$input = $request->all();
		$input['password'] = bcrypt($input['password']);

		if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/users/');
			$input['image'] = $image['name'];
			@unlink(public_path('/users/'.$user->image));
			@unlink(public_path('/users/x100/'.$user->image));
			@unlink(public_path('/users/x200/'.$user->image));
			@unlink(public_path('/users/x400/'.$user->image));

		}
		
		$this->userRepository->updateRich($input, $id);

		Flash::success('User updated successfully.');

		return redirect(route('administration.users.index'));
	}

	/**
	 * Remove the specified User from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = $this->userRepository->find($id);

		if(empty($user))
		{
			Flash::error('User not found');

			return redirect(route('administration.users.index'));
		}

				@unlink(public_path('/users/'.$user->image));
		@unlink(public_path('/users/x100/'.$user->image));
		@unlink(public_path('/users/x200/'.$user->image));
		@unlink(public_path('/users/x400/'.$user->image));


		$this->userRepository->delete($id);

		Flash::success('User deleted successfully.');

		return redirect(route('administration.users.index'));
	}
}
