<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateItineraryRequest;
use App\Http\Requests\UpdateItineraryRequest;
use App\Libraries\Repositories\ItineraryRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Itinerary;
use App\Models\TourPackage;

class ItineraryController extends AppBaseController
{

	/** @var  ItineraryRepository */
	private $itineraryRepository;

	function __construct(ItineraryRepository $itineraryRepo)
	{
		$this->itineraryRepository = $itineraryRepo;
	}

	/**
	 * Display a listing of the Itinerary.
	 *
	 * @return Response
	 */
	public function index()
	{
		$itineraries = Itinerary::with(['tourPackage'])->paginate(10);

		return view('itineraries.index')
			->with('itineraries', $itineraries);
	}

	/**
	 * Show the form for creating a new Itinerary.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$tourPackage=TourPackage::all();
		$tourPackages = [];
		foreach ($tourPackage as $key => $value) {
			$tourPackages[$value->id] = $value->name;
		}
		return view('itineraries.create')->with(['tourPackages'=>$tourPackages]);
	}

	/**
	 * Store a newly created Itinerary in storage.
	 *
	 * @param CreateItineraryRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateItineraryRequest $request)
	{
		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/itineraries/');
			$input['image'] = $image['name'];
		}

		$itinerary = $this->itineraryRepository->create($input);

		Flash::success('Itinerary saved successfully.');

		return redirect(route('administration.itineraries.index'));
	}

	/**
	 * Display the specified Itinerary.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$itinerary = Itinerary::with(['tourPackage'])->where('id', $id)->first();

		if(empty($itinerary))
		{
			Flash::error('Itinerary not found');

			return redirect(route('administration.itineraries.index'));
		}

		return view('itineraries.show')->with('itinerary', $itinerary);
	}

	/**
	 * Show the form for editing the specified Itinerary.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$tourPackage=TourPackage::all();
		$tourPackages = [];
		foreach ($tourPackage as $key => $value) {
			$tourPackages[$value->id] = $value->name;
		}

		$itinerary = $this->itineraryRepository->find($id);

		if(empty($itinerary))
		{
			Flash::error('Itinerary not found');

			return redirect(route('administration.itineraries.index'));
		}

		return view('itineraries.edit')->with(['tourPackages'=>$tourPackages, 'itinerary'=>$itinerary]);
	}

	/**
	 * Update the specified Itinerary in storage.
	 *
	 * @param  int              $id
	 * @param UpdateItineraryRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateItineraryRequest $request)
	{
		$itinerary = $this->itineraryRepository->find($id);

		if(empty($itinerary))
		{
			Flash::error('Itinerary not found');

			return redirect(route('administration.itineraries.index'));
		}

		$input = $request->all();

				if($request->file('image')){
			$image = $this->uploadImage($request->file('image'),'/itineraries/');
			$input['image'] = $image['name'];
					@unlink(public_path('/itineraries/'.$itinerary->image));
		@unlink(public_path('/itineraries/x100/'.$itinerary->image));
		@unlink(public_path('/itineraries/x200/'.$itinerary->image));
		@unlink(public_path('/itineraries/x400/'.$itinerary->image));

		}

		$this->itineraryRepository->updateRich($input, $id);

		Flash::success('Itinerary updated successfully.');

		return redirect(route('administration.itineraries.index'));
	}

	/**
	 * Remove the specified Itinerary from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$itinerary = $this->itineraryRepository->find($id);

		if(empty($itinerary))
		{
			Flash::error('Itinerary not found');

			return redirect(route('administration.itineraries.index'));
		}

				@unlink(public_path('/itineraries/'.$itinerary->image));
		@unlink(public_path('/itineraries/x100/'.$itinerary->image));
		@unlink(public_path('/itineraries/x200/'.$itinerary->image));
		@unlink(public_path('/itineraries/x400/'.$itinerary->image));


		$this->itineraryRepository->delete($id);

		Flash::success('Itinerary deleted successfully.');

		return redirect(route('administration.itineraries.index'));
	}
}
