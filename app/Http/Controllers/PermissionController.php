<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Libraries\Repositories\PermissionRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Permission;
use App\Models\Role;

class PermissionController extends AppBaseController
{

	/** @var  PermissionRepository */
	private $permissionRepository;

	function __construct(PermissionRepository $permissionRepo)
	{
		$this->permissionRepository = $permissionRepo;
	}

	/**
	 * Display a listing of the Permission.
	 *
	 * @return Response
	 */
	public function index()
	{
		$permissions = Permission::with(['role'])->paginate(10);

		return view('permissions.index')
			->with('permissions', $permissions);
	}

	/**
	 * Show the form for creating a new Permission.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$role=Role::all();
		$roles = [];
		foreach ($role as $key => $value) {
			$roles[$value->id] = $value->name;
		}
		return view('permissions.create')->with(['roles'=>$roles]);
	}

	/**
	 * Store a newly created Permission in storage.
	 *
	 * @param CreatePermissionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePermissionRequest $request)
	{
		$input = $request->all();

		

		$permission = $this->permissionRepository->create($input);

		Flash::success('Permission saved successfully.');

		return redirect(route('administration.permissions.index'));
	}

	/**
	 * Display the specified Permission.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$permission = Permission::with(['role'])->where('id', $id)->first();

		if(empty($permission))
		{
			Flash::error('Permission not found');

			return redirect(route('administration.permissions.index'));
		}

		return view('permissions.show')->with('permission', $permission);
	}

	/**
	 * Show the form for editing the specified Permission.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$role=Role::all();
		$roles = [];
		foreach ($role as $key => $value) {
			$roles[$value->id] = $value->name;
		}

		$permission = $this->permissionRepository->find($id);

		if(empty($permission))
		{
			Flash::error('Permission not found');

			return redirect(route('administration.permissions.index'));
		}

		return view('permissions.edit')->with(['roles'=>$roles, 'permission'=>$permission]);
	}

	/**
	 * Update the specified Permission in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePermissionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePermissionRequest $request)
	{
		$permission = $this->permissionRepository->find($id);

		if(empty($permission))
		{
			Flash::error('Permission not found');

			return redirect(route('administration.permissions.index'));
		}

		$input = $request->all();

		

		$this->permissionRepository->updateRich($input, $id);

		Flash::success('Permission updated successfully.');

		return redirect(route('administration.permissions.index'));
	}

	/**
	 * Remove the specified Permission from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$permission = $this->permissionRepository->find($id);

		if(empty($permission))
		{
			Flash::error('Permission not found');

			return redirect(route('administration.permissions.index'));
		}

		

		$this->permissionRepository->delete($id);

		Flash::success('Permission deleted successfully.');

		return redirect(route('administration.permissions.index'));
	}
}
