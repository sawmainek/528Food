<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTicketOperatorRequest;
use App\Http\Requests\UpdateTicketOperatorRequest;
use App\Libraries\Repositories\TicketOperatorRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\TicketOperator;
use App\Models\Agent;
use App\Models\OperatorType;

class TicketOperatorController extends AppBaseController
{

	/** @var  TicketOperatorRepository */
	private $ticketOperatorRepository;

	function __construct(TicketOperatorRepository $ticketOperatorRepo)
	{
		$this->ticketOperatorRepository = $ticketOperatorRepo;
	}

	/**
	 * Display a listing of the TicketOperator.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ticketOperators = TicketOperator::with(['agent', 'operatorType'])->paginate(10);

		return view('ticketOperators.index')
			->with('ticketOperators', $ticketOperators);
	}

	/**
	 * Show the form for creating a new TicketOperator.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->id;
		}

		$operatorType=OperatorType::all();
		$operatorTypes = [];
		foreach ($operatorType as $key => $value) {
			$operatorTypes[$value->id] = $value->name;
		}
		return view('ticketOperators.create')->with(['agents'=>$agents, 'operatorTypes'=>$operatorTypes]);
	}

	/**
	 * Store a newly created TicketOperator in storage.
	 *
	 * @param CreateTicketOperatorRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTicketOperatorRequest $request)
	{
		$input = $request->all();

		

		$ticketOperator = $this->ticketOperatorRepository->create($input);

		Flash::success('TicketOperator saved successfully.');

		return redirect(route('administration.ticketOperators.index'));
	}

	/**
	 * Display the specified TicketOperator.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ticketOperator = TicketOperator::with(['agent', 'operatorType'])->where('id', $id)->first();

		if(empty($ticketOperator))
		{
			Flash::error('TicketOperator not found');

			return redirect(route('administration.ticketOperators.index'));
		}

		return view('ticketOperators.show')->with('ticketOperator', $ticketOperator);
	}

	/**
	 * Show the form for editing the specified TicketOperator.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$agent=Agent::all();
		$agents = [];
		foreach ($agent as $key => $value) {
			$agents[$value->id] = $value->id;
		}

		$operatorType=OperatorType::all();
		$operatorTypes = [];
		foreach ($operatorType as $key => $value) {
			$operatorTypes[$value->id] = $value->name;
		}

		$ticketOperator = $this->ticketOperatorRepository->find($id);

		if(empty($ticketOperator))
		{
			Flash::error('TicketOperator not found');

			return redirect(route('administration.ticketOperators.index'));
		}

		return view('ticketOperators.edit')->with(['agents'=>$agents, 'operatorTypes'=>$operatorTypes, 'ticketOperator'=>$ticketOperator]);
	}

	/**
	 * Update the specified TicketOperator in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTicketOperatorRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTicketOperatorRequest $request)
	{
		$ticketOperator = $this->ticketOperatorRepository->find($id);

		if(empty($ticketOperator))
		{
			Flash::error('TicketOperator not found');

			return redirect(route('administration.ticketOperators.index'));
		}

		$input = $request->all();

		

		$this->ticketOperatorRepository->updateRich($input, $id);

		Flash::success('TicketOperator updated successfully.');

		return redirect(route('administration.ticketOperators.index'));
	}

	/**
	 * Remove the specified TicketOperator from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$ticketOperator = $this->ticketOperatorRepository->find($id);

		if(empty($ticketOperator))
		{
			Flash::error('TicketOperator not found');

			return redirect(route('administration.ticketOperators.index'));
		}

		

		$this->ticketOperatorRepository->delete($id);

		Flash::success('TicketOperator deleted successfully.');

		return redirect(route('administration.ticketOperators.index'));
	}
}
