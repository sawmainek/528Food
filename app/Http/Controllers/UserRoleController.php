<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRoleRequest;
use App\Http\Requests\UpdateUserRoleRequest;
use App\Libraries\Repositories\UserRoleRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\User;

class UserRoleController extends AppBaseController
{

	/** @var  UserRoleRepository */
	private $userRoleRepository;

	function __construct(UserRoleRepository $userRoleRepo)
	{
		$this->userRoleRepository = $userRoleRepo;
	}

	/**
	 * Display a listing of the UserRole.
	 *
	 * @return Response
	 */
	public function index()
	{
		$userRoles = UserRole::with(['role', 'user'])->paginate(10);

		return view('userRoles.index')
			->with('userRoles', $userRoles);
	}

	/**
	 * Show the form for creating a new UserRole.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$role=Role::all();
		$roles = [];
		foreach ($role as $key => $value) {
			$roles[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}
		return view('userRoles.create')->with(['roles'=>$roles, 'users'=>$users]);
	}

	/**
	 * Store a newly created UserRole in storage.
	 *
	 * @param CreateUserRoleRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserRoleRequest $request)
	{
		$input = $request->all();

		

		$userRole = $this->userRoleRepository->create($input);

		Flash::success('UserRole saved successfully.');

		return redirect(route('administration.userRoles.index'));
	}

	/**
	 * Display the specified UserRole.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$userRole = UserRole::with(['role', 'user'])->where('id', $id)->first();

		if(empty($userRole))
		{
			Flash::error('UserRole not found');

			return redirect(route('administration.userRoles.index'));
		}

		return view('userRoles.show')->with('userRole', $userRole);
	}

	/**
	 * Show the form for editing the specified UserRole.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		
		$role=Role::all();
		$roles = [];
		foreach ($role as $key => $value) {
			$roles[$value->id] = $value->name;
		}

		$user=User::all();
		$users = [];
		foreach ($user as $key => $value) {
			$users[$value->id] = $value->name;
		}

		$userRole = $this->userRoleRepository->find($id);

		if(empty($userRole))
		{
			Flash::error('UserRole not found');

			return redirect(route('administration.userRoles.index'));
		}

		return view('userRoles.edit')->with(['roles'=>$roles, 'users'=>$users, 'userRole'=>$userRole]);
	}

	/**
	 * Update the specified UserRole in storage.
	 *
	 * @param  int              $id
	 * @param UpdateUserRoleRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserRoleRequest $request)
	{
		$userRole = $this->userRoleRepository->find($id);

		if(empty($userRole))
		{
			Flash::error('UserRole not found');

			return redirect(route('administration.userRoles.index'));
		}

		$input = $request->all();

		

		$this->userRoleRepository->updateRich($input, $id);

		Flash::success('UserRole updated successfully.');

		return redirect(route('administration.userRoles.index'));
	}

	/**
	 * Remove the specified UserRole from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$userRole = $this->userRoleRepository->find($id);

		if(empty($userRole))
		{
			Flash::error('UserRole not found');

			return redirect(route('administration.userRoles.index'));
		}

		

		$this->userRoleRepository->delete($id);

		Flash::success('UserRole deleted successfully.');

		return redirect(route('administration.userRoles.index'));
	}
}
